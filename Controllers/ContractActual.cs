﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class ContractActualController : Controller
    {
        private readonly KPIContext db;
        public ContractActualController(KPIContext context)
        {

            db = context;
        }

        public IActionResult Index()
        {
            TabelContractActual TableActualModel = new TabelContractActual();
            TableActualModel.ActualTable = (from a in db.Contract_actual
                                     select new ActualGridTable{
                                         ID_Contract_actual=a.ID_Contract_actual,
                                          buyer = a.buyer.name,
                                          commodiy_group=a.commodity_group.name,
                                     month = a.month,
                                     value = a.value,
                                     direct=a.direct== true?"Direct":"Indirect"}).OrderBy(ss => ss.ID_Contract_actual).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableActualModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelContractActual  TabelContractActual = new  TabelContractActual();
            if (id != null && id != 0)
            {
                TabelContractActual.InputActual = new ActualInput();
                  TabelContractActual.InputActual.Form = "Edit";
                var Contract_actual = (from a in db.Contract_actual
                                       where a.ID_Contract_actual == id
                                       select new
                                       {
                                           ID_Contract_actual = a.ID_Contract_actual,

                                           buyer = a.buyer.ID_Contract_buyer,
                                           commodity_group = a.commodity_group.ID_Contract_commodity_group,
                                           direct = a.direct,

                                           month = a.month,
                                           value = a.value,

                                       }).FirstOrDefault();




                TabelContractActual.InputActual.ID_Contract_actual = Contract_actual.ID_Contract_actual;
                TabelContractActual.InputActual.direct = Contract_actual.direct;
                TabelContractActual.InputActual.month = Contract_actual.month;
                TabelContractActual.InputActual.value = Contract_actual.value;
                TabelContractActual.InputActual.buyer = Contract_actual.buyer;
                TabelContractActual.InputActual.commodity_group = Contract_actual.commodity_group;

            }
            else
            {
                 TabelContractActual.InputActual = new ActualInput();
                 TabelContractActual.InputActual.Form = "Create";
            }

            #region
            ViewBag.commodity_group = db.Contract_commodity_group.Select(ss => new { value = ss.ID_Contract_commodity_group, text = ss.name }).ToList();

            ViewBag.buyer = db.Contract_buyer.Select(ss => new { value = ss.ID_Contract_buyer, text = ss.name }).ToList();
            var StatusVal = new List<string>();
            StatusVal.Add("True");
            StatusVal.Add("False");
            ViewBag.StatusVal = StatusVal.Select(ss => new { value = ss, text = ss }).ToList();
            #endregion

            return PartialView("InputPopup", TabelContractActual);
        }
        private Contract_buyer Contract_buyer(long Value)
        {
            Contract_buyer List = new Contract_buyer();
            List = db.Contract_buyer.Where(ss => ss.ID_Contract_buyer == Value).FirstOrDefault();
            return List;

        }
        private Contract_commodity_group Contract_commodity_group(long Value)
        {
            Contract_commodity_group List = new Contract_commodity_group();
            List = db.Contract_commodity_group.Where(ss => ss.ID_Contract_commodity_group == Value).FirstOrDefault();
            return List;
        }
        [HttpPost]
        public IActionResult Input( TabelContractActual  TabelContractActual)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelContractActual.InputActual.ID_Contract_actual;
                if (id != null && id != 0)
                {
                    Contract_actual toUpdate = db.Contract_actual.Find(id);
                    
                    toUpdate.value = TabelContractActual.InputActual.value;
                    toUpdate.direct = TabelContractActual.InputActual.direct;
                    toUpdate.month = TabelContractActual.InputActual.month;
                    toUpdate.buyer = Contract_buyer(TabelContractActual.InputActual.buyer);
                    toUpdate.commodity_group = Contract_commodity_group(TabelContractActual.InputActual.commodity_group);



                    dbContextTransaction.Commit();
                    db.Entry(toUpdate).State = EntityState.Modified;
        
                    db.SaveChanges();

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
                else
                {
                    //iniloh
                    Contract_actual toAdd = new Contract_actual();
                    toAdd.value = TabelContractActual.InputActual.value;
                    toAdd.direct = TabelContractActual.InputActual.direct;
                    toAdd.month = TabelContractActual.InputActual.month;
                    toAdd.buyer = Contract_buyer(TabelContractActual.InputActual.buyer);
                    toAdd.commodity_group = Contract_commodity_group(TabelContractActual.InputActual.commodity_group);


                    dbContextTransaction.Commit();
                    db.Contract_actual.Add(toAdd);
                    db.SaveChanges();
                  

                   
                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelContractActual  TabelContractActual = new  TabelContractActual();
            TabelContractActual.InputActual = new ActualInput();
            Contract_actual Contract_actual= new Contract_actual();
            Contract_actual= db.Contract_actual.Find(id);
             TabelContractActual.InputActual.ID_Contract_actual= Contract_actual.ID_Contract_actual;

             TabelContractActual.URLAction = "Delete";
             TabelContractActual.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelContractActual);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelContractActual  TabelContractActual)
        {
            long? id =  TabelContractActual.InputActual.ID_Contract_actual;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_actual toDelete = db.Contract_actual.Find(id);
               
                 
            
                dbContextTransaction.Commit();
                db.Contract_actual.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelContractActual TabelContractActual = new TabelContractActual();
            if (!String.IsNullOrEmpty(id))
            {
                TabelContractActual.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelContractActual.idDeleteString = id;
            }
            var howmanyrow = TabelContractActual.idDeleteArray != null ? TabelContractActual.idDeleteArray.Count() : 0;

            TabelContractActual.URLAction = "DeleteAll";
            TabelContractActual.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelContractActual);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelContractActual TabelContractActual)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelContractActual.idDeleteString))
                {
                     TabelContractActual.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelContractActual.idDeleteString);
                }
                var howmanyrow =  TabelContractActual.idDeleteArray != null ?  TabelContractActual.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelContractActual.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_actual toDelete = db.Contract_actual.Find(idDelete);
                     
                   
                    db.Contract_actual.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}