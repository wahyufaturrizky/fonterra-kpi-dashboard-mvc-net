﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class ContractBuyerController : Controller
    {
        private readonly KPIContext db;
        public ContractBuyerController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelBuyer TableBuyerModel = new TabelBuyer();
            TableBuyerModel.BuyerTable = (from a in db.Contract_buyer
                                     select new BuyerGridTable{
                                         ID_Contract_buyer=a.ID_Contract_buyer,
                                          name = a.name}).OrderBy(ss => ss.ID_Contract_buyer).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableBuyerModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelBuyer  TabelBuyer = new  TabelBuyer();
            if (id != null && id != 0)
            {
                TabelBuyer.InputBuyer = new BuyerInput();
                  TabelBuyer.InputBuyer.Form = "Edit";

                Contract_buyer Contract_buyer= new Contract_buyer();
                Contract_buyer= db.Contract_buyer.Find(id);
                 TabelBuyer.InputBuyer.ID_Contract_buyer= Contract_buyer.ID_Contract_buyer;
                 TabelBuyer.InputBuyer.name = Contract_buyer.name;
                             }
            else
            {
                 TabelBuyer.InputBuyer = new BuyerInput();
                 TabelBuyer.InputBuyer.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelBuyer);
        }

        [HttpPost]
        public IActionResult Input( TabelBuyer  TabelBuyer)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelBuyer.InputBuyer.ID_Contract_buyer;
                List<Contract_buyer> cekName = new List<Contract_buyer>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Contract_buyer
                               where (a.name == TabelBuyer.InputBuyer.name && a.ID_Contract_buyer != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                       
                        Contract_buyer toUpdate = db.Contract_buyer.Find(id);
                        toUpdate.name = TabelBuyer.InputBuyer.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;

                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Buyer name had already taken !";
                    }
                }
                else
                {
                    cekName = (from a in db.Contract_buyer
                               where (a.name == TabelBuyer.InputBuyer.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {   
                        Contract_buyer toAdd = new Contract_buyer();
                        toAdd.name = TabelBuyer.InputBuyer.name;


                        dbContextTransaction.Commit();
                        db.Contract_buyer.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
               else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Buyer name had already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelBuyer  TabelBuyer = new  TabelBuyer();
            TabelBuyer.InputBuyer = new BuyerInput();
            Contract_buyer Contract_buyer= new Contract_buyer();
            Contract_buyer= db.Contract_buyer.Find(id);
             TabelBuyer.InputBuyer.ID_Contract_buyer= Contract_buyer.ID_Contract_buyer;

             TabelBuyer.URLAction = "Delete";
             TabelBuyer.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelBuyer);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelBuyer  TabelBuyer)
        {
            long? id =  TabelBuyer.InputBuyer.ID_Contract_buyer;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_buyer toDelete = db.Contract_buyer.Find(id);
               
                 
            
                dbContextTransaction.Commit();
                db.Contract_buyer.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelBuyer TabelBuyer = new TabelBuyer();
            if (!String.IsNullOrEmpty(id))
            {
                TabelBuyer.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelBuyer.idDeleteString = id;
            }
            var howmanyrow = TabelBuyer.idDeleteArray != null ? TabelBuyer.idDeleteArray.Count() : 0;

            TabelBuyer.URLAction = "DeleteAll";
            TabelBuyer.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelBuyer);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelBuyer TabelBuyer)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelBuyer.idDeleteString))
                {
                     TabelBuyer.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelBuyer.idDeleteString);
                }
                var howmanyrow =  TabelBuyer.idDeleteArray != null ?  TabelBuyer.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelBuyer.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_buyer toDelete = db.Contract_buyer.Find(idDelete);
                     
                   
                    db.Contract_buyer.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}