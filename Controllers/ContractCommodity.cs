﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class ContractCommodityController : Controller
    {
        private readonly KPIContext db;
        public ContractCommodityController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelComodity TableComodityModel = new TabelComodity();
            TableComodityModel.ComodityTable = (from a in db.Contract_commodity_group
                                     select new ComodityGridTable{
                                         ID_Contract_commodity_group=a.ID_Contract_commodity_group,
                                          name = a.name}).OrderBy(ss => ss.ID_Contract_commodity_group).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableComodityModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelComodity  TabelComodity = new  TabelComodity();
            if (id != null && id != 0)
            {
                TabelComodity.InputComodity = new ComodityInput();
                  TabelComodity.InputComodity.Form = "Edit";

                Contract_commodity_group Contract_commodity_group= new Contract_commodity_group();
                Contract_commodity_group= db.Contract_commodity_group.Find(id);
                 TabelComodity.InputComodity.ID_Contract_commodity_group= Contract_commodity_group.ID_Contract_commodity_group;
                 TabelComodity.InputComodity.name = Contract_commodity_group.name;
                             }
            else
            {
                 TabelComodity.InputComodity = new ComodityInput();
                 TabelComodity.InputComodity.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelComodity);
        }

        [HttpPost]
        public IActionResult Input( TabelComodity  TabelComodity)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelComodity.InputComodity.ID_Contract_commodity_group;
                List<Contract_commodity_group> cekName = new List<Contract_commodity_group>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Contract_commodity_group
                               where (a.name == TabelComodity.InputComodity.name && a.ID_Contract_commodity_group != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_commodity_group toUpdate = db.Contract_commodity_group.Find(id);
                        //  }
                        toUpdate.name = TabelComodity.InputComodity.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;

                        db.SaveChanges();





                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Commodity group name had already taken !";
                    }
                    }
                else
                {

                    cekName = (from a in db.Contract_commodity_group
                               where (a.name == TabelComodity.InputComodity.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_commodity_group toAdd = new Contract_commodity_group();
                        toAdd.name = TabelComodity.InputComodity.name;
                        dbContextTransaction.Commit();
                        db.Contract_commodity_group.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Commodity group name already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelComodity  TabelComodity = new  TabelComodity();
            TabelComodity.InputComodity = new ComodityInput();
            Contract_commodity_group Contract_commodity_group= new Contract_commodity_group();
            Contract_commodity_group= db.Contract_commodity_group.Find(id);
             TabelComodity.InputComodity.ID_Contract_commodity_group= Contract_commodity_group.ID_Contract_commodity_group;

             TabelComodity.URLAction = "Delete";
             TabelComodity.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelComodity);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelComodity  TabelComodity)
        {
            long? id =  TabelComodity.InputComodity.ID_Contract_commodity_group;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_commodity_group toDelete = db.Contract_commodity_group.Find(id);
                dbContextTransaction.Commit();
                db.Contract_commodity_group.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelComodity TabelComodity = new TabelComodity();
            if (!String.IsNullOrEmpty(id))
            {
                TabelComodity.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelComodity.idDeleteString = id;
            }
            var howmanyrow = TabelComodity.idDeleteArray != null ? TabelComodity.idDeleteArray.Count() : 0;

            TabelComodity.URLAction = "DeleteAll";
            TabelComodity.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelComodity);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelComodity TabelComodity)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelComodity.idDeleteString))
                {
                     TabelComodity.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelComodity.idDeleteString);
                }
                var howmanyrow =  TabelComodity.idDeleteArray != null ?  TabelComodity.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelComodity.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_commodity_group toDelete = db.Contract_commodity_group.Find(idDelete);
                     
                   
                    db.Contract_commodity_group.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}