﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
namespace hera.Controllers
{
    public class ContractPeriodController : Controller
    {
        private readonly KPIContext db;
        public ContractPeriodController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelPeriod TablePeriodModel = new TabelPeriod();
            TablePeriodModel.PeriodTable = (from a in db.Contract_period_coverage
                                     select new PeriodGridTable{
                                         ID_Contract_period_coverage=a.ID_Contract_period_coverage,
                                          period = a.period}).OrderBy(ss => ss.ID_Contract_period_coverage).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TablePeriodModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelPeriod  TabelPeriod = new  TabelPeriod();
            if (id != null && id != 0)
            {
                TabelPeriod.InputPeriod = new PeriodInput();
                  TabelPeriod.InputPeriod.Form = "Edit";

                Contract_period_coverage Contract_period_coverage= new Contract_period_coverage();
                Contract_period_coverage= db.Contract_period_coverage.Find(id);
                 TabelPeriod.InputPeriod.ID_Contract_period_coverage= Contract_period_coverage.ID_Contract_period_coverage;
                 TabelPeriod.InputPeriod.period = Contract_period_coverage.period;
                             }
            else
            {
                 TabelPeriod.InputPeriod = new PeriodInput();
                 TabelPeriod.InputPeriod.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelPeriod);
        }

        [HttpPost]
        public IActionResult Input( TabelPeriod  TabelPeriod)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelPeriod.InputPeriod.ID_Contract_period_coverage;
                List<Contract_period_coverage> cekName = new List<Contract_period_coverage>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Contract_period_coverage
                               where (a.period == TabelPeriod.InputPeriod.period && a.ID_Contract_period_coverage != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_period_coverage toUpdate = db.Contract_period_coverage.Find(id);
                        toUpdate.period = TabelPeriod.InputPeriod.period;

                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Periode had already taken !";
                    }
                }
                else
                {
                    cekName = (from a in db.Contract_period_coverage
                               where (a.period == TabelPeriod.InputPeriod.period)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_period_coverage toAdd = new Contract_period_coverage();
                        toAdd.period = TabelPeriod.InputPeriod.period;

                        dbContextTransaction.Commit();
                        db.Contract_period_coverage.Add(toAdd);
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Periode had already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelPeriod  TabelPeriod = new  TabelPeriod();
            TabelPeriod.InputPeriod = new PeriodInput();
            Contract_period_coverage Contract_period_coverage= new Contract_period_coverage();
            Contract_period_coverage= db.Contract_period_coverage.Find(id);
             TabelPeriod.InputPeriod.ID_Contract_period_coverage= Contract_period_coverage.ID_Contract_period_coverage;

             TabelPeriod.URLAction = "Delete";
             TabelPeriod.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelPeriod);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelPeriod  TabelPeriod)
        {
            long? id =  TabelPeriod.InputPeriod.ID_Contract_period_coverage;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_period_coverage toDelete = db.Contract_period_coverage.Find(id);
               
                 
            
                dbContextTransaction.Commit();
                db.Contract_period_coverage.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelPeriod TabelPeriod = new TabelPeriod();
            if (!String.IsNullOrEmpty(id))
            {
                TabelPeriod.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelPeriod.idDeleteString = id;
            }
            var howmanyrow = TabelPeriod.idDeleteArray != null ? TabelPeriod.idDeleteArray.Count() : 0;

            TabelPeriod.URLAction = "DeleteAll";
            TabelPeriod.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelPeriod);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelPeriod TabelPeriod)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelPeriod.idDeleteString))
                {
                     TabelPeriod.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelPeriod.idDeleteString);
                }
                var howmanyrow =  TabelPeriod.idDeleteArray != null ?  TabelPeriod.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelPeriod.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_period_coverage toDelete = db.Contract_period_coverage.Find(idDelete);
                     
                   
                    db.Contract_period_coverage.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}