﻿
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace hera.Controllers
{
    public class ContractQuarterController : Controller
    {
        private readonly KPIContext db;
        public ContractQuarterController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelContractQuarter TableQuarterModel = new TabelContractQuarter();
            TableQuarterModel.QuarterTable = (from a in db.Contract_quarter
                                              select new QuarterGridTable
                                              {
                                                  ID_Contract_quarter = a.ID_Contract_quarter,
                                                  tracking = a.tracking.ID_Contract_tracking,
                                                  quarter = a.quarter,


                                                  week = a.week,
                                              }).OrderBy(ss => ss.ID_Contract_quarter).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableQuarterModel);
        }

        [HttpGet]
        public IActionResult Input(long? id)
        {
            TabelContractQuarter TabelContractQuarter = new TabelContractQuarter();
            if (id != null && id != 0)
            {
                TabelContractQuarter.InputQuarter = new QuarterInput();
                TabelContractQuarter.InputQuarter.Form = "Edit";
                var Contract_quarter = (from a in db.Contract_quarter
                                        where a.ID_Contract_quarter == id
                                        select new
                                        {
                                            ID_Contract_quarter = a.ID_Contract_quarter,

                                            week = a.week,
                                            quarter = a.quarter,
                                            tracking = a.tracking.ID_Contract_tracking

                                        }).FirstOrDefault();





                TabelContractQuarter.InputQuarter.ID_Contract_quarter = Contract_quarter.ID_Contract_quarter;
                TabelContractQuarter.InputQuarter.quarter = Contract_quarter.quarter;
                TabelContractQuarter.InputQuarter.week = Contract_quarter.week;
                TabelContractQuarter.InputQuarter.tracking = Contract_quarter.tracking;

            }
            else
            {
                TabelContractQuarter.InputQuarter = new QuarterInput();
                TabelContractQuarter.InputQuarter.Form = "Create";
            }
            #region dropdownlist
            ViewBag.tracking = db.Contract_tracking.Select(ss => new { value = ss.ID_Contract_tracking, text = ss.ID_Contract_tracking }).ToList();
            #endregion


            return PartialView("InputPopup", TabelContractQuarter);
        }

        [HttpPost]
        public IActionResult Input(TabelContractQuarter TabelContractQuarter)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id = TabelContractQuarter.InputQuarter.ID_Contract_quarter;
                if (id != null && id != 0)
                {
                    Contract_quarter toUpdate = db.Contract_quarter.Find(id);
                    toUpdate.quarter = TabelContractQuarter.InputQuarter.quarter;
                    toUpdate.week = TabelContractQuarter.InputQuarter.week;
                    var tracking_unyu = toUpdate.trackingID_Contract_tracking;
                    toUpdate.tracking = Contract_tracking(TabelContractQuarter.InputQuarter.tracking);
                    dbContextTransaction.Commit();
                    db.Entry(toUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                 
                    if (toUpdate.trackingID_Contract_tracking != TabelContractQuarter.InputQuarter.ID_Contract_quarter)
                    {
                      var totalsebelum = db.Contract_quarter.Where(s => s.trackingID_Contract_tracking == tracking_unyu).Sum(s => s.quarter);
                    Contract_tracking toEdit = db.Contract_tracking.Find(tracking_unyu);
                    if (totalsebelum < 10)
                    {
                      toEdit.completed_count = false;
                     db.Entry(toEdit).State = EntityState.Modified;
                    db.SaveChanges();
                    }

                     }

                    var totalsesudah = db.Contract_quarter.Where(s => s.trackingID_Contract_tracking == TabelContractQuarter.InputQuarter.tracking).Sum(s => s.quarter);
                    Contract_tracking toEdit1 = db.Contract_tracking.Find(TabelContractQuarter.InputQuarter.tracking);
                    if (totalsesudah >= 10)
                    {

                        toEdit1.completed_count = true;
                        db.Entry(toEdit1).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (totalsesudah <= 9)
                    {

                        toEdit1.completed_count = false;
                        db.Entry(toEdit1).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
                else
                {
                    Contract_quarter toAdd = new Contract_quarter();
                    toAdd.quarter = TabelContractQuarter.InputQuarter.quarter;
                    toAdd.tracking = Contract_tracking(TabelContractQuarter.InputQuarter.tracking);
                    toAdd.week = TabelContractQuarter.InputQuarter.week;

                    dbContextTransaction.Commit();
                    db.Contract_quarter.Add(toAdd);
                    db.SaveChanges();
                    cekTotalAdd(TabelContractQuarter, toAdd);

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }
        public void cekTotalAdd(TabelContractQuarter TabelContractQuarter, Contract_quarter edit)
        {
            var total = db.Contract_quarter.Where(s => s.trackingID_Contract_tracking == edit.trackingID_Contract_tracking).Sum(s => s.quarter);
            Contract_tracking toEdit = db.Contract_tracking.Find(edit.trackingID_Contract_tracking);
            if (total >= 10)
            {
                toEdit.completed_count = true;
            }
            else if (total < 10)
            {
                toEdit.completed_count = false;

            }

            db.Entry(toEdit).State = EntityState.Modified;
            db.SaveChanges();

        }
      
        [HttpGet]
        public IActionResult Delete(long? id)
        {

            TabelContractQuarter TabelContractQuarter = new TabelContractQuarter();
            TabelContractQuarter.InputQuarter = new QuarterInput();
            Contract_quarter Contract_quarter = new Contract_quarter();
            Contract_quarter = db.Contract_quarter.Find(id);
            TabelContractQuarter.InputQuarter.ID_Contract_quarter = Contract_quarter.ID_Contract_quarter;

            TabelContractQuarter.URLAction = "Delete";
            TabelContractQuarter.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup", TabelContractQuarter);
        }






        [HttpPost]
        public IActionResult Delete(TabelContractQuarter TabelContractQuarter)
        {
            long? id = TabelContractQuarter.InputQuarter.ID_Contract_quarter;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {

                Contract_quarter toDelete = db.Contract_quarter.Find(id);
                var total = db.Contract_quarter.Where(s => s.trackingID_Contract_tracking == toDelete.trackingID_Contract_tracking).Sum(s => s.quarter);
                Contract_tracking toEdit = db.Contract_tracking.Find(toDelete.trackingID_Contract_tracking);
                if (total >= 10)
                {
                    toEdit.completed_count = true;
                }
                else if (total < 10)
                {
                    toEdit.completed_count = false;

                }

                db.Entry(toEdit).State = EntityState.Modified;
                db.SaveChanges();

                dbContextTransaction.Commit();
                db.Contract_quarter.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelContractQuarter TabelContractQuarter = new TabelContractQuarter();
            if (!String.IsNullOrEmpty(id))
            {
                TabelContractQuarter.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelContractQuarter.idDeleteString = id;
            }
            var howmanyrow = TabelContractQuarter.idDeleteArray != null ? TabelContractQuarter.idDeleteArray.Count() : 0;

            TabelContractQuarter.URLAction = "DeleteAll";
            TabelContractQuarter.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelContractQuarter);
        }
        private Contract_tracking Contract_tracking(long Value)
        {
            Contract_tracking List = new Contract_tracking();
            List = db.Contract_tracking.Where(ss => ss.ID_Contract_tracking == Value).FirstOrDefault();
            return List;
        }
        [HttpPost]
        public IActionResult DeleteAll(TabelContractQuarter TabelContractQuarter)
        {
            try
            {
                if (!String.IsNullOrEmpty(TabelContractQuarter.idDeleteString))
                {
                    TabelContractQuarter.idDeleteArray = JsonConvert.DeserializeObject<string[]>(TabelContractQuarter.idDeleteString);
                }
                var howmanyrow = TabelContractQuarter.idDeleteArray != null ? TabelContractQuarter.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in TabelContractQuarter.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);

                    Contract_quarter toDelete = db.Contract_quarter.Find(idDelete);
                    var total = db.Contract_quarter.Where(s => s.trackingID_Contract_tracking == toDelete.trackingID_Contract_tracking).Sum(s => s.quarter);
                    Contract_tracking toEdit = db.Contract_tracking.Find(toDelete.trackingID_Contract_tracking);
                    if (total >= 10)
                    {
                        toEdit.completed_count = true;
                    }
                    else if (total < 10)
                    {
                        toEdit.completed_count = false;

                    }

                    db.Entry(toEdit).State = EntityState.Modified;
                    db.SaveChanges();

                    db.Contract_quarter.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }


    }
}