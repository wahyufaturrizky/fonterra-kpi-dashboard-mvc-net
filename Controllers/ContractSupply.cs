﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class ContractSupplyController : Controller
    {
        private readonly KPIContext db;
        public ContractSupplyController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelSupply TableSupplyModel = new TabelSupply();
            TableSupplyModel.SupplyTable = (from a in db.Contract_supply_category
                                     select new SupplyGridTable{
                                         ID_Contract_supply_category=a.ID_Contract_supply_category,
                                          name = a.name}).OrderBy(ss => ss.ID_Contract_supply_category).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableSupplyModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelSupply  TabelSupply = new  TabelSupply();
            if (id != null && id != 0)
            {
                TabelSupply.InputSupply = new SupplyInput();
                  TabelSupply.InputSupply.Form = "Edit";

                Contract_supply_category Contract_supply_category= new Contract_supply_category();
                Contract_supply_category= db.Contract_supply_category.Find(id);
                 TabelSupply.InputSupply.ID_Contract_supply_category= Contract_supply_category.ID_Contract_supply_category;
                 TabelSupply.InputSupply.name = Contract_supply_category.name;
                             }
            else
            {
                 TabelSupply.InputSupply = new SupplyInput();
                 TabelSupply.InputSupply.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelSupply);
        }

        [HttpPost]
        public IActionResult Input( TabelSupply  TabelSupply)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelSupply.InputSupply.ID_Contract_supply_category;
                List<Contract_supply_category> cekName = new List<Contract_supply_category>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Contract_supply_category
                               where (a.name == TabelSupply.InputSupply.name && a.ID_Contract_supply_category != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_supply_category toUpdate = db.Contract_supply_category.Find(id);
                        toUpdate.name = TabelSupply.InputSupply.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Supply category name had already taken !";
                    }
                }
                else
                {
                    cekName = (from a in db.Contract_supply_category
                               where (a.name == TabelSupply.InputSupply.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Contract_supply_category toAdd = new Contract_supply_category();
                        toAdd.name = TabelSupply.InputSupply.name;
           
                        dbContextTransaction.Commit();
                        db.Contract_supply_category.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Supply category name had already taken !";
                    }
                }

            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelSupply  TabelSupply = new  TabelSupply();
            TabelSupply.InputSupply = new SupplyInput();
            Contract_supply_category Contract_supply_category= new Contract_supply_category();
            Contract_supply_category= db.Contract_supply_category.Find(id);
             TabelSupply.InputSupply.ID_Contract_supply_category= Contract_supply_category.ID_Contract_supply_category;

             TabelSupply.URLAction = "Delete";
             TabelSupply.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelSupply);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelSupply  TabelSupply)
        {
            long? id =  TabelSupply.InputSupply.ID_Contract_supply_category;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_supply_category toDelete = db.Contract_supply_category.Find(id);
               
                 
            
                dbContextTransaction.Commit();
                db.Contract_supply_category.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelSupply TabelSupply = new TabelSupply();
            if (!String.IsNullOrEmpty(id))
            {
                TabelSupply.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelSupply.idDeleteString = id;
            }
            var howmanyrow = TabelSupply.idDeleteArray != null ? TabelSupply.idDeleteArray.Count() : 0;

            TabelSupply.URLAction = "DeleteAll";
            TabelSupply.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelSupply);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelSupply TabelSupply)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelSupply.idDeleteString))
                {
                     TabelSupply.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelSupply.idDeleteString);
                }
                var howmanyrow =  TabelSupply.idDeleteArray != null ?  TabelSupply.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelSupply.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_supply_category toDelete = db.Contract_supply_category.Find(idDelete);
                     
                   
                    db.Contract_supply_category.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}