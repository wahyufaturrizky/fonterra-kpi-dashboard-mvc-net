﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;


namespace hera.Controllers
{
    public class ContractTargetController : Controller
    {
        private readonly KPIContext db;
        public ContractTargetController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelContractTarget TableTargetModel = new TabelContractTarget();
            TableTargetModel.TargetTable = (from a in db.Contract_target
                                     select new TargetGridTable{
                                         ID_Contract_target=a.ID_Contract_target,
                                          buyer = a.buyer.name,
                                          commodiy_group=a.commodity_group.name,
                                          value=a.value,
                                     month = a.month,
                                  

                                         direct = a.direct == true? "Direct":"Indirect"
                                     }).OrderBy(ss => ss.ID_Contract_target).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableTargetModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelContractTarget  TabelContractTarget = new  TabelContractTarget();
            if (id != null && id != 0)
            {
                TabelContractTarget.InputTarget = new TargetInput();
                  TabelContractTarget.InputTarget.Form = "Edit";
                var Contract_target = (from a in db.Contract_target
                                      where a.ID_Contract_target == id
                                      select new
                                      {
                                          ID_Contract_target = a.ID_Contract_target,
                                        
                                          buyer = a.buyer.ID_Contract_buyer,
                                          commodity_group = a.commodity_group.ID_Contract_commodity_group,
                                          direct = a.direct,

                                          month = a.month,
                                          value = a.value,
                                        
                                      }).FirstOrDefault();

               

             
                 TabelContractTarget.InputTarget.ID_Contract_target= Contract_target.ID_Contract_target;
                 TabelContractTarget.InputTarget.direct = Contract_target.direct;
                TabelContractTarget.InputTarget.month = Contract_target.month;
                TabelContractTarget.InputTarget.value = Contract_target.value;
                TabelContractTarget.InputTarget.buyer = Contract_target.buyer;
                TabelContractTarget.InputTarget.commodity_group = Contract_target.commodity_group;


            }
            else
            {
                 TabelContractTarget.InputTarget = new TargetInput();
                 TabelContractTarget.InputTarget.Form = "Create";
            }
            #region
            ViewBag.commodity_group = db.Contract_commodity_group.Select(ss => new { value = ss.ID_Contract_commodity_group, text = ss.name }).ToList();
       
                ViewBag.buyer = db.Contract_buyer.Select(ss => new { value = ss.ID_Contract_buyer, text = ss.name }).ToList();
            var StatusVal = new List<string>();
            StatusVal.Add("True");
            StatusVal.Add("False");
            ViewBag.StatusVal = StatusVal.Select(ss => new { value = ss, text = ss }).ToList();
            #endregion

            return PartialView("InputPopup",  TabelContractTarget);
        }
        private Contract_buyer Contract_buyer (long Value)
        {
            Contract_buyer List = new Contract_buyer();
            List = db.Contract_buyer.Where(ss => ss.ID_Contract_buyer == Value).FirstOrDefault();
            return List;

        }
        private Contract_commodity_group Contract_commodity_group(long Value)
        {
            Contract_commodity_group List = new Contract_commodity_group();
            List = db.Contract_commodity_group.Where(ss => ss.ID_Contract_commodity_group == Value).FirstOrDefault();
            return List;
        }
        [HttpPost]
        public IActionResult Input( TabelContractTarget  TabelContractTarget)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelContractTarget.InputTarget.ID_Contract_target;
                if (id != null && id != 0)
                {
                    Contract_target toUpdate = db.Contract_target.Find(id);
                    toUpdate.value =  TabelContractTarget.InputTarget.value;
                    toUpdate.direct = TabelContractTarget.InputTarget.direct;
                    toUpdate.month = TabelContractTarget.InputTarget.month;
                    toUpdate.buyer = Contract_buyer(TabelContractTarget.InputTarget.buyer);
                    toUpdate.commodity_group = Contract_commodity_group(TabelContractTarget.InputTarget.commodity_group);
                   

                    
                    dbContextTransaction.Commit();
                    db.Entry(toUpdate).State = EntityState.Modified;
    
                    db.SaveChanges();
                
                     
                


                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
                else
                {
                    Contract_target toAdd = new Contract_target();
                   toAdd.value = TabelContractTarget.InputTarget.value;
                   toAdd.direct = TabelContractTarget.InputTarget.direct;
                   toAdd.month = TabelContractTarget.InputTarget.month;
                   toAdd.buyer = Contract_buyer(TabelContractTarget.InputTarget.buyer);
                   toAdd.commodity_group = Contract_commodity_group(TabelContractTarget.InputTarget.commodity_group);


                    dbContextTransaction.Commit();
                    db.Contract_target.Add(toAdd);
                    db.SaveChanges();
                  

                   
                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelContractTarget  TabelContractTarget = new  TabelContractTarget();
            TabelContractTarget.InputTarget = new TargetInput();
            Contract_target Contract_target= new Contract_target();
            Contract_target= db.Contract_target.Find(id);
             TabelContractTarget.InputTarget.ID_Contract_target= Contract_target.ID_Contract_target;

             TabelContractTarget.URLAction = "Delete";
             TabelContractTarget.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelContractTarget);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelContractTarget  TabelContractTarget)
        {
            long? id =  TabelContractTarget.InputTarget.ID_Contract_target;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Contract_target toDelete = db.Contract_target.Find(id);
               
                 
            
                dbContextTransaction.Commit();
                db.Contract_target.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelContractTarget TabelContractTarget = new TabelContractTarget();
            if (!String.IsNullOrEmpty(id))
            {
                TabelContractTarget.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelContractTarget.idDeleteString = id;
            }
            var howmanyrow = TabelContractTarget.idDeleteArray != null ? TabelContractTarget.idDeleteArray.Count() : 0;

            TabelContractTarget.URLAction = "DeleteAll";
            TabelContractTarget.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelContractTarget);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelContractTarget TabelContractTarget)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelContractTarget.idDeleteString))
                {
                     TabelContractTarget.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelContractTarget.idDeleteString);
                }
                var howmanyrow =  TabelContractTarget.idDeleteArray != null ?  TabelContractTarget.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelContractTarget.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Contract_target toDelete = db.Contract_target.Find(idDelete);
                     
                   
                    db.Contract_target.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}