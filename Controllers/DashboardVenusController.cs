﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Venus;
using Newtonsoft.Json;

namespace hera.Controllers
{
    public class DashboardVenusController : Controller
    {
        private readonly KPIContext db;
        public DashboardVenusController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index(string potypepass, string userpass, string businessunitpass)
        {
            VenusGlobalModel VenusModel = new VenusGlobalModel();

            VenusModel.POTypeFilterArray = new string[] { };
            VenusModel.UserFilterArray = new string[] { };
            VenusModel.BusinessUnitFilterArray = new string[] { };
            if (!String.IsNullOrEmpty(potypepass))
            {
                VenusModel.POTypeFilterArray = JsonConvert.DeserializeObject<string[]>(potypepass);
            }
            if (!String.IsNullOrEmpty(userpass))
            {
                VenusModel.UserFilterArray = JsonConvert.DeserializeObject<string[]>(userpass);
            }
            if (!String.IsNullOrEmpty(businessunitpass))
            {
                VenusModel.BusinessUnitFilterArray = JsonConvert.DeserializeObject<string[]>(businessunitpass);
            }

            #region DatabaseLoad
            VenusModel.POType = (from a in db.Venus_po_type select a).OrderBy(ss => ss.ID_Venus_po_type).ToList();
            VenusModel.Department = (from a in db.Venus_department select a).OrderBy(ss => ss.ID_Venus_department).ToList();
            VenusModel.BusinessUnit = (from a in db.Venus_business_unit select a).OrderBy(ss => ss.ID_Venus_business_unit).ToList();
            VenusModel.Venus_grni = (from a in db.Venus_grni select a).OrderBy(ss => ss.ID_Venus_grni).ToList();
            #endregion

            #region Checkboxload
            VenusModel.POTypeFilter = (from a in db.Venus_po_type
                                       select new POTypeFilter
                                       {
                                           ID_Venus_po_type = a.ID_Venus_po_type,
                                           name = a.name,
                                           IsCheckedPOType = true
                                       }
                                       ).OrderBy(ss => ss.ID_Venus_po_type).ToList();
            VenusModel.UserFilter = (from a in db.Venus_department
                                     select new DepartmentFilter
                                     {
                                         ID_Venus_department = a.ID_Venus_department,
                                         name = a.name,
                                         IsCheckedDepartment = true
                                     }
                                     ).OrderBy(ss => ss.ID_Venus_department).ToList();
            VenusModel.BusinessUnitFilter = (from a in db.Venus_business_unit
                                             select new BusinessUnitFilter
                                             {
                                                 ID_Venus_business_unit = a.ID_Venus_business_unit,
                                                 name = a.name,
                                                 IsCheckedBusinessUnit = true
                                             }
                                             ).OrderBy(ss => ss.ID_Venus_business_unit).ToList();
            #endregion

            #region Filter
            VenusModel = Filter(VenusModel);
            #endregion

            #region MonthlyGRNI
            var MonthlyGRNISumAll = VenusModel.Venus_grni.Sum(ss => ss.amount);
            var MonthlyGRNISumClose = VenusModel.Venus_grni.Where(ss => ss.status.ToLower().Trim() == "close").Sum(ss => ss.amount);
            var MonthlyGRNISumFormula = Convert.ToDecimal(MonthlyGRNISumAll) != 0 ? (Convert.ToDecimal(MonthlyGRNISumClose) / Convert.ToDecimal(MonthlyGRNISumAll)) * 100 : 0;
            string MonthlyGRNISumFormulaStr = MonthlyGRNISumFormula.ToString("#,##0.00") + "%";
            var MonthlyGRNICountAll = VenusModel.Venus_grni.Count();
            var MonthlyGRNICountClose = VenusModel.Venus_grni.Where(ss => ss.status.ToLower().Trim() == "close").Count();
            var MonthlyGRNICountFormula = Convert.ToDecimal(MonthlyGRNICountAll) != 0 ? (Convert.ToDecimal(MonthlyGRNICountClose) / Convert.ToDecimal(MonthlyGRNICountAll)) * 100 : 0;
            string MonthlyGRNICountFormulaStr = MonthlyGRNICountFormula.ToString("#,##0.00") + "%";

            VenusModel.MonthlyGRNI = new List<MonthlyGRNI>();
            VenusModel.MonthlyGRNI.Add(new MonthlyGRNI
            {
                Values = MonthlyGRNISumFormulaStr,
                Remarks = "Values have been closed"
            });
            VenusModel.MonthlyGRNI.Add(new MonthlyGRNI
            {
                Values = MonthlyGRNICountFormulaStr,
                Remarks = "Cases have been closed",
            });
            VenusModel.MonthlyGRNI.Add(new MonthlyGRNI
            {
                Values = "1232.21",
                Remarks = "days covered",
            });
            #endregion

            #region MonthlyGRNIFigures
            var FiguresCasesCount = VenusModel.Venus_grni
                .GroupBy(ss => ss.month.Month)
                .Select(grp => new
                {
                    Month = grp.Key,
                    CasesCount = grp.Count()
                });
            string AgustusCasesCount = FiguresCasesCount.Where(ss => ss.Month == 8).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string SeptemberCasesCount = FiguresCasesCount.Where(ss => ss.Month == 9).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string OktoberCasesCount = FiguresCasesCount.Where(ss => ss.Month == 10).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string NovemberCasesCount = FiguresCasesCount.Where(ss => ss.Month == 11).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string DesemberCasesCount = FiguresCasesCount.Where(ss => ss.Month == 12).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string JanuariCasesCount = FiguresCasesCount.Where(ss => ss.Month == 1).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string FebuariCasesCount = FiguresCasesCount.Where(ss => ss.Month == 2).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string MaretCasesCount = FiguresCasesCount.Where(ss => ss.Month == 3).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string AprilCasesCount = FiguresCasesCount.Where(ss => ss.Month == 4).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string MeiCasesCount = FiguresCasesCount.Where(ss => ss.Month == 5).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string JuniCasesCount = FiguresCasesCount.Where(ss => ss.Month == 6).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            string JuliCasesCount = FiguresCasesCount.Where(ss => ss.Month == 7).Select(ss => ss.CasesCount).FirstOrDefault().ToString();
            List<string> CasesCountCalc = new List<string>
            {
                AgustusCasesCount,
                SeptemberCasesCount,
                OktoberCasesCount,
                NovemberCasesCount,
                DesemberCasesCount,
                JanuariCasesCount,
                FebuariCasesCount,
                MaretCasesCount,
                AprilCasesCount,
                MeiCasesCount,
                JuniCasesCount,
                JuliCasesCount
            };
            VenusModel.MonthlyGRNIFigures.CasesCount = CasesCountCalc.ToArray();
            var FiguresClosed = VenusModel.Venus_grni
                .Where(ss => ss.status.ToLower().Trim() == "close")
                .GroupBy(ss => ss.month.Month)
                .Select(grp => new
                {
                    Month = grp.Key,
                    Closed = grp.Sum(ss => ss.amount)
                });
            string AgustusClosed = FiguresClosed.Where(ss => ss.Month == 8).Sum(ss => ss.Closed).ToString();
            string SeptemberClosed = FiguresClosed.Where(ss => ss.Month == 9).Sum(ss => ss.Closed).ToString();
            string OktoberClosed = FiguresClosed.Where(ss => ss.Month == 10).Sum(ss => ss.Closed).ToString();
            string NovemberClosed = FiguresClosed.Where(ss => ss.Month == 11).Sum(ss => ss.Closed).ToString();
            string DesemberClosed = FiguresClosed.Where(ss => ss.Month == 12).Sum(ss => ss.Closed).ToString();
            string JanuariClosed = FiguresClosed.Where(ss => ss.Month == 1).Sum(ss => ss.Closed).ToString();
            string FebuariClosed = FiguresClosed.Where(ss => ss.Month == 2).Sum(ss => ss.Closed).ToString();
            string MaretClosed = FiguresClosed.Where(ss => ss.Month == 3).Sum(ss => ss.Closed).ToString();
            string AprilClosed = FiguresClosed.Where(ss => ss.Month == 4).Sum(ss => ss.Closed).ToString();
            string MeiClosed = FiguresClosed.Where(ss => ss.Month == 5).Sum(ss => ss.Closed).ToString();
            string JuniClosed = FiguresClosed.Where(ss => ss.Month == 6).Sum(ss => ss.Closed).ToString();
            string JuliClosed = FiguresClosed.Where(ss => ss.Month == 7).Sum(ss => ss.Closed).ToString();
            List<string> ClosedCalc = new List<string>
            {
                AgustusClosed,
                SeptemberClosed,
                OktoberClosed,
                NovemberClosed,
                DesemberClosed,
                JanuariClosed,
                FebuariClosed,
                MaretClosed,
                AprilClosed,
                MeiClosed,
                JuniClosed,
                JuliClosed
            };
            VenusModel.MonthlyGRNIFigures.Closed = ClosedCalc.ToArray();
            var FiguresOpen = VenusModel.Venus_grni
                .Where(ss => ss.status.ToLower().Trim() == "open")
                .GroupBy(ss => ss.month.Month)
                .Select(grp => new
                {
                    Month = grp.Key,
                    Open = grp.Sum(ss => ss.amount)
                });
            string AgustusOpen = FiguresOpen.Where(ss => ss.Month == 8).Sum(ss => ss.Open).ToString();
            string SeptemberOpen = FiguresOpen.Where(ss => ss.Month == 9).Sum(ss => ss.Open).ToString();
            string OktoberOpen = FiguresOpen.Where(ss => ss.Month == 10).Sum(ss => ss.Open).ToString();
            string NovemberOpen = FiguresOpen.Where(ss => ss.Month == 11).Sum(ss => ss.Open).ToString();
            string DesemberOpen = FiguresOpen.Where(ss => ss.Month == 12).Sum(ss => ss.Open).ToString();
            string JanuariOpen = FiguresOpen.Where(ss => ss.Month == 1).Sum(ss => ss.Open).ToString();
            string FebuariOpen = FiguresOpen.Where(ss => ss.Month == 2).Sum(ss => ss.Open).ToString();
            string MaretOpen = FiguresOpen.Where(ss => ss.Month == 3).Sum(ss => ss.Open).ToString();
            string AprilOpen = FiguresOpen.Where(ss => ss.Month == 4).Sum(ss => ss.Open).ToString();
            string MeiOpen = FiguresOpen.Where(ss => ss.Month == 5).Sum(ss => ss.Open).ToString();
            string JuniOpen = FiguresOpen.Where(ss => ss.Month == 6).Sum(ss => ss.Open).ToString();
            string JuliOpen = FiguresOpen.Where(ss => ss.Month == 7).Sum(ss => ss.Open).ToString();
            List<string> OpenCalc = new List<string>
            {
                AgustusOpen,
                SeptemberOpen,
                OktoberOpen,
                NovemberOpen,
                DesemberOpen,
                JanuariOpen,
                FebuariOpen,
                MaretOpen,
                AprilOpen,
                MeiOpen,
                JuniOpen,
                JuliOpen
            };
            VenusModel.MonthlyGRNIFigures.Open = OpenCalc.ToArray();
            #endregion

            #region MonthlyGRNIDatatable
            VenusModel.MonthlyGRNIDatatable = VenusModel.Venus_grni.OrderByDescending(ss => ss.amount).Take(10).ToList();
            #endregion

            return View(VenusModel);
        }

        private VenusGlobalModel Filter(VenusGlobalModel VenusModel)
        {
            if (VenusModel.POTypeFilterArray != null && VenusModel.POTypeFilterArray.Length > 0)
            {
                VenusModel.Venus_grni = VenusModel.Venus_grni.Where(ss => VenusModel.POTypeFilterArray.Contains(ss.po_type.name)).ToList();
                VenusModel.POTypeFilter = (from a in db.Venus_po_type
                                           select new POTypeFilter
                                           {
                                               ID_Venus_po_type = a.ID_Venus_po_type,
                                               name = a.name,
                                               IsCheckedPOType = VenusModel.POTypeFilterArray.Contains(a.name) ? true : false
                                           }
                                           ).OrderBy(ss => ss.ID_Venus_po_type).ToList();
            }
            if (VenusModel.UserFilterArray != null && VenusModel.UserFilterArray.Length > 0)
            {
                VenusModel.Venus_grni = VenusModel.Venus_grni.Where(ss => VenusModel.UserFilterArray.Contains(ss.department.name)).ToList();
                VenusModel.UserFilter = (from a in db.Venus_department
                                         select new DepartmentFilter
                                         {
                                             ID_Venus_department = a.ID_Venus_department,
                                             name = a.name,
                                             IsCheckedDepartment = VenusModel.UserFilterArray.Contains(a.name) ? true : false
                                         }
                                         ).OrderBy(ss => ss.ID_Venus_department).ToList();
            }
            if (VenusModel.BusinessUnitFilterArray != null && VenusModel.BusinessUnitFilterArray.Length > 0)
            {
                VenusModel.Venus_grni = VenusModel.Venus_grni.Where(ss => VenusModel.BusinessUnitFilterArray.Contains(ss.business_unit.name)).ToList();
                VenusModel.BusinessUnitFilter = (from a in db.Venus_business_unit
                                                 select new BusinessUnitFilter
                                                 {
                                                     ID_Venus_business_unit = a.ID_Venus_business_unit,
                                                     name = a.name,
                                                     IsCheckedBusinessUnit = VenusModel.BusinessUnitFilterArray.Contains(a.name) ? true : false
                                                 }
                                             ).OrderBy(ss => ss.ID_Venus_business_unit).ToList();
            }

            return VenusModel;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
