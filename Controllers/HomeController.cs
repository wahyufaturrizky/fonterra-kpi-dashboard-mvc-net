﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Contracts;

namespace hera.Controllers
{
    public class HomeController : Controller
    {
        private readonly KPIContext db;
        public HomeController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index(int? buyerpass, int? spendingtypepass, int? commoditypass)
        {
            ContractGlobalModel ContractModel = new ContractGlobalModel();
            ContractModel.BuyerFilter = buyerpass == null ? 0 : (int)buyerpass;
            ContractModel.SpendingTypeFilter = spendingtypepass == null ? 2 : (int)spendingtypepass; 
            ContractModel.CommodityCategoryFilter = commoditypass == null ? 0 : (int)commoditypass;

            #region DatabaseLoad
            ContractModel.Buyer = (from a in db.Contract_buyer select a).OrderBy(ss => ss.ID_Contract_buyer).ToList();
            ContractModel.Commodity_Category = (from a in db.Contract_commodity_group select a).OrderBy(ss => ss.ID_Contract_commodity_group).ToList();
            ContractModel.Contract_period_coverage = (from a in db.Contract_period_coverage select a).OrderBy(ss => ss.ID_Contract_period_coverage).ToList();
     
            ContractModel.Contract_Tracking = (from a in db.Contract_tracking select a).OrderBy(ss => ss.ID_Contract_tracking).ToList();
            ContractModel.Contract_Target = (from a in db.Contract_target select a).OrderBy(ss => ss.ID_Contract_target).ToList();
            ContractModel.Contract_Actual = (from a in db.Contract_actual select a).OrderBy(ss => ss.ID_Contract_actual).ToList();
            
            ContractModel = Filter(ContractModel);
            #endregion

            #region Radiobuttonload
            ContractModel.BuyerRadio = ContractModel.Buyer;
            ContractModel.CommodityCategoryRadio = ContractModel.Commodity_Category;
            #endregion

            #region TotalCompleteness
            ContractModel.TotalCompleteness.Completed = ContractModel.Contract_Tracking.Where(ss => ss.completed_count == true).Count();
            ContractModel.TotalCompleteness.Uncompleted = ContractModel.Contract_Tracking.Where(ss => ss.completed_count == false).Count();
            #endregion

            #region CompletenessConverage
            var CompletenessConverage = ContractModel.Contract_Tracking
                .GroupBy(ss => ss.period_coverage)
                .Select(grp => new
                {
                    Period = grp.Key.period,
                    Completed = grp.Where(ss => ss.completed_count == true).Select(ss => ss.completed_count).Count(),
                    Uncompleted = grp.Where(ss => ss.completed_count == false).Select(ss => ss.completed_count).Count()
                }).OrderBy(ss => ss.Period);
            ContractModel.CompletenessConverage.FY = CompletenessConverage.Select(ss => ss.Period).ToArray();
            ContractModel.CompletenessConverage.Completed = CompletenessConverage.Select(ss => ss.Completed).ToArray();
            ContractModel.CompletenessConverage.Uncompleted = CompletenessConverage.Select(ss => ss.Uncompleted).ToArray();
            #endregion

            #region Contractsbycategotry
            var Contractsbycategotry = ContractModel.Contract_Tracking
                .GroupBy(ss => ss.commodity_group)
                .Select(grp => new
                {
                    Commodity = grp.Key.name,
                    ContractCount = grp.Select(ss => ss.contract_ID).Count()
                }).OrderBy(ss => ss.Commodity);
            ContractModel.ContractsbyCategory.CommodityCategory = Contractsbycategotry.Select(ss => ss.Commodity).ToArray();
            ContractModel.ContractsbyCategory.ContractsCount = Contractsbycategotry.Select(ss => ss.ContractCount).ToArray();
            #endregion

            #region CompletenessbyCategory
            var CompletenessbyCategory = ContractModel.Contract_Tracking
                .GroupBy(ss => ss.commodity_group)
                .Select(grp => new
                {
                    Commodity = grp.Key.name,
                    Completed = grp.Where(ss => ss.completed_count == true).Select(ss => ss.completed_count).Count(),
                    Uncompleted = grp.Where(ss => ss.completed_count == false).Select(ss => ss.completed_count).Count()
                }).OrderBy(ss => ss.Commodity);
            ContractModel.CompletenessbyCategory.CommodityCategory = CompletenessbyCategory.Select(ss => ss.Commodity).ToArray();
            ContractModel.CompletenessbyCategory.CompleteCount = CompletenessbyCategory.Select(ss => ss.Completed).ToArray();
            ContractModel.CompletenessbyCategory.UncompleteCount = CompletenessbyCategory.Select(ss => ss.Uncompleted).ToArray();
            #endregion

            #region AllProducts
            var AllProductsTarget = ContractModel.Contract_Target
                .GroupBy(ss => ss.month.Month)
                .Select(grp => new
                {
                    Month = grp.Key,
                    TargetSUM = (double)grp.Sum(ss => ss.value)
                });
            double AllValueTarget = ContractModel.Contract_Target.Sum(ss => ss.value);
            string AgustusTarget = (AllProductsTarget.Where(ss => ss.Month == 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string SeptemberTarget = (AllProductsTarget.Where(ss => ss.Month >= 8 && ss.Month <= 9).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string OktoberTarget = (AllProductsTarget.Where(ss => ss.Month >= 8 && ss.Month <= 10).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string NovemberTarget = (AllProductsTarget.Where(ss => ss.Month >= 8 && ss.Month <= 11).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string DesemberTarget = (AllProductsTarget.Where(ss => ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JanuariTarget = (AllProductsTarget.Where(ss => ss.Month <= 1 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string FebuariTarget = (AllProductsTarget.Where(ss => ss.Month <= 2 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string MaretTarget = (AllProductsTarget.Where(ss => ss.Month <= 3 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string AprilTarget = (AllProductsTarget.Where(ss => ss.Month <= 4 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string MeiTarget = (AllProductsTarget.Where(ss => ss.Month <= 5 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JuniTarget = (AllProductsTarget.Where(ss => ss.Month != 7).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JuliTarget = (AllProductsTarget.Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            List<string> TargetCalc = new List<string>
            {
                AgustusTarget,
                SeptemberTarget,
                OktoberTarget,
                NovemberTarget,
                DesemberTarget,
                JanuariTarget,
                FebuariTarget,
                MaretTarget,
                AprilTarget,
                MeiTarget,
                JuniTarget,
                JuliTarget
            };
            ContractModel.AllProducts.Target = TargetCalc.ToArray();
            var AllProductsActual = ContractModel.Contract_Actual
                .GroupBy(ss => ss.month.Month)
                .Select(grp => new
                {
                    Month = grp.Key,
                    ActualSUM = (double)grp.Sum(ss => ss.value)
                });
            string AgustusActual = (AllProductsActual.Where(ss => ss.Month == 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string SeptemberActual = (AllProductsActual.Where(ss => ss.Month >= 8 && ss.Month <= 9).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string OktoberActual = (AllProductsActual.Where(ss => ss.Month >= 8 && ss.Month <= 10).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string NovemberActual = (AllProductsActual.Where(ss => ss.Month >= 8 && ss.Month <= 11).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string DesemberActual = (AllProductsActual.Where(ss => ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JanuariActual = (AllProductsActual.Where(ss => ss.Month <= 1 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string FebuariActual = (AllProductsActual.Where(ss => ss.Month <= 2 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string MaretActual = (AllProductsActual.Where(ss => ss.Month <= 3 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string AprilActual = (AllProductsActual.Where(ss => ss.Month <= 4 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string MeiActual = (AllProductsActual.Where(ss => ss.Month <= 5 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JuniActual = (AllProductsActual.Where(ss => ss.Month != 7).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string JuliActual = (AllProductsActual.Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            List<string> ActualCalc = new List<string>
            {
                AgustusActual,
                SeptemberActual,
                OktoberActual,
                NovemberActual,
                DesemberActual,
                JanuariActual,
                FebuariActual,
                MaretActual,
                AprilActual,
                MeiActual,
                JuniActual,
                JuliActual
            };
            ContractModel.AllProducts.Actual = ActualCalc.ToArray();
            #endregion

            #region AllProductsbyQuater
           
            string Q1Target = (AllProductsTarget.Where(ss => ss.Month >= 8 && ss.Month <= 10).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q2Target = (AllProductsTarget.Where(ss => ss.Month <= 1 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q3Target = (AllProductsTarget.Where(ss => ss.Month <= 4 || ss.Month >= 8).Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q4Target = (AllProductsTarget.Sum(ss => ss.TargetSUM) / AllValueTarget).ToString().Replace(",", ".");
            List<string> TargetCountQ = new List<string>
            {
                Q1Target,
                Q2Target,
                Q3Target,
                Q4Target
            };
            ContractModel.AllProductsbyQuarter.Target = TargetCountQ.ToArray();

         
            string Q1Actual = (AllProductsActual.Where(ss => ss.Month >= 8 && ss.Month <= 10).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q2Actual = (AllProductsActual.Where(ss => ss.Month <= 1 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q3Actual = (AllProductsActual.Where(ss => ss.Month <= 4 || ss.Month >= 8).Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            string Q4Actual = (AllProductsActual.Sum(ss => ss.ActualSUM) / AllValueTarget).ToString().Replace(",", ".");
            List<string> ActualCountQ = new List<string>
            {
                Q1Actual,
                Q2Actual,
                Q3Actual,
                Q4Actual
            };
            ContractModel.AllProductsbyQuarter.Actual = ActualCountQ.ToArray();
            #endregion

            #region ContractsValue
            var ContractsValue = ContractModel.Contract_Tracking
                .GroupBy(ss => ss.commodity_group)
                .Select(grp => new
                {
                    Commodity = grp.Key.name,
                    Completed = grp.Where(ss => ss.completed_count == true).Sum(ss => ss.nzd),
                    Uncompleted = grp.Where(ss => ss.completed_count == false).Sum(ss => ss.nzd)
                }).OrderBy(ss => ss.Commodity);
            ContractModel.ContractsValue.CommodityCategory = ContractsValue.Select(ss => ss.Commodity).ToArray();
            ContractModel.ContractsValue.CompleteSUM = ContractsValue.Select(ss => ss.Completed).ToArray();
            ContractModel.ContractsValue.UncompleteSUM = ContractsValue.Select(ss => ss.Uncompleted).ToArray();
            #endregion

            #region YTDTargetvsActual
            ContractModel.YTDTargetvsActual.Target = ContractModel.Contract_Target.Sum(ss => ss.value);
            ContractModel.YTDTargetvsActual.Actual = ContractModel.Contract_Actual.Sum(ss => ss.value);
            #endregion

            #region DatatableContrats
            ContractModel.DatatableContrats = ContractModel.Contract_Tracking.OrderByDescending(ss => ss.nzd).Take(10).ToList();

            #endregion

            return View(ContractModel);
        }

        private ContractGlobalModel Filter(ContractGlobalModel ContractModel)
        {
            if (ContractModel.BuyerFilter != 0)
            {
                ContractModel.Contract_Tracking = ContractModel.Contract_Tracking.Where(ss => ss.buyer.ID_Contract_buyer == ContractModel.BuyerFilter).ToList();
                ContractModel.Contract_Target = ContractModel.Contract_Target.Where(ss => ss.buyer.ID_Contract_buyer == ContractModel.BuyerFilter).ToList();
                ContractModel.Contract_Actual = ContractModel.Contract_Actual.Where(ss => ss.buyer.ID_Contract_buyer == ContractModel.BuyerFilter).ToList();
            }

            if (ContractModel.SpendingTypeFilter == 1)
            {
                ContractModel.Contract_Tracking = ContractModel.Contract_Tracking.Where(ss => ss.direct == true).ToList();
                ContractModel.Contract_Target = ContractModel.Contract_Target.Where(ss => ss.direct == true).ToList();
                ContractModel.Contract_Actual = ContractModel.Contract_Actual.Where(ss => ss.direct == true).ToList();
            }
            else if (ContractModel.SpendingTypeFilter == 0)
            {
                ContractModel.Contract_Tracking = ContractModel.Contract_Tracking.Where(ss => ss.direct == false).ToList();
                ContractModel.Contract_Target = ContractModel.Contract_Target.Where(ss => ss.direct == false).ToList();
                ContractModel.Contract_Actual = ContractModel.Contract_Actual.Where(ss => ss.direct == false).ToList();
            }

            if (ContractModel.CommodityCategoryFilter != 0)
            {
                ContractModel.Contract_Tracking = ContractModel.Contract_Tracking.Where(ss => ss.commodity_group.ID_Contract_commodity_group == ContractModel.CommodityCategoryFilter).ToList();
                ContractModel.Contract_Target = ContractModel.Contract_Target.Where(ss => ss.commodity_group.ID_Contract_commodity_group == ContractModel.CommodityCategoryFilter).ToList();
                ContractModel.Contract_Actual = ContractModel.Contract_Actual.Where(ss => ss.commodity_group.ID_Contract_commodity_group == ContractModel.CommodityCategoryFilter).ToList();
            }

            return ContractModel;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
