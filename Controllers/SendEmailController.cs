﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Mail;
using hera.Models.ViewModels.Email;
using hera.Models;
using System.Linq;
using static hera.Models.ViewModels.Email.SendEmail;
using System.Text;

namespace hera.Controllers
{
    public class SendEmailController : Controller
    {
        private readonly KPIContext db;
        public SendEmailController(KPIContext context)
        {
            db = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            if (ModelState.IsValid)
            {

                SendEmail ViewDatatable1 = new SendEmail();
               ViewDatatable1.EmailUpload1 = (from a in db.EmailTable
                                               select new EmailUpload
                                               {
                                                   ID_Email = a.ID_Email,
                                                   SystemId = a.SystemId,
                                                   po_number = a.VenusRaw.po_number,
                                                   amount = a.VenusRaw.amount,
                                               }).OrderBy(ss => ss.ID_Email).ToList();

                #region dropdownlist
                var ReasonList = new List<string>();
                ReasonList.Add("Choose Reason");
                ReasonList.Add("Trouble Supplier");
                ReasonList.Add("Pending Clearance");
                ReasonList.Add("Pending Document");
                ReasonList.Add("other");
                ViewBag.ReasonList = ReasonList.Select(ss => new { value = ss, text = ss }).ToList();
                #endregion

           
                return View(ViewDatatable1);
            } return View();
        }
        [HttpPost]

        public ActionResult SendEmail1(string receiver, string subject, SendEmail send)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<html>" +
                "<table border=1 >" +
                "<th style = 'font-family: Arial; font-size: 10pt;' > SystemId </th>" +
                 "<th style = 'font-family: Arial; font-size: 10pt;' > Po Number</th>" +
                  "<th style = 'font-family: Arial; font-size: 10pt;' > Amount </th>" +
                   "<th style = 'font-family: Arial; font-size: 10pt;' > Reason </th>"+
                  "<th style = 'font-family: Arial; font-size: 10pt;' > Other Reason </th></tr>");

            for (int i = 0; i < send.EmailUpload1.Count; i++)
            {
                if (send.EmailUpload1[i].ReasonOther !=  "Choose Reason")
                {

                    builder.Append("<tr><th style = 'font-family: Arial; font-size: 10pt;' >" + send.EmailUpload1[i].SystemId + " </th>" +
                        "<th style = 'font-family: Arial; font-size: 10pt;' >" + send.EmailUpload1[i].po_number + " </th>" +
                        "<th style = 'font-family: Arial; font-size: 10pt;' >" + send.EmailUpload1[i].amount + " </th>" +
                        "<th style = 'font-family: Arial; font-size: 10pt;' >" + send.EmailUpload1[i].ReasonOther + " </th>");
               
            if (send.EmailUpload1[i].ReasonOther == "other")
                    {
                        builder.Append("<th style = 'font-family: Arial; font-size: 10pt;' >" + send.EmailUpload1[i].otherText + "</th>");
                  }
                    else
                    {
                        builder.Append("<th style = 'font-family: Arial; font-size: 10pt;'> ------ </th>");
        }
                }
                
            }
            builder.Append("</tr></table></html>");
            string bodi = builder.ToString();
            try
                {
                    if (ModelState.IsValid)
                    {
                        var senderEmail = new MailAddress("dawahyuh@student.ce.undip.ac.id", "adminfrontera");
                        var receiverEmail = new MailAddress(receiver, "Receiver");
                        var password = "anjing123";
                        var sub = subject;
                        var body = bodi;


                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(senderEmail.Address, password)
                        };
                        using (var mess = new MailMessage(senderEmail, receiverEmail)
                        {
                            IsBodyHtml = true,
                            Subject = subject,
                            Body = bodi,

                        })
                        {
                            smtp.Send(mess);
                        }
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    ViewBag.Error = "Some Error";
                }
                return RedirectToAction("Index");
            }


        }
    }
    