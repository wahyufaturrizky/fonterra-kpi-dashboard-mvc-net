﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using hera.Models;
using hera.Models.ViewModels.Contracts;
using hera.Models.ViewModels.Venus;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class TableContractController : Controller
    {
        private readonly KPIContext db;
        public TableContractController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index(int? buyerpass, int? spendingtypepass, int? commoditypass)
        {
            TableContractGlobalModel TableContractGlobalModel = new TableContractGlobalModel();
            TableContractGlobalModel.BuyerFilter = buyerpass == null ? 0 : (int)buyerpass;
            TableContractGlobalModel.SpendingTypeFilter = spendingtypepass == null ? 2 : (int)spendingtypepass;
            TableContractGlobalModel.CommodityCategoryFilter = commoditypass == null ? 0 : (int)commoditypass;

            TableContractGlobalModel.BuyerRadio = (from a in db.Contract_buyer select a).OrderBy(ss => ss.ID_Contract_buyer).ToList(); ;
            TableContractGlobalModel.CommodityCategoryRadio = (from a in db.Contract_commodity_group select a).OrderBy(ss => ss.ID_Contract_commodity_group).ToList();

            TableContractGlobalModel.ContractTable = (from a in db.Contract_tracking
                                                      select new ContractGridTable
                                                      {
                                                          checkID = false,
                                                          ID_Contract_tracking = a.ID_Contract_tracking,
                                                          supplier = a.supplier,
                                                          supplier_ID = a.supplier_ID,
                                                          direct = a.direct == true ? "Yes" : "No",
                                                          commodity_groupID = a.commodity_group.ID_Contract_commodity_group,
                                                          commodity_group = a.commodity_group.name,
                                                          supply_categoryID = a.supply_category.ID_Contract_supply_category,
                                                          supply_category = a.supply_category.name,
                                                          nzd = a.nzd,
                                                          contract_ID = a.contract_ID,
                                                          expiry_date = a.expiry_date,
                                                          buyerID = a.buyer.ID_Contract_buyer,
                                                          buyer = a.buyer.name,
                                                          period_coverageID = a.period_coverage.ID_Contract_period_coverage,
                                                          period_coverage = a.period_coverage.period,
                                                          completed_count = a.completed_count == true ? "Completed" : "Uncompleted",
                                                          remarks_last_week = a.remarks_last_week,
                                                          remarks_this_week = a.remarks_this_week,
                                                           directBOOL = a.direct
                                                      }).OrderBy(ss => ss.ID_Contract_tracking).ToList();
            TableContractGlobalModel = Filter(TableContractGlobalModel);

            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableContractGlobalModel);
        }

        [HttpGet]
        public IActionResult Input(long? id)
        {
            TableContractGlobalModel TableContractGlobalModel = new TableContractGlobalModel();
            if (id != null && id != 0)
            {
                TableContractGlobalModel.ContractInput.Form = "Edit";

                var Contract_tracking = (from a in db.Contract_tracking
                                          where a.ID_Contract_tracking == id
                                          select new 
                                          {
                                              ID_Contract_tracking = a.ID_Contract_tracking,
                                              supplier = a.supplier,
                                              supplier_ID = a.supplier_ID,
                                              direct = a.direct,
                                              commodity_group = a.commodity_group.ID_Contract_commodity_group,
                                              supply_category = a.supply_category.ID_Contract_supply_category,
                                              nzd = a.nzd,
                                              contract_ID = a.contract_ID,
                                              expiry_date = a.expiry_date,
                                              buyer = a.buyer.ID_Contract_buyer,
                                              period_coverage = a.period_coverage.ID_Contract_period_coverage,
                                              completed_count = a.completed_count,
                                              remarks_last_week = a.remarks_last_week,
                                              remarks_this_week = a.remarks_this_week
                                          }).FirstOrDefault();

                TableContractGlobalModel.ContractInput.ID_Contract_tracking = Contract_tracking.ID_Contract_tracking;
                TableContractGlobalModel.ContractInput.supplier = Contract_tracking.supplier;
                TableContractGlobalModel.ContractInput.supplier_ID = Contract_tracking.supplier_ID;
                TableContractGlobalModel.ContractInput.direct = Contract_tracking.direct;
                TableContractGlobalModel.ContractInput.commodity_group = Contract_tracking.commodity_group;
                TableContractGlobalModel.ContractInput.supply_category = Contract_tracking.supply_category;
                TableContractGlobalModel.ContractInput.nzd = Contract_tracking.nzd;
                TableContractGlobalModel.ContractInput.contract_ID = Contract_tracking.contract_ID;
                TableContractGlobalModel.ContractInput.expiry_date = Contract_tracking.expiry_date;
                TableContractGlobalModel.ContractInput.buyer = Contract_tracking.buyer;
                TableContractGlobalModel.ContractInput.period_coverage = Contract_tracking.period_coverage;
                TableContractGlobalModel.ContractInput.completed_count = Contract_tracking.completed_count;
                TableContractGlobalModel.ContractInput.remarks_last_week = Contract_tracking.remarks_last_week;
                TableContractGlobalModel.ContractInput.remarks_this_week = Contract_tracking.remarks_this_week;
            }
            else
            {
                TableContractGlobalModel.ContractInput = new TableContract();
                TableContractGlobalModel.ContractInput.Form = "Create";
            }

            #region dropdownlist
            ViewBag.commodity_group = db.Contract_commodity_group.Select(ss => new { value = ss.ID_Contract_commodity_group, text = ss.name }).ToList();
            ViewBag.supply_category = db.Contract_supply_category.Select(ss => new { value = ss.ID_Contract_supply_category, text = ss.name }).ToList();
            ViewBag.buyer = db.Contract_buyer.Select(ss => new { value = ss.ID_Contract_buyer, text = ss.name }).ToList();
            ViewBag.period_coverage = db.Contract_period_coverage.Select(ss => new { value = ss.ID_Contract_period_coverage, text = ss.period }).ToList();
            var StatusVal = new List<string>();
            StatusVal.Add("True");
            StatusVal.Add("False");
            ViewBag.StatusVal = StatusVal.Select(ss => new { value = ss, text = ss }).ToList();
            #endregion

            return PartialView("InputPopup", TableContractGlobalModel);
        }

        [HttpPost]
        public IActionResult Input(TableContractGlobalModel TableContractGlobalModel)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id = TableContractGlobalModel.ContractInput.ID_Contract_tracking;
                if (id != null && id != 0)
                {
                    Contract_tracking toUpdate = db.Contract_tracking.Find(id);
                    toUpdate.supplier = TableContractGlobalModel.ContractInput.supplier;
                    toUpdate.supplier_ID = TableContractGlobalModel.ContractInput.supplier_ID;
                    toUpdate.direct = TableContractGlobalModel.ContractInput.direct;
                    toUpdate.commodity_group = Contract_commodity_group(TableContractGlobalModel.ContractInput.commodity_group);
                    toUpdate.supply_category = Contract_supply_category(TableContractGlobalModel.ContractInput.supply_category);
                    toUpdate.nzd = TableContractGlobalModel.ContractInput.nzd;
                    toUpdate.contract_ID = TableContractGlobalModel.ContractInput.contract_ID;
                    toUpdate.expiry_date = TableContractGlobalModel.ContractInput.expiry_date;
                    toUpdate.buyer = Contract_buyer(TableContractGlobalModel.ContractInput.buyer);
                    toUpdate.period_coverage = Contract_period_coverage(TableContractGlobalModel.ContractInput.period_coverage);
                    toUpdate.completed_count = TableContractGlobalModel.ContractInput.completed_count;
                    toUpdate.remarks_last_week = TableContractGlobalModel.ContractInput.remarks_last_week;
                    toUpdate.remarks_this_week = TableContractGlobalModel.ContractInput.remarks_this_week;

                    dbContextTransaction.Commit();
                    db.Entry(toUpdate).State = EntityState.Modified;
                    db.SaveChanges();

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
                else
                {
                    Contract_tracking toAdd = new Contract_tracking();
                    toAdd.supplier = TableContractGlobalModel.ContractInput.supplier;
                    toAdd.supplier_ID = TableContractGlobalModel.ContractInput.supplier_ID;
                    toAdd.direct = TableContractGlobalModel.ContractInput.direct;
                    toAdd.commodity_group = Contract_commodity_group(TableContractGlobalModel.ContractInput.commodity_group);
                    toAdd.supply_category = Contract_supply_category(TableContractGlobalModel.ContractInput.supply_category);
                    toAdd.nzd = TableContractGlobalModel.ContractInput.nzd;
                    toAdd.contract_ID = TableContractGlobalModel.ContractInput.contract_ID;
                    toAdd.expiry_date = TableContractGlobalModel.ContractInput.expiry_date;
                    toAdd.buyer = Contract_buyer(TableContractGlobalModel.ContractInput.buyer);
                    toAdd.period_coverage = Contract_period_coverage(TableContractGlobalModel.ContractInput.period_coverage);
                    toAdd.completed_count = TableContractGlobalModel.ContractInput.completed_count;
                    toAdd.remarks_last_week = TableContractGlobalModel.ContractInput.remarks_last_week;
                    toAdd.remarks_this_week = TableContractGlobalModel.ContractInput.remarks_this_week;

                    dbContextTransaction.Commit();
                    db.Contract_tracking.Add(toAdd);
                    db.SaveChanges();

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            TableContractGlobalModel TableContractGlobalModel = new TableContractGlobalModel();
            Contract_tracking Contract_tracking = db.Contract_tracking.Find(id);
            TableContractGlobalModel.ContractInput.ID_Contract_tracking = Contract_tracking.ID_Contract_tracking;

            TableContractGlobalModel.URLAction = "Delete";
            TableContractGlobalModel.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup", TableContractGlobalModel);
        }

        [HttpPost]
        public IActionResult Delete(TableContractGlobalModel TableContractGlobalModel)
        {
            long? id = TableContractGlobalModel.ContractInput.ID_Contract_tracking;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                Contract_tracking toDelete = db.Contract_tracking.Find(id);

                dbContextTransaction.Commit();
                db.Contract_tracking.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TableContractGlobalModel TableContractGlobalModel = new TableContractGlobalModel();
            if (!String.IsNullOrEmpty(id))
            {
                TableContractGlobalModel.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TableContractGlobalModel.idDeleteString = id;
            }
            var howmanyrow = TableContractGlobalModel.idDeleteArray != null ? TableContractGlobalModel.idDeleteArray.Count() : 0;

            TableContractGlobalModel.URLAction = "DeleteAll";
            TableContractGlobalModel.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TableContractGlobalModel);
        }

        [HttpPost]
        public IActionResult DeleteAll(TableContractGlobalModel TableContractGlobalModel)
        {
            try
            {
                if (!String.IsNullOrEmpty(TableContractGlobalModel.idDeleteString))
                {
                    TableContractGlobalModel.idDeleteArray = JsonConvert.DeserializeObject<string[]>(TableContractGlobalModel.idDeleteString);
                }
                var howmanyrow = TableContractGlobalModel.idDeleteArray != null ? TableContractGlobalModel.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in TableContractGlobalModel.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);

                    Contract_tracking toDelete = db.Contract_tracking.Find(idDelete);

                    db.Contract_tracking.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        private TableContractGlobalModel Filter(TableContractGlobalModel TableContractGlobalModel)
        {
            if (TableContractGlobalModel.BuyerFilter != 0)
            {
                TableContractGlobalModel.ContractTable = TableContractGlobalModel.ContractTable.Where(ss => ss.buyerID == TableContractGlobalModel.BuyerFilter).ToList();
            }

            if (TableContractGlobalModel.SpendingTypeFilter == 1)
            {
                TableContractGlobalModel.ContractTable = TableContractGlobalModel.ContractTable.Where(ss => ss.directBOOL == true).ToList();
            }
            else if (TableContractGlobalModel.SpendingTypeFilter == 0)
            {
                TableContractGlobalModel.ContractTable = TableContractGlobalModel.ContractTable.Where(ss => ss.directBOOL == false).ToList();
            }

            if (TableContractGlobalModel.CommodityCategoryFilter != 0)
            {
                TableContractGlobalModel.ContractTable = TableContractGlobalModel.ContractTable.Where(ss => ss.commodity_groupID == TableContractGlobalModel.CommodityCategoryFilter).ToList();
            }

            return TableContractGlobalModel;
        }

        private Contract_commodity_group Contract_commodity_group(long Value)
        {
            Contract_commodity_group List = new Contract_commodity_group();
            List = db.Contract_commodity_group.Where(ss => ss.ID_Contract_commodity_group == Value).FirstOrDefault();
            return List;
        }
        private Contract_supply_category Contract_supply_category(long Value)
        {
            Contract_supply_category List = new Contract_supply_category();
            List = db.Contract_supply_category.Where(ss => ss.ID_Contract_supply_category == Value).FirstOrDefault();
            return List;
        }
        private Contract_buyer Contract_buyer(long Value)
        {
            Contract_buyer List = new Contract_buyer();
            List = db.Contract_buyer.Where(ss => ss.ID_Contract_buyer == Value).FirstOrDefault();
            return List;
        }
        private Contract_period_coverage Contract_period_coverage(long Value)
        {
            Contract_period_coverage List = new Contract_period_coverage();
            List = db.Contract_period_coverage.Where(ss => ss.ID_Contract_period_coverage == Value).FirstOrDefault();
            return List;
        }
    }
}