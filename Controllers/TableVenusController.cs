﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Venus;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class TableVenusController : Controller
    {
        private readonly KPIContext db;
        public TableVenusController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index(string potypepass)
        {
            TableVenusGlobalModel TableVenusGlobalModel = new TableVenusGlobalModel();

            TableVenusGlobalModel.POTypeFilterArray = new string[] { };
            if (!String.IsNullOrEmpty(potypepass))
            {
                TableVenusGlobalModel.POTypeFilterArray = JsonConvert.DeserializeObject<string[]>(potypepass);
            }

            TableVenusGlobalModel.POTypeFilter = (from a in db.Venus_po_type
                                                  select new POTypeFilter
                                                  {
                                                      ID_Venus_po_type = a.ID_Venus_po_type,
                                                      name = a.name,
                                                      IsCheckedPOType = true
                                                  }
                                       ).OrderBy(ss => ss.ID_Venus_po_type).ToList();

            TableVenusGlobalModel = Filter(TableVenusGlobalModel);

            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableVenusGlobalModel);
        }

        [HttpGet]
        public IActionResult Input(long? id)
        {
            TableVenusGlobalModel TableVenusGlobalModel = new TableVenusGlobalModel();
            if (id != null && id != 0)
            {
                TableVenusGlobalModel.GNRIInput.Form = "Edit";

                var Venus_grni_raw = (from a in db.Venus_grni_raw
                                      join b in db.Venus_grni on a.ID_Venus_grni_raw equals b.ID_Venus_grni
                                      where a.ID_Venus_grni_raw == id
                                      select new
                                      {
                                          ID_Venus_grni_raw = a.ID_Venus_grni_raw,
                                          amount = a.amount,
                                          po_type = a.po_type,
                                          po_number = a.po_number,
                                          doc_type = a.doc_type,
                                          doc_number = a.doc_number,
                                          doc_date = a.doc_date,
                                          business_unit = b.business_unit.ID_Venus_business_unit,
                                          department = b.department.ID_Venus_department,
                                          vendor = b.vendor.ID_Venus_vendor,

                                          supplier_code = a.supplier_code,
                                          supplier_name = a.supplier_name,
                                          remark = a.remark,
                                          po_currency = a.po_currency,
                                          po_typeID_Venus_po_type = b.po_typeID_Venus_po_type,
                                          unit = a.unit,
                                          minimum = a.batas_bawah,
                                          maksimum = a.batas_atas,
                                          pic = b.pic,

                                      }).FirstOrDefault();

                TableVenusGlobalModel.GNRIInput.department = Convert.ToInt32(Venus_grni_raw.department);
                TableVenusGlobalModel.GNRIInput.vendor = Convert.ToInt32(Venus_grni_raw.vendor);
                TableVenusGlobalModel.GNRIInput.po_type = Venus_grni_raw.po_type;
                TableVenusGlobalModel.GNRIInput.businessunit = Convert.ToInt32(Venus_grni_raw.business_unit);
                TableVenusGlobalModel.GNRIInput.ID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
             
            
                TableVenusGlobalModel.GNRIInput.po_number = Venus_grni_raw.po_number;
                TableVenusGlobalModel.GNRIInput.doc_type = Venus_grni_raw.doc_type;
                TableVenusGlobalModel.GNRIInput.doc_number = Venus_grni_raw.doc_number;
                TableVenusGlobalModel.GNRIInput.doc_date = Venus_grni_raw.doc_date;
                TableVenusGlobalModel.GNRIInput.supplier_code = Venus_grni_raw.supplier_code;
                TableVenusGlobalModel.GNRIInput.supplier_name = Venus_grni_raw.supplier_name;
                TableVenusGlobalModel.GNRIInput.remark = Venus_grni_raw.remark;
                TableVenusGlobalModel.GNRIInput.po_currency = Venus_grni_raw.po_currency;
                TableVenusGlobalModel.GNRIInput.amount = Venus_grni_raw.amount;
                TableVenusGlobalModel.GNRIInput.unit = Venus_grni_raw.unit;
                TableVenusGlobalModel.GNRIInput.pic = Venus_grni_raw.pic;

            }
            else
            {
                TableVenusGlobalModel.GNRIInput = new TableVenusGNRI();
                TableVenusGlobalModel.GNRIInput.Form = "Create";
            }

            #region dropdownlist
            ViewBag.POType = db.Venus_po_type.Select(ss => new { value = ss.name, text = ss.name }).ToList();
            ViewBag.BusinessUnit = db.Venus_business_unit.Select(ss => new { value = ss.ID_Venus_business_unit, text = ss.name }).ToList();
            ViewBag.Department = db.Venus_department.Select(ss => new { value = ss.ID_Venus_department, text = ss.name }).ToList();
            ViewBag.Vendor = db.Venus_vendor.Select(ss => new { value = ss.ID_Venus_vendor, text = ss.name }).ToList();
            var DocType = new List<string>();
            DocType.Add("PV");
            DocType.Add("OV");
            ViewBag.DocType = DocType.Select(ss => new { value = ss, text = ss }).ToList();
            #endregion

            return PartialView("InputPopup", TableVenusGlobalModel);
        }

        [HttpPost]
        public IActionResult Input(TableVenusGlobalModel TableVenusGlobalModel)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id = TableVenusGlobalModel.GNRIInput.ID_Venus_grni_raw;
                if (id != null && id != 0)
                {
                    Venus_grni_raw toUpdate = db.Venus_grni_raw.Find(id);
                    toUpdate.po_type = TableVenusGlobalModel.GNRIInput.po_type;
                    toUpdate.po_number = TableVenusGlobalModel.GNRIInput.po_number;
                    toUpdate.doc_type = TableVenusGlobalModel.GNRIInput.doc_type;
                    toUpdate.doc_number = TableVenusGlobalModel.GNRIInput.doc_number;
                    toUpdate.doc_date = TableVenusGlobalModel.GNRIInput.doc_date;
                    toUpdate.supplier_code = TableVenusGlobalModel.GNRIInput.supplier_code;
                    toUpdate.supplier_name = TableVenusGlobalModel.GNRIInput.supplier_name;
                    toUpdate.remark = TableVenusGlobalModel.GNRIInput.remark;
                    toUpdate.po_currency = TableVenusGlobalModel.GNRIInput.po_currency;
                    toUpdate.amount = TableVenusGlobalModel.GNRIInput.amount;
                    toUpdate.batas_bawah = TableVenusGlobalModel.GNRIInput.amount - ((float)Math.Abs(0.05 * TableVenusGlobalModel.GNRIInput.amount));
                    toUpdate.batas_atas = TableVenusGlobalModel.GNRIInput.amount + ((float)Math.Abs(0.05 * TableVenusGlobalModel.GNRIInput.amount));
                    toUpdate.pairID_Venus_grni_raw = 0;

                    toUpdate.unit = TableVenusGlobalModel.GNRIInput.unit;

                    dbContextTransaction.Commit();
                    db.Entry(toUpdate).State = EntityState.Modified;
                    db.SaveChanges();
         
                    Venus_grni toUpdate3 = db.Venus_grni.Find(TableVenusGlobalModel.GNRIInput.ID_Venus_grni_raw);
                    var updatePairnya = db.Venus_grni_raw.Where(x => x.pairID_Venus_grni_raw == id).FirstOrDefault();

                    if (updatePairnya != null)
                    {
                        Venus_grni_raw updatePair = db.Venus_grni_raw.Find(updatePairnya.ID_Venus_grni_raw);
                        Venus_grni updatePairgrni = db.Venus_grni.Find(updatePair.ID_Venus_grni_raw);

                        updatePair.pairID_Venus_grni_raw = 0;
                        toUpdate.pairID_Venus_grni_raw = 0;
                        updatePairgrni.status = "open";

                        db.Entry(toUpdate).State = EntityState.Modified;

                        db.Entry(updatePair).State = EntityState.Modified;
                        db.Entry(updatePairgrni).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    Venus_po_type po_type = Venus_po_type(TableVenusGlobalModel.GNRIInput.po_type.ToString().Trim());
                    Venus_department department = Venus_department(TableVenusGlobalModel.GNRIInput.department.ToString().Trim());
                    Venus_vendor vendor = Venus_vendor(TableVenusGlobalModel.GNRIInput.vendor.ToString().Trim());
                    Venus_business_unit business_unit = Venus_business_unit(TableVenusGlobalModel.GNRIInput.businessunit.ToString().Trim());
                    toUpdate3.po_number = TableVenusGlobalModel.GNRIInput.po_number;
                    toUpdate3.vendor = vendor; 
                    toUpdate3.po_type = po_type;
                    toUpdate3.department = department;
                    toUpdate3.business_unit = business_unit;
                    toUpdate3.status = "open";
                    toUpdate3.month = TableVenusGlobalModel.GNRIInput.doc_date;
                    toUpdate3.pic = TableVenusGlobalModel.GNRIInput.pic;
                    TimeSpan date_po = DateTime.Now - TableVenusGlobalModel.GNRIInput.doc_date;
                    toUpdate3.po_age = date_po.Days;
                    toUpdate3.remarks = TableVenusGlobalModel.GNRIInput.remark;
                    toUpdate3.amount = Convert.ToInt64(TableVenusGlobalModel.GNRIInput.amount);
                    db.Entry(toUpdate3).State = EntityState.Modified;
                    db.SaveChanges();
                    TableVenusGlobalModel.GNRIInput.statusAksi = "edit";
                    cekPair(toUpdate, toUpdate3, TableVenusGlobalModel);

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
                else
                {
                    Venus_grni_raw toAdd = new Venus_grni_raw();
                    toAdd.po_type = TableVenusGlobalModel.GNRIInput.po_type;
                    toAdd.po_number = TableVenusGlobalModel.GNRIInput.po_number;
                    toAdd.doc_type = TableVenusGlobalModel.GNRIInput.doc_type;
                    toAdd.doc_number = TableVenusGlobalModel.GNRIInput.doc_number;
                    toAdd.doc_date = TableVenusGlobalModel.GNRIInput.doc_date;
                    toAdd.supplier_code = TableVenusGlobalModel.GNRIInput.supplier_code;
                    toAdd.supplier_name = TableVenusGlobalModel.GNRIInput.supplier_name;
                    toAdd.remark = TableVenusGlobalModel.GNRIInput.remark;
                    toAdd.po_currency = TableVenusGlobalModel.GNRIInput.po_currency;
                    toAdd.amount = TableVenusGlobalModel.GNRIInput.amount;
                    toAdd.status_add_email = 0;
                    toAdd.batas_bawah = TableVenusGlobalModel.GNRIInput.amount - ((float)Math.Abs(0.05 * TableVenusGlobalModel.GNRIInput.amount));

                    toAdd.batas_atas = TableVenusGlobalModel.GNRIInput.amount + ((float)Math.Abs(0.05 * TableVenusGlobalModel.GNRIInput.amount));
                    toAdd.unit = TableVenusGlobalModel.GNRIInput.unit;

                    dbContextTransaction.Commit();
                    db.Venus_grni_raw.Add(toAdd);
                    db.SaveChanges();

                    InsertGRNI(toAdd, TableVenusGlobalModel);

                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }
        public void cekPair(Venus_grni_raw Venus_grni_raw, Venus_grni toAdd, TableVenusGlobalModel TableVenusGlobalModel)
        {


            List<Venus_grni_raw> CheckList = new List<Venus_grni_raw>();
            List<Venus_grni_raw> CheckList1 = new List<Venus_grni_raw>();
            CheckList = (from a in db.Venus_grni_raw
                         where (a.doc_type == "OV")
                         && a.po_type == Venus_grni_raw.po_type
                         && (a.amount >= Venus_grni_raw.batas_bawah && a.amount <= Venus_grni_raw.batas_atas)
                          select a).ToList();
            CheckList1 = (from a in db.Venus_grni_raw
                          where (a.doc_type == "PV")
                          && a.po_type == Venus_grni_raw.po_type
                           && (a.amount >= Venus_grni_raw.batas_bawah && a.amount <= Venus_grni_raw.batas_atas)
                          select a).ToList();



            string statusAksi = TableVenusGlobalModel.GNRIInput.statusAksi;
            if (Venus_grni_raw.doc_type == "OV")
            {


                if ( CheckList1.Count > 0)
                {
                        var getID = db.Venus_grni_raw.Where(x => x.doc_type == "PV").Where(
                            x => x.po_type == Venus_grni_raw.po_type).Where(
                             x => x.amount >= Venus_grni_raw.batas_bawah)
                            .Where(x => x.amount <= Venus_grni_raw.batas_atas).First();

                        Venus_grni updateStatus = db.Venus_grni.Find(getID.ID_Venus_grni_raw);
                        Venus_grni_raw toUpdate1 = db.Venus_grni_raw.Find(getID.ID_Venus_grni_raw);

                        if (statusAksi == "add")
                        {

                            toAdd.status = "close";
                            updateStatus.status = "close";
                            toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                            Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                            toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                            db.Entry(toUpdate2).State = EntityState.Modified;
                            db.Entry(updateStatus).State = EntityState.Modified;
                        db.Entry(toUpdate1).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else if (statusAksi == "edit")
                        {
                            toAdd.status = "close";
                            updateStatus.status = "close";
                            toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                            Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                            toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                            db.Entry(toUpdate2).State = EntityState.Modified;
                            db.Entry(updateStatus).State = EntityState.Modified;
                            db.Entry(toUpdate1).State = EntityState.Modified;
                            db.Entry(toAdd).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else if (statusAksi == "hapus")
                        {

                            updateStatus.status = "open";
                            toUpdate1.pairID_Venus_grni_raw = 0;

                            db.Entry(updateStatus).State = EntityState.Modified;
                            db.Entry(toUpdate1).State = EntityState.Modified;

                            db.SaveChanges();
                        }
                    }
            }
            if (Venus_grni_raw.doc_type == "PV")
            {


                if (CheckList.Count > 0)
                {
                 
                        var getID = db.Venus_grni_raw.Where(x => x.doc_type == "OV").Where(
                            x => x.po_type == Venus_grni_raw.po_type).Where(
                             x => x.amount >= Venus_grni_raw.batas_bawah)
                            .Where(x => x.amount <= Venus_grni_raw.batas_atas).First();


                        Venus_grni updateStatus = db.Venus_grni.Find(getID.ID_Venus_grni_raw);
                        Venus_grni_raw toUpdate1 = db.Venus_grni_raw.Find(getID.ID_Venus_grni_raw);
                        if (statusAksi == "add")
                        {

                            toAdd.status = "close";
                            updateStatus.status = "close";
                            toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                            Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                            toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                            db.Entry(toUpdate2).State = EntityState.Modified;
                            db.Entry(updateStatus).State = EntityState.Modified;
                            db.Entry(toUpdate1).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else if (statusAksi == "edit")
                        {
                            toAdd.status = "close";
                            updateStatus.status = "close";
                            toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                            Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                            toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                            db.Entry(toUpdate2).State = EntityState.Modified;
                            db.Entry(updateStatus).State = EntityState.Modified;
                            db.Entry(toUpdate1).State = EntityState.Modified;
                            db.Entry(toAdd).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        else if (statusAksi == "hapus")
                        {

                            updateStatus.status = "open";
                            toUpdate1.pairID_Venus_grni_raw = 0;

                            db.Entry(updateStatus).State = EntityState.Modified;
                            db.Entry(toUpdate1).State = EntityState.Modified;

                            db.SaveChanges();
                        }
                    }
            }

        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            TableVenusGlobalModel TableVenusGlobalModel = new TableVenusGlobalModel();
            Venus_grni_raw Venus_grni_raw = new Venus_grni_raw();
            Venus_grni_raw = db.Venus_grni_raw.Find(id);
            TableVenusGlobalModel.GNRIInput.ID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;

            TableVenusGlobalModel.URLAction = "Delete";
            TableVenusGlobalModel.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup", TableVenusGlobalModel);
        }

        [HttpPost]
        public IActionResult Delete(TableVenusGlobalModel TableVenusGlobalModel)
        {
            long? id = TableVenusGlobalModel.GNRIInput.ID_Venus_grni_raw;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                Venus_grni_raw toDelete = db.Venus_grni_raw.Find(id);
                Venus_grni toDelete1 = db.Venus_grni.Find(id);
                TableVenusGlobalModel.GNRIInput.statusAksi = "hapus";
                cekPair(toDelete, toDelete1, TableVenusGlobalModel);
             

                dbContextTransaction.Commit();
                db.Venus_grni_raw.Remove(toDelete);
                db.Venus_grni.Remove(toDelete1);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult AddEmail(long? id)
        {
            TableVenusGlobalModel addemail = new TableVenusGlobalModel();
            Venus_grni_raw venus_grni_raw = new Venus_grni_raw();
            venus_grni_raw = db.Venus_grni_raw.Find(id);
            addemail.GNRIInput.ID_Venus_grni_raw = venus_grni_raw.ID_Venus_grni_raw;
            addemail.URLAction = "AddEmail";
            addemail.AddEmailMessage = "Are you sure move this item to draft send email ?";
            return PartialView("AddEmailPopup", addemail);
        }
        [HttpPost]
        public IActionResult AddEmail(TableVenusGlobalModel addemail)
        {
            long? id = addemail.GNRIInput.ID_Venus_grni_raw;
            var dbContextTransaction = db.Database.BeginTransaction();
            try
            {
                Venus_grni_raw add = db.Venus_grni_raw.Find(id);
                add.status_add_email = 1;
                EmailTable send = new EmailTable();
                send.SystemId = add.ID_Venus_grni_raw;
                dbContextTransaction.Commit();
                db.EmailTable.Add(send);
                db.Entry(add).State = EntityState.Modified;

                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Updating row";

            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }



        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TableVenusGlobalModel TableVenusGlobalModel = new TableVenusGlobalModel();
            if (!String.IsNullOrEmpty(id))
            {
                TableVenusGlobalModel.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TableVenusGlobalModel.idDeleteString = id;
            }
            var howmanyrow = TableVenusGlobalModel.idDeleteArray != null ? TableVenusGlobalModel.idDeleteArray.Count() : 0;

            TableVenusGlobalModel.URLAction = "DeleteAll";
            TableVenusGlobalModel.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TableVenusGlobalModel);
        }

        [HttpPost]
        public IActionResult DeleteAll(TableVenusGlobalModel TableVenusGlobalModel)
        {
            try
            {
                if (!String.IsNullOrEmpty(TableVenusGlobalModel.idDeleteString))
                {
                    TableVenusGlobalModel.idDeleteArray = JsonConvert.DeserializeObject<string[]>(TableVenusGlobalModel.idDeleteString);
                }
                var howmanyrow = TableVenusGlobalModel.idDeleteArray != null ? TableVenusGlobalModel.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in TableVenusGlobalModel.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);

                    Venus_grni_raw toDelete = db.Venus_grni_raw.Find(idDelete);
                    Venus_grni toDelete1 = db.Venus_grni.Find(idDelete);
                    TableVenusGlobalModel.GNRIInput.statusAksi = "hapus";
                    cekPair(toDelete, toDelete1, TableVenusGlobalModel);
                    db.Venus_grni_raw.Remove(toDelete);
                    db.Venus_grni.Remove(toDelete1);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        private TableVenusGlobalModel Filter(TableVenusGlobalModel TableVenusGlobalModel)
        {
            if (TableVenusGlobalModel.POTypeFilterArray != null && TableVenusGlobalModel.POTypeFilterArray.Length > 0)
            {
                TableVenusGlobalModel.GNRITable = (from a in db.Venus_grni_raw
                                                   join b in db.Venus_grni on a.ID_Venus_grni_raw equals b.ID_Venus_grni

                                                   where TableVenusGlobalModel.POTypeFilterArray.Contains(a.po_type)
                                                   select new GNRIGridTable
                                                   {
                                                       checkID = false,
                                                       ID_Venus_grni_raw = a.ID_Venus_grni_raw,
                                                       po_type = a.po_type,
                                                       po_number = a.po_number,
                                                       doc_type = a.doc_type,
                                          
                                                       doc_number = a.doc_number,
                                                       doc_date = a.doc_date,
                                                       business_unit = b.business_unit.name,
                                                       department = b.department.name,
                                                       vendor = b.vendor.name,
                                                       status_add_email = a.status_add_email,
                                                       supplier_code = a.supplier_code,
                                                       supplier_name = a.supplier_name,
                                                       remark = a.remark,
                                                       po_currency = a.po_currency,
                                                       amount = a.amount,
                                                       unit = a.unit,
                                                       minimum = a.batas_bawah,
                                                       maksimum = a.batas_atas,
                                                       pic = b.pic,
                                                       po_age = b.po_age,
                                                       pairID_Venus_grni_raw = a.pairID_Venus_grni_raw,
                                                       status = b.status
                                                   }
                                                   ).OrderBy(ss => ss.ID_Venus_grni_raw).ToList();
                TableVenusGlobalModel.POTypeFilter = (from a in db.Venus_po_type
                                                   
                                                      select new POTypeFilter
                                                      {
                                                          ID_Venus_po_type = a.ID_Venus_po_type,
                                                          name = a.name,
                                                          IsCheckedPOType = TableVenusGlobalModel.POTypeFilterArray.Contains(a.name) ? true : false
                                                      }
                                                      ).OrderBy(ss => ss.ID_Venus_po_type).ToList();
            }
            else
            {
                TableVenusGlobalModel.GNRITable = (from a in db.Venus_grni_raw
                                                   join b in db.Venus_grni on a.ID_Venus_grni_raw equals b.ID_Venus_grni

                                                   select new GNRIGridTable
                                                   {
                                                       checkID = false,
                                                       ID_Venus_grni_raw = a.ID_Venus_grni_raw,
                                                       po_type = a.po_type,
                                                       po_number = a.po_number,
                                                       doc_type = a.doc_type,

                                                       doc_number = a.doc_number,
                                                       doc_date = a.doc_date,
                                                       business_unit = b.business_unit.name,
                                                       department = b.department.name,
                                                       vendor = b.vendor.name,
                                                       status_add_email = a.status_add_email,
                                                       supplier_code = a.supplier_code,
                                                       supplier_name = a.supplier_name,
                                                       remark = a.remark,
                                                       po_currency = a.po_currency,
                                                       amount = a.amount,
                                                       unit = a.unit,
                                                       minimum = a.batas_bawah,
                                                       maksimum = a.batas_atas,
                                                       pic = b.pic,
                                                       po_age = b.po_age,
                                                       pairID_Venus_grni_raw = a.pairID_Venus_grni_raw,
                                                       status = b.status
                                                   }
                                                   ).OrderBy(ss => ss.ID_Venus_grni_raw).ToList();
            }

            return TableVenusGlobalModel;
        }

        private void InsertGRNI(Venus_grni_raw Venus_grni_raw, TableVenusGlobalModel TableVenusGlobalModel)
        {
            Venus_grni toAdd = new Venus_grni();
            Venus_po_type po_type = Venus_po_type(TableVenusGlobalModel.GNRIInput.po_type.ToString().Trim());
            toAdd.po_number = Venus_grni_raw.po_number;
            toAdd.vendorID_Venus_vendor = TableVenusGlobalModel.GNRIInput.vendor;

            toAdd.departmentID_Venus_department = TableVenusGlobalModel.GNRIInput.department;


            toAdd.business_unitID_Venus_business_unit = TableVenusGlobalModel.GNRIInput.businessunit;
            toAdd.po_type = po_type;


            toAdd.month = Venus_grni_raw.doc_date;
            toAdd.pic = TableVenusGlobalModel.GNRIInput.pic;
            TimeSpan date_po = DateTime.Now - TableVenusGlobalModel.GNRIInput.doc_date;
            toAdd.po_age = date_po.Days;
            toAdd.status = "open";
            
           
            toAdd.remarks = Venus_grni_raw.remark;
            toAdd.amount = Convert.ToInt64(Venus_grni_raw.amount);

            TableVenusGlobalModel.GNRIInput.statusAksi = "add";
            cekPair(Venus_grni_raw, toAdd, TableVenusGlobalModel);
            db.Venus_grni.Add(toAdd);
            db.SaveChanges();
        }

                private Venus_po_type Venus_po_type(string Value)
        {
            Venus_po_type List = new Venus_po_type();
            List = db.Venus_po_type.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_department Venus_department(string Value)
        {
            Venus_department List = new Venus_department();
            List = db.Venus_department.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_vendor Venus_vendor(string Value)
        {
            Venus_vendor List = new Venus_vendor();
            List = db.Venus_vendor.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_business_unit Venus_business_unit(string Value)
        {
            Venus_business_unit List = new Venus_business_unit();
            List = db.Venus_business_unit.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
    }
}