﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using hera.Models;
using hera.Models.ViewModels.Upload;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System.IO;
using System.Threading;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using hera.Models.ViewModels.Venus;

namespace hera.Controllers
{
    public class UploadFileController : Controller
    {
        private readonly KPIContext db;
        public UploadFileController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            UploadFile UploadFile = new UploadFile();

            ViewBag.Status = TempData["Status"];
            ViewBag.Message = TempData["Message"];
            return View();
        }
   
        public ActionResult ExportTemplateGRNI()
       {           
                UploadFile UploadFile = new UploadFile();

            UploadFile.UploadGRNI = (from a in db.Venus_grni_raw
                                     join b in db.Venus_grni on a.ID_Venus_grni_raw equals b.ID_Venus_grni
                                     select new UploadGRNI
                                     {

                                         ID_Venus_grni_raw = a.ID_Venus_grni_raw,

                                         po_type = a.po_type,
                                         po_number = a.po_number,
                                         po_date = a.po_date.ToString("dd/MM/yyyy"),
                                         doc_type = a.doc_type,
                                         doc_number = a.doc_number,
                                         doc_date = a.doc_date.ToString("dd/MM/yyyy"),
                                         supplier_code = a.supplier_code,
                                         supplier_name = a.supplier_name,
                                         remark = a.remark,
                                         po_currency = a.po_currency,
                                         amount = a.amount,
                                         unit = a.unit,
                                         pairID_Venus_grni_raw = a.pairID_Venus_grni_raw,
                                         department = b.department.name,
                                         status = b.status,
                                         vendor = b.vendor.name,
                                         business = b.business_unit.name,
                                         po_age = b.po_age,
                                         pic =b.pic



                                     }
                                                  ).OrderBy(ss => ss.ID_Venus_grni_raw).ToList();

            ExcelPackage excel = new ExcelPackage();
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            ExcelWorksheet wsDefault = excel.Workbook.Worksheets.Add("GRNI");

            DataTable dataTable = new DataTable();
            dataTable = GenerateDataTable(UploadFile.UploadGRNI, new UploadGRNI());

            int GeneralRowStartValue = 3;
            int GeneralRowEndValue = GeneralRowStartValue;

            wsDefault.Cells["A1"].LoadFromDataTable(dataTable, true);
            wsDefault.Cells.AutoFitColumns();

            string documentFileType = ".xlsx";
            string fileName = "GRNITemplate" + documentFileType;
            var byteData = excel.GetAsByteArray();
            return File(byteData, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

        }

        public ActionResult ExportTemplateContracts()
        {
            UploadFile UploadFile = new UploadFile();

            var TempData = db.Contract_tracking.OrderBy(ss => ss.ID_Contract_tracking).ToList();
            var commodity_group = db.Contract_commodity_group.OrderBy(ss => ss.ID_Contract_commodity_group).Select(ss => ss.name).FirstOrDefault();
            var supply_category = db.Contract_supply_category.OrderBy(ss => ss.ID_Contract_supply_category).Select(ss => ss.name).FirstOrDefault();
            var buyer = db.Contract_buyer.OrderBy(ss => ss.ID_Contract_buyer).Select(ss => ss.name).FirstOrDefault();
            var period_coverage = db.Contract_period_coverage.OrderBy(ss => ss.ID_Contract_period_coverage).Select(ss => ss.period).FirstOrDefault();

            UploadFile.UploadContracts = (from a in db.Contract_tracking
                                          select new UploadContracts
                                          {
                                              supplier = a.supplier,
                                              supplier_ID = a.supplier_ID,
                                              direct = a.direct == true ? "Direct" :"Indirect",
                                              commodity_group =  a.commodity_group.name,
                                              supply_category =  a.supply_category.name,
                                              nzd = a.nzd.ToString(),
                                              contract_ID = a.contract_ID,
                                              expiry_date = a.expiry_date.ToString("dd/MM/yyyy"),
                                              buyer =  a.buyer.name ,
                                              period_coverage =  a.period_coverage.period,
                                              completed_count = a.completed_count == true ? "Completed":"Uncompleted",
                                              remarks_this_week = a.remarks_this_week
                                          }).ToList();

            ExcelPackage excel = new ExcelPackage();
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            ExcelWorksheet wsDefault = excel.Workbook.Worksheets.Add("Employee BPJS");

            DataTable dataTable = new DataTable();
            dataTable = GenerateDataTable(UploadFile.UploadContracts, new UploadContracts());

            int GeneralRowStartValue = 3;
            int GeneralRowEndValue = GeneralRowStartValue;

            wsDefault.Cells["A1"].LoadFromDataTable(dataTable, true);
            wsDefault.Cells.AutoFitColumns();

            string documentFileType = ".xlsx";
            string fileName = "ContractsTemplate" + documentFileType;
            var byteData = excel.GetAsByteArray();
            return File(byteData, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }

        public IActionResult UploadContracts(UploadFile UploadFile)
        {
            try
            {
                IFormFile file = UploadFile.File;
                CancellationToken cancellationToken;

                var stream = new MemoryStream();
                file.CopyToAsync(stream, cancellationToken);

                var package = new ExcelPackage(stream);
                var currentSheet = package.Workbook.Worksheets;
                var worksheet = currentSheet.First();
                var rowCount = worksheet.Dimension.Rows;

                var list = UploadFile.Contract_tracking.ToList();

                for (int row = 2; row <= rowCount; row++)
                {
                    Contract_tracking toInsert = new Contract_tracking();
                    toInsert.supplier = worksheet.Cells[row, 1].Value.ToString().Trim();
                    toInsert.supplier_ID = worksheet.Cells[row, 2].Value.ToString().Trim();
                    toInsert.direct = worksheet.Cells[row, 3].Value.ToString().Trim() == "1" ? true : false;
                    Contract_commodity_group commodity = db.Contract_commodity_group.Where(s => s.name == worksheet.Cells[row, 4].Value.ToString().Trim()).FirstOrDefault();
                    if (commodity == null)
                    {
                        Contract_commodity_group toAdd = new Contract_commodity_group();
                        toAdd.name = worksheet.Cells[row, 4].Value.ToString().Trim();
                        db.Contract_commodity_group.Add(toAdd);
                        db.SaveChanges();
                    }
                    Contract_buyer  buyer = db.Contract_buyer.Where(s => s.name == worksheet.Cells[row, 9].Value.ToString().Trim()).FirstOrDefault();
                    if (buyer == null)
                    {
                        Contract_buyer toAdd = new Contract_buyer();
                        toAdd.name = worksheet.Cells[row, 9].Value.ToString().Trim();
                        db.Contract_buyer.Add(toAdd);
                        db.SaveChanges();
                    }
                    Contract_supply_category supply_category = db.Contract_supply_category.Where(s => s.name == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                    if (supply_category == null)
                    {
                        Contract_supply_category toAdd = new Contract_supply_category();
                        toAdd.name = worksheet.Cells[row, 5].Value.ToString().Trim();
                        db.Contract_supply_category.Add(toAdd);
                        db.SaveChanges();
                    }
                    Contract_period_coverage period_coverage = db.Contract_period_coverage.Where(s => s.period == worksheet.Cells[row, 5].Value.ToString().Trim()).FirstOrDefault();
                    if (period_coverage == null)
                    {
                        Contract_period_coverage toAdd = new Contract_period_coverage();
                        toAdd.period = worksheet.Cells[row, 10].Value.ToString().Trim();
                        db.Contract_period_coverage.Add(toAdd);
                        db.SaveChanges();
                    }
                    toInsert.commodity_group = Contract_commodity_group(worksheet.Cells[row, 4].Value.ToString().Trim());
                    toInsert.supply_category = Contract_supply_category(worksheet.Cells[row, 5].Value.ToString().Trim());
                    toInsert.nzd = Convert.ToInt32(worksheet.Cells[row, 6].Value.ToString().Trim());
                    toInsert.contract_ID = worksheet.Cells[row, 7].Value.ToString().Trim();
                    toInsert.expiry_date = Convert.ToDateTime(worksheet.Cells[row, 8].Value.ToString().Trim());
                    toInsert.buyer = Contract_buyer(worksheet.Cells[row, 9].Value.ToString().Trim());
                    toInsert.period_coverage = Contract_period_coverage(worksheet.Cells[row, 10].Value.ToString().Trim());
                    toInsert.completed_count = worksheet.Cells[row, 11].Value.ToString().Trim() == "Completed" ? true : false;
                    toInsert.remarks_this_week = worksheet.Cells[row, 12].Value.ToString().Trim();

                    db.Contract_tracking.Add(toInsert);
                    db.SaveChanges();
                }
            }
            catch (Exception Ex)
            {
        throw Ex;
            }
            return View("Index");
        }

        public IActionResult UploadGRNI(UploadFile UploadFile)
        {
         try
            {
                IFormFile file = UploadFile.File;
                CancellationToken cancellationToken;
              

                var stream = new MemoryStream();
                file.CopyToAsync(stream, cancellationToken);

                var package = new ExcelPackage(stream);
                var currentSheet = package.Workbook.Worksheets;
                var worksheet = currentSheet.First();
                var rowCount = worksheet.Dimension.Rows;
                var list = UploadFile.Venus_grni_raw.ToList();
                for (int row = 2; row <= rowCount; row++)
                {
                  
                        Venus_grni_raw toInsert = new Venus_grni_raw();
                    Venus_po_type po_type = db.Venus_po_type.Where(s => s.name == worksheet.Cells[row, 2].Value.ToString().Trim()).FirstOrDefault();
                    if (po_type == null)
                    {
                        Venus_po_type toAdd = new Venus_po_type();
                        toAdd.name = worksheet.Cells[row, 2].Value.ToString().Trim();
                        db.Venus_po_type.Add(toAdd);
                        db.SaveChanges();
                    }
                  
                    toInsert.po_type = worksheet.Cells[row, 2].Value.ToString().Trim();
                      toInsert.po_number = worksheet.Cells[row, 3].Value.ToString().Trim();
                    toInsert.po_date = Convert.ToDateTime(worksheet.Cells[row, 4].Value.ToString().Trim());
                       toInsert.doc_type = worksheet.Cells[row, 5].Value.ToString().Trim();
                       toInsert.doc_number = worksheet.Cells[row, 6].Value.ToString().Trim();
                        toInsert.doc_date = Convert.ToDateTime(worksheet.Cells[row, 7].Value.ToString().Trim());
                        toInsert.supplier_code = worksheet.Cells[row, 8].Value.ToString().Trim();
                        toInsert.supplier_name = worksheet.Cells[row, 9].Value.ToString().Trim();
                        toInsert.remark = worksheet.Cells[row, 10].Value.ToString().Trim();
                        toInsert.po_currency = worksheet.Cells[row, 11].Value.ToString().Trim();
                        toInsert.amount = Convert.ToInt32(worksheet.Cells[row, 12].Value.ToString().Trim());
                        toInsert.unit = worksheet.Cells[row, 13].Value.ToString().Trim();
                   toInsert.archived = false;
                    toInsert.batas_atas = Convert.ToInt32(worksheet.Cells[row, 12].Value.ToString().Trim()) + ((float)Math.Abs(0.05 * Convert.ToInt32(worksheet.Cells[row, 12].Value.ToString().Trim())));
                    toInsert.batas_bawah = Convert.ToInt32(worksheet.Cells[row, 12].Value.ToString().Trim()) - ((float)Math.Abs(0.05 * Convert.ToInt32(worksheet.Cells[row, 12].Value.ToString().Trim())));
                    toInsert.status_add_email = 0;

                    db.Venus_grni_raw.Add(toInsert);
                        db.SaveChanges();
              Venus_department department = db.Venus_department.Where(s => s.name == worksheet.Cells[row, 13].Value.ToString().Trim()).FirstOrDefault();
                    if (department == null)
                    {
                        Venus_department toAdd = new Venus_department();
                        toAdd.name = worksheet.Cells[row, 13].Value.ToString().Trim();
                        db.Venus_department.Add(toAdd);
                        db.SaveChanges();
                    }
                    Venus_vendor vendor = db.Venus_vendor.Where(s => s.name == worksheet.Cells[row, 14].Value.ToString().Trim()).FirstOrDefault();
                    if (vendor == null)
                    {
                        Venus_vendor toAdd = new Venus_vendor();
                        toAdd.name = worksheet.Cells[row, 14].Value.ToString().Trim();
                        db.Venus_vendor.Add(toAdd);
                        db.SaveChanges();
                    }
                    Venus_business_unit business_unit = db.Venus_business_unit.Where(s => s.name == worksheet.Cells[row, 15].Value.ToString().Trim()).FirstOrDefault();
                    if (business_unit == null)
                    {
                        Venus_business_unit toAdd = new Venus_business_unit();
                        toAdd.name = worksheet.Cells[row, 15].Value.ToString().Trim();
                        db.Venus_business_unit.Add(toAdd);
                        db.SaveChanges();
                    }
                    Venus_grni toInsert1 = new Venus_grni();
                  toInsert1.po_type = Venus_po_type(worksheet.Cells[row, 2].Value.ToString().Trim());
                  toInsert1.po_number = worksheet.Cells[row, 3].Value.ToString().Trim();
                  toInsert1.department = Venus_department(worksheet.Cells[row, 13].Value.ToString().Trim());
                   toInsert1.vendor = Venus_vendor(worksheet.Cells[row, 14].Value.ToString().Trim());
                    TimeSpan date_po = DateTime.Now - Convert.ToDateTime(worksheet.Cells[row, 7].Value.ToString().Trim());
                    toInsert1.business_unit = Venus_business_unit(worksheet.Cells[row, 15].Value.ToString().Trim());
                    //

                    toInsert1.po_age = date_po.Days;
                    toInsert1.pic = worksheet.Cells[row, 16].Value.ToString().Trim();
                    cekPair(toInsert, toInsert1);

                    db.Venus_grni.Add(toInsert1);
                     db.SaveChanges();
                    TempData["Status"] = "1";
                    TempData["Message"] = "Updating row";
                }
            }
            catch (Exception Ex)
            {
                TempData["Message"] = "Updating row";
                throw Ex;
            }
            return View("Index");
        }
        public void cekPair(Venus_grni_raw Venus_grni_raw, Venus_grni toAdd)
        {
            List<Venus_grni_raw> cekPairnyaPV = new List<Venus_grni_raw>();
            List<Venus_grni_raw> cekPairnyaOV = new List<Venus_grni_raw>();
            cekPairnyaPV = (from a in db.Venus_grni_raw
                          where (a.doc_type == "OV")
                         && a.po_type == Venus_grni_raw.po_type
                         && (a.amount >= Venus_grni_raw.batas_bawah && a.amount <= Venus_grni_raw.batas_atas)
                          select a).ToList();
            cekPairnyaOV = (from a in db.Venus_grni_raw
                            where (a.doc_type == "PV")
                           && a.po_type == Venus_grni_raw.po_type
                           && (a.amount >= Venus_grni_raw.batas_bawah && a.amount <= Venus_grni_raw.batas_atas)
                            select a).ToList();
        if (Venus_grni_raw.doc_type == "PV")
            {

                if (cekPairnyaPV.Count() > 0)
                {
                    var getID = db.Venus_grni_raw.Where(x => x.doc_type == "OV").Where(
                           x => x.po_type == Venus_grni_raw.po_type).Where(
                            x => x.amount >= Venus_grni_raw.batas_bawah)
                           .Where(x => x.amount <= Venus_grni_raw.batas_atas).First();

                    Venus_grni updateStatus = db.Venus_grni.Find(getID.ID_Venus_grni_raw);
                    Venus_grni_raw toUpdate1 = db.Venus_grni_raw.Find(getID.ID_Venus_grni_raw);
                    toAdd.status = "close";
                    updateStatus.status = "close";
                    toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                    Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                    toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                    db.Entry(toUpdate2).State = EntityState.Modified;
                    db.Entry(updateStatus).State = EntityState.Modified;
                    db.Entry(toUpdate1).State = EntityState.Modified;
        
                    db.SaveChanges();
                }
            }
            if (Venus_grni_raw.doc_type == "OV")
            {

                if (cekPairnyaOV.Count() > 0)
                {
                    var getID = db.Venus_grni_raw.Where(x => x.doc_type == "PV").Where(
                           x => x.po_type == Venus_grni_raw.po_type).Where(
                            x => x.amount >= Venus_grni_raw.batas_bawah)
                           .Where(x => x.amount <= Venus_grni_raw.batas_atas).First();

                    Venus_grni updateStatus = db.Venus_grni.Find(getID.ID_Venus_grni_raw);
                    Venus_grni_raw toUpdate1 = db.Venus_grni_raw.Find(getID.ID_Venus_grni_raw);
                    toAdd.status = "close";
                    updateStatus.status = "close";
                    toUpdate1.pairID_Venus_grni_raw = Venus_grni_raw.ID_Venus_grni_raw;
                    Venus_grni_raw toUpdate2 = db.Venus_grni_raw.Find(Venus_grni_raw.ID_Venus_grni_raw);
                    toUpdate2.pairID_Venus_grni_raw = Convert.ToInt64(getID.ID_Venus_grni_raw);
                    db.Entry(toUpdate2).State = EntityState.Modified;
                    db.Entry(updateStatus).State = EntityState.Modified;
                    db.Entry(toUpdate1).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }


                

        }
        

        private Contract_commodity_group Contract_commodity_group(string Value)
        {
            Contract_commodity_group List = new Contract_commodity_group();
            List = db.Contract_commodity_group.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Contract_supply_category Contract_supply_category(string Value)
        {
            Contract_supply_category List = new Contract_supply_category();
            List = db.Contract_supply_category.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Contract_buyer Contract_buyer(string Value)
        {
            Contract_buyer List = new Contract_buyer();
            List = db.Contract_buyer.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Contract_period_coverage Contract_period_coverage(string Value)
        {
            Contract_period_coverage List = new Contract_period_coverage();
            List = db.Contract_period_coverage.Where(ss => ss.period == Value).FirstOrDefault();
            return List;
        }
        private Venus_po_type Venus_po_type(string Value)
        {
            Venus_po_type List = new Venus_po_type();
            List = db.Venus_po_type.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_department Venus_department(string Value)
        {
            Venus_department List = new Venus_department();
            List = db.Venus_department.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_vendor Venus_vendor(string Value)
        {
            Venus_vendor List = new Venus_vendor();
            List = db.Venus_vendor.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }
        private Venus_business_unit Venus_business_unit(string Value)
        {
            Venus_business_unit List = new Venus_business_unit();
            List = db.Venus_business_unit.Where(ss => ss.name == Value).FirstOrDefault();
            return List;
        }

        private DataTable GenerateDataTable<T>(List<T> ListData, T Gridview)
        {
            DataTable dataTable = new DataTable();
            List<string> FieldException = GenerateListException();
            Type Type1 = Gridview.GetType();

            List<PropertyInfo> ListProperties = (from a in FieldException
                                                 from b in Type1.GetProperties().Where(ss => !ss.PropertyType.FullName.Contains(a))
                                                 select b).ToList();

            foreach (var propInfo in ListProperties)
            {
                dataTable.Columns.Add(propInfo.Name, Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType);

            }

            if (ListData != null)
            {
                foreach (T empItem in ListData)
                {
                    DataRow row = dataTable.NewRow();
                    foreach (DataColumn itemCol in dataTable.Columns)
                    {
                        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                        PropertyDescriptor prop = properties.Cast<PropertyDescriptor>().Where(ss => ss.Name == itemCol.ColumnName.Replace("*", "").Trim()).FirstOrDefault();
                        if (prop != null)
                        {
                            row[prop.Name] = prop.GetValue(empItem) ?? DBNull.Value;

                        }
                    }
                    dataTable.Rows.Add(row);
                }
            }

            return dataTable;
        }
        private List<string> GenerateListException()
        {
            List<string> temp = new List<string>();
            temp.Add("System.Collections");
            return temp;
        }
    }
}