﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using hera.Models;
using hera.Models.ViewModels.Venus;
using hera.Models.ViewModels.Email;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.ExcelUtilities;

namespace hera.Controllers
{
    public class VenusBusinessController : Controller
    {

        private readonly KPIContext db;
        
        public VenusBusinessController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelBusiness TableBusinessModel = new TabelBusiness();
            TableBusinessModel.BusinessTable = (from a in db.Venus_business_unit
                                                select new BusinessGridTable
                                                {
                                                    ID_Venus_business_unit = a.ID_Venus_business_unit,
                                                    name = a.name
                                                }).OrderBy(ss => ss.ID_Venus_business_unit).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableBusinessModel);
        }
       
        [HttpGet]
        public IActionResult Input(long? id)
        {
            TabelBusiness TabelBusiness = new TabelBusiness();
   
            

            if (id != null && id != 0 )
            {
                TabelBusiness.InputBusiness = new BusinessInput();
                TabelBusiness.InputBusiness.Form = "Edit";

                Venus_business_unit Venus_business_unit = new Venus_business_unit();
                Venus_business_unit = db.Venus_business_unit.Find(id);
                TabelBusiness.InputBusiness.ID_Venus_business_unit = Venus_business_unit.ID_Venus_business_unit;
                TabelBusiness.InputBusiness.name = Venus_business_unit.name;
            }
            else
            {
                TabelBusiness.InputBusiness = new BusinessInput();
                TabelBusiness.InputBusiness.Form = "Create";
            }


            return PartialView("InputPopup", TabelBusiness);
        }

        [HttpPost]
        public IActionResult Input(TabelBusiness TabelBusiness)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id = TabelBusiness.InputBusiness.ID_Venus_business_unit;
                List<Venus_business_unit> cekName = new List<Venus_business_unit>();
               
                if (id != null && id != 0)
                {
                    cekName = (from a in db.Venus_business_unit
                               where (a.name == TabelBusiness.InputBusiness.name && a.ID_Venus_business_unit !=id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_business_unit toUpdate = db.Venus_business_unit.Find(id);
                        //  }

                        toUpdate.name = TabelBusiness.InputBusiness.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;

                        //db.Entry(updatePair).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }

                    else if (cekName.Count != 0)
                    {
                       

                        TempData["Status"] = "2";
                        TempData["Message"] = "Name had already taken !";

                    }
                }
                else
                {
                    cekName = (from a in db.Venus_business_unit
                               where (a.name == TabelBusiness.InputBusiness.name)
                               select a).ToList();

                    if (cekName.Count == 0)
                    {
                        Venus_business_unit toAdd = new Venus_business_unit();
                        toAdd.name = TabelBusiness.InputBusiness.name;
      
                        dbContextTransaction.Commit();
                        db.Venus_business_unit.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Name had already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {

            TabelBusiness TabelBusiness = new TabelBusiness();
            TabelBusiness.InputBusiness = new BusinessInput();
            Venus_business_unit Venus_business_unit = new Venus_business_unit();
            Venus_business_unit = db.Venus_business_unit.Find(id);
            TabelBusiness.InputBusiness.ID_Venus_business_unit = Venus_business_unit.ID_Venus_business_unit;

            TabelBusiness.URLAction = "Delete";
            TabelBusiness.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup", TabelBusiness);
        }






        [HttpPost]
        public IActionResult Delete(TabelBusiness TabelBusiness)
        {
            long? id = TabelBusiness.InputBusiness.ID_Venus_business_unit;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {

                Venus_business_unit toDelete = db.Venus_business_unit.Find(id);
                List<Venus_grni> cekPairnya = new List<Venus_grni>();
                cekPairnya = (from a in db.Venus_grni
                            where (a.business_unit.ID_Venus_business_unit == id)
                            select a).ToList();
                 for(int i = 0; i < cekPairnya.Count(); i++)
                {
                    var grni = db.Venus_grni.Where(x => x.business_unit.ID_Venus_business_unit == id).FirstOrDefault();
                    Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);

                    cekdelete(grni_raw);
                    db.Venus_grni_raw.Remove(grni_raw);
                    db.Venus_grni.Remove(grni);
                

                    db.SaveChanges();

                }
                db.Venus_business_unit.Remove(toDelete);
                dbContextTransaction.Commit();
                db.SaveChanges();
               
                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelBusiness TabelBusiness = new TabelBusiness();
            if (!String.IsNullOrEmpty(id))
            {
                TabelBusiness.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelBusiness.idDeleteString = id;
            }
            var howmanyrow = TabelBusiness.idDeleteArray != null ? TabelBusiness.idDeleteArray.Count() : 0;

            TabelBusiness.URLAction = "DeleteAll";
            TabelBusiness.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelBusiness);
        }
        public void cekdelete(Venus_grni_raw grni_raw)
        {
            if (grni_raw.pairID_Venus_grni_raw != 0)
            {
                if (grni_raw.doc_type == "PV")
                {
                    List<Venus_grni_raw> cekPV = new List<Venus_grni_raw>();
                    cekPV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "OV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var idPair = cekPV.FirstOrDefault();
                    idPair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(idPair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(idPair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
                if (grni_raw.doc_type == "OV")
                {
                    List<Venus_grni_raw> cekOV = new List<Venus_grni_raw>();
                    cekOV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "PV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var idPair = cekOV.FirstOrDefault();
                   
                    idPair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(idPair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(idPair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }
        [HttpPost]
        public IActionResult DeleteAll(TabelBusiness TabelBusiness)
        {
            try
            {
                if (!String.IsNullOrEmpty(TabelBusiness.idDeleteString))
                {
                    TabelBusiness.idDeleteArray = JsonConvert.DeserializeObject<string[]>(TabelBusiness.idDeleteString);
                }
                var howmanyrow = TabelBusiness.idDeleteArray != null ? TabelBusiness.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in TabelBusiness.idDeleteArray)
                {

                    long idDelete = Convert.ToInt64(toDeleteID);


                    Venus_business_unit toDelete1 = db.Venus_business_unit.Find(idDelete);

              
                    List<Venus_grni> cekPairnya1 = new List<Venus_grni>();
                    cekPairnya1 = (from a in db.Venus_grni
                                  where (a.business_unit.ID_Venus_business_unit == idDelete)
                                  select a).ToList();
                     for (int i = 0; i < cekPairnya1.Count(); i++)
                    {
                        var grni = db.Venus_grni.Where(x => x.business_unit.ID_Venus_business_unit == idDelete).FirstOrDefault();
                        Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                     
                        cekdelete(grni_raw);
                        db.Venus_grni_raw.Remove(grni_raw);
                        db.Venus_grni.Remove(grni);
                        

                        db.SaveChanges();

                    }
                    db.Venus_business_unit.Remove(toDelete1);
                    db.SaveChanges();


                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

    }
}