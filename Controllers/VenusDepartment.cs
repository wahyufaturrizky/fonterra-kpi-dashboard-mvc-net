﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using hera.Models;
using hera.Models.ViewModels.Venus;
using hera.Models.ViewModels.Email;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml.FormulaParsing.ExcelUtilities;

namespace hera.Controllers
{
    public class VenusDepartmentController : Controller
    {
        private readonly KPIContext db;
        public VenusDepartmentController(KPIContext context)
        {
            db = context;
        }
        public IActionResult Index()
        {
            TabelDepartment TableDepartmentModel = new TabelDepartment();
            TableDepartmentModel.DepartmentTable = (from a in db.Venus_department
                                     select new DepartmentGridTable{
                                         ID_Venus_department=a.ID_Venus_department,
                                          name = a.name}).OrderBy(ss => ss.ID_Venus_department).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableDepartmentModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelDepartment  TabelDepartment = new  TabelDepartment();
            if (id != null && id != 0)
            {
                TabelDepartment.InputDepartment = new DepartmentInput();
                  TabelDepartment.InputDepartment.Form = "Edit";

                Venus_department Venus_department = new Venus_department();
                Venus_department = db.Venus_department.Find(id);
                 TabelDepartment.InputDepartment.ID_Venus_department = Venus_department.ID_Venus_department;
                 TabelDepartment.InputDepartment.name = Venus_department.name;
                             }
            else
            {
                 TabelDepartment.InputDepartment = new DepartmentInput();
                 TabelDepartment.InputDepartment.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelDepartment);
        }

        [HttpPost]
        public IActionResult Input( TabelDepartment  TabelDepartment)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelDepartment.InputDepartment.ID_Venus_department;
                List<Venus_department> cekName = new List<Venus_department>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Venus_department
                               where (a.name == TabelDepartment.InputDepartment.name && a.ID_Venus_department != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {

                        Venus_department toUpdate = db.Venus_department.Find(id);
                        toUpdate.name = TabelDepartment.InputDepartment.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Department name had already taken !";
                    }
                }
                else
                {
                    cekName = (from a in db.Venus_department
                               where (a.name == TabelDepartment.InputDepartment.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_department toAdd = new Venus_department();
                        toAdd.name = TabelDepartment.InputDepartment.name;

                        dbContextTransaction.Commit();
                        db.Venus_department.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Department name had already taken !";
                    }
                }
                }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelDepartment  TabelDepartment = new  TabelDepartment();
            TabelDepartment.InputDepartment = new DepartmentInput();
            Venus_department Venus_department = new Venus_department();
            Venus_department = db.Venus_department.Find(id);
             TabelDepartment.InputDepartment.ID_Venus_department = Venus_department.ID_Venus_department;

             TabelDepartment.URLAction = "Delete";
             TabelDepartment.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelDepartment);
        }
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelDepartment  TabelDepartment)
        {
            long? id =  TabelDepartment.InputDepartment.ID_Venus_department;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Venus_department toDelete = db.Venus_department.Find(id);
               
                List<Venus_grni> cekPairnya = new List<Venus_grni>();
                cekPairnya = (from a in db.Venus_grni
                              where (a.department.ID_Venus_department == id)
                              select a).ToList();
                for (int i = 0; i < cekPairnya.Count(); i++)
                {
                    
                    var grni = db.Venus_grni.Where(x => x.department.ID_Venus_department == id).FirstOrDefault();
                    Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                    cekdelete(grni_raw);
                    db.Venus_grni_raw.Remove(grni_raw);
                    db.Venus_grni.Remove(grni);
                   

                    db.SaveChanges();

                }


                dbContextTransaction.Commit();
                db.Venus_department.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }
          public void cekdelete(Venus_grni_raw grni_raw)
        {
            if (grni_raw.pairID_Venus_grni_raw != 0)
            {
                if (grni_raw.doc_type == "PV")
                {
                    List<Venus_grni_raw> cekPV = new List<Venus_grni_raw>();
                    cekPV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "OV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var idPair = cekPV.FirstOrDefault();
                   
                    idPair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(idPair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(idPair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
                if (grni_raw.doc_type == "OV")
                {
                    List<Venus_grni_raw> cekOV = new List<Venus_grni_raw>();
                    cekOV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "PV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var idPair = cekOV.FirstOrDefault();
                    idPair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(idPair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(idPair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }
      
      
        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelDepartment TabelDepartment = new TabelDepartment();
            if (!String.IsNullOrEmpty(id))
            {
                TabelDepartment.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelDepartment.idDeleteString = id;
            }
            var howmanyrow = TabelDepartment.idDeleteArray != null ? TabelDepartment.idDeleteArray.Count() : 0;

            TabelDepartment.URLAction = "DeleteAll";
            TabelDepartment.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelDepartment);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelDepartment TabelDepartment)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelDepartment.idDeleteString))
                {
                     TabelDepartment.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelDepartment.idDeleteString);
                }
                var howmanyrow =  TabelDepartment.idDeleteArray != null ?  TabelDepartment.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelDepartment.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Venus_department toDelete = db.Venus_department.Find(idDelete);


                    List<Venus_grni> cekPairnya1 = new List<Venus_grni>();
                    cekPairnya1 = (from a in db.Venus_grni
                                   where (a.department.ID_Venus_department == idDelete)
                                   select a).ToList();
                   for (int i = 0; i < cekPairnya1.Count(); i++)
                    {
                        var grni = db.Venus_grni.Where(x => x.department.ID_Venus_department == idDelete).FirstOrDefault();
                        Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                     
                        cekdelete(grni_raw);
                        db.Venus_grni_raw.Remove(grni_raw);
                        db.Venus_grni.Remove(grni);
                      

                        db.SaveChanges();

                    }
                    db.Venus_department.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }

}