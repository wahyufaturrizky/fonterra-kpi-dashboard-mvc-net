﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Venus;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class VenusTypeController : Controller
    {
        private readonly KPIContext db;
        public VenusTypeController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelType TableTypeModel = new TabelType();
            TableTypeModel.TypeTable = (from a in db.Venus_po_type
                                        select new TypeGridTable {
                                            ID_Venus_po_type = a.ID_Venus_po_type,
                                            name = a.name }).OrderBy(ss => ss.ID_Venus_po_type).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableTypeModel);
        }

        [HttpGet]
        public IActionResult Input(long? id)
        {
            TabelType TabelType = new TabelType();
            if (id != null && id != 0)
            {
                TabelType.InputType = new TypeInput();
                TabelType.InputType.Form = "Edit";

                Venus_po_type Venus_po_type = new Venus_po_type();
                Venus_po_type = db.Venus_po_type.Find(id);
                TabelType.InputType.ID_Venus_po_type = Venus_po_type.ID_Venus_po_type;
                TabelType.InputType.name = Venus_po_type.name;
            }
            else
            {
                TabelType.InputType = new TypeInput();
                TabelType.InputType.Form = "Create";
            }


            return PartialView("InputPopup", TabelType);
        }

        [HttpPost]
        public IActionResult Input(TabelType TabelType)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id = TabelType.InputType.ID_Venus_po_type;
              
                List<Venus_po_type> cekName = new List<Venus_po_type>();
                if (id != null && id != 0)
                {
                   
                    cekName = (from a in db.Venus_po_type
                               where (a.name == TabelType.InputType.name && a.ID_Venus_po_type != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_po_type toUpdate = db.Venus_po_type.Find(id);

                        List<Venus_grni_raw> cekEdit = new List<Venus_grni_raw>();
                        cekEdit = (from a in db.Venus_grni_raw
                                   where (a.po_type == toUpdate.name)
                                   select a).ToList();
                        for (int i = 0; i < cekEdit.Count(); i++)
                        {
                            var grni_raw = db.Venus_grni_raw.Where(x => x.po_type == toUpdate.name).FirstOrDefault();
                            grni_raw.po_type = TabelType.InputType.name;

                            db.Entry(grni_raw).State = EntityState.Modified;

                            db.SaveChanges();

                        }
                        toUpdate.name = TabelType.InputType.name;

                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;
                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Type name had already taken !";
                    }


                }
            
                else
                {
                    cekName = (from a in db.Venus_po_type
                               where (a.name == TabelType.InputType.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_po_type toAdd = new Venus_po_type();
                        toAdd.name = TabelType.InputType.name;
            
                        dbContextTransaction.Commit();
                        db.Venus_po_type.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Type name had already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }
    
        [HttpGet]
        public IActionResult Delete(long? id)
        {

            TabelType TabelType = new TabelType();
            TabelType.InputType = new TypeInput();
            Venus_po_type Venus_po_type = new Venus_po_type();
            Venus_po_type = db.Venus_po_type.Find(id);
            TabelType.InputType.ID_Venus_po_type = Venus_po_type.ID_Venus_po_type;

            TabelType.URLAction = "Delete";
            TabelType.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup", TabelType);
        }

        [HttpPost]
        public IActionResult Delete(TabelType TabelType)
        {
            long? id = TabelType.InputType.ID_Venus_po_type;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {

                Venus_po_type toDelete = db.Venus_po_type.Find(id);


                List<Venus_grni> cekPairnya = new List<Venus_grni>();
                cekPairnya = (from a in db.Venus_grni
                              where (a.po_type.ID_Venus_po_type == id)
                              select a).ToList();
               for (int i = 0; i < cekPairnya.Count(); i++)
                {
                    var grni = db.Venus_grni.Where(x => x.po_type.ID_Venus_po_type == id).FirstOrDefault();
                    Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);

                    db.Venus_grni_raw.Remove(grni_raw);
                    db.Venus_grni.Remove(grni);
                  

                    db.SaveChanges();

                }

                dbContextTransaction.Commit();
                db.Venus_po_type.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelType TabelType = new TabelType();
            if (!String.IsNullOrEmpty(id))
            {
                TabelType.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelType.idDeleteString = id;
            }
            var howmanyrow = TabelType.idDeleteArray != null ? TabelType.idDeleteArray.Count() : 0;

            TabelType.URLAction = "DeleteAll";
            TabelType.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelType);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelType TabelType)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelType.idDeleteString))
                {
                     TabelType.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelType.idDeleteString);
                }
                var howmanyrow =  TabelType.idDeleteArray != null ?  TabelType.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelType.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Venus_po_type toDelete = db.Venus_po_type.Find(idDelete);

                    List<Venus_grni> cekPairnya1 = new List<Venus_grni>();
                    cekPairnya1 = (from a in db.Venus_grni
                                   where (a.po_type.ID_Venus_po_type == idDelete)
                                   select a).ToList();
                    for (int i = 0; i < cekPairnya1.Count(); i++)
                    {
                        var grni = db.Venus_grni.Where(x => x.po_type.ID_Venus_po_type == idDelete).FirstOrDefault();
                        Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                      
                        db.Venus_grni_raw.Remove(grni_raw);
                        db.Venus_grni.Remove(grni);

                      
                        db.SaveChanges();

                    }

                    db.Venus_po_type.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}