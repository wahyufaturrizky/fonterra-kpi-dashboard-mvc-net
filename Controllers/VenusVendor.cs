﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using hera.Models;
using hera.Models.ViewModels.Venus;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace hera.Controllers
{
    public class VenusVendorController : Controller
    {
        private readonly KPIContext db;
        public VenusVendorController(KPIContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            TabelVendor TableVendorModel = new TabelVendor();
            TableVendorModel.VendorTable = (from a in db.Venus_vendor 
                                     select new VendorGridTable{
                                         ID_Venus_vendor=a.ID_Venus_vendor,
                                          name = a.name}).OrderBy(ss => ss.ID_Venus_vendor).ToList();
            ViewBag.Status = TempData["Status"] != null ? TempData["Status"] : 0;
            ViewBag.Message = TempData["Message"] != null ? TempData["Message"] : "NULL";

            return View(TableVendorModel);
        }
      
    [HttpGet]
        public IActionResult Input(long? id)
        {
             TabelVendor  TabelVendor = new  TabelVendor();
            if (id != null && id != 0)
            {
                TabelVendor.InputVendor = new VendorInput();
                  TabelVendor.InputVendor.Form = "Edit";

                Venus_vendor Venus_vendor = new Venus_vendor();
                Venus_vendor = db.Venus_vendor.Find(id);
                 TabelVendor.InputVendor.ID_Venus_vendor = Venus_vendor.ID_Venus_vendor;
                 TabelVendor.InputVendor.name = Venus_vendor.name;
                             }
            else
            {
                 TabelVendor.InputVendor = new VendorInput();
                 TabelVendor.InputVendor.Form = "Create";
            }

          
            return PartialView("InputPopup",  TabelVendor);
        }

        [HttpPost]
        public IActionResult Input( TabelVendor  TabelVendor)
        {
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
                long? id =  TabelVendor.InputVendor.ID_Venus_vendor;
                List<Venus_vendor> cekName = new List<Venus_vendor>();
                if (id != null && id != 0)
                {

                    cekName = (from a in db.Venus_vendor
                               where (a.name == TabelVendor.InputVendor.name && a.ID_Venus_vendor != id)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_vendor toUpdate = db.Venus_vendor.Find(id);
                        toUpdate.name = TabelVendor.InputVendor.name;



                        dbContextTransaction.Commit();
                        db.Entry(toUpdate).State = EntityState.Modified;

                        db.SaveChanges();

                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Vendor name had already taken !";
                    }
                }
                else
                {
                    cekName = (from a in db.Venus_vendor
                               where (a.name == TabelVendor.InputVendor.name)
                               select a).ToList();
                    if (cekName.Count == 0)
                    {
                        Venus_vendor toAdd = new Venus_vendor();
                        toAdd.name = TabelVendor.InputVendor.name;

                        dbContextTransaction.Commit();
                        db.Venus_vendor.Add(toAdd);
                        db.SaveChanges();



                        TempData["Status"] = "1";
                        TempData["Message"] = "Updating row";
                    }
                    else if (cekName.Count != 0)
                    {
                        TempData["Status"] = "2";
                        TempData["Message"] = "Vendor name had already taken !";
                    }
                }
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Updating row";
                throw;
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
           
            TabelVendor  TabelVendor = new  TabelVendor();
            TabelVendor.InputVendor = new VendorInput();
            Venus_vendor Venus_vendor = new Venus_vendor();
            Venus_vendor = db.Venus_vendor.Find(id);
             TabelVendor.InputVendor.ID_Venus_vendor = Venus_vendor.ID_Venus_vendor;

             TabelVendor.URLAction = "Delete";
             TabelVendor.DeleteMessage = "Are you sure want to delete this ?";

            return PartialView("DeletePopup",  TabelVendor);
        } 
   

   

            
        
        [HttpPost]
        public IActionResult Delete( TabelVendor  TabelVendor)
        {
            long? id =  TabelVendor.InputVendor.ID_Venus_vendor;
            var dbContextTransaction = db.Database.BeginTransaction();

            try
            {
               
                Venus_vendor toDelete = db.Venus_vendor.Find(id);
    
                List<Venus_grni> cekPairnya = new List<Venus_grni>();
                cekPairnya = (from a in db.Venus_grni
                              where (a.vendor.ID_Venus_vendor== id)
                              select a).ToList();
                for (int i = 0; i < cekPairnya.Count(); i++)
                {
                    var grni = db.Venus_grni.Where(x => x.vendor.ID_Venus_vendor == id).FirstOrDefault();
                    Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                
                    cekdelete(grni_raw);

                    db.Venus_grni_raw.Remove(grni_raw);
                    db.Venus_grni.Remove(grni);


                    db.SaveChanges();

                }


                dbContextTransaction.Commit();
                db.Venus_vendor.Remove(toDelete);
                db.SaveChanges();

                TempData["Status"] = "1";
                TempData["Message"] = "Delete row";
            }
            catch (Exception e)
            {
                dbContextTransaction.Rollback();
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }
        public void cekdelete(Venus_grni_raw grni_raw)
        {
            if (grni_raw.pairID_Venus_grni_raw != 0)
            {
                if (grni_raw.doc_type == "PV")
                {
                    List<Venus_grni_raw> cekPV = new List<Venus_grni_raw>();
                    cekPV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "OV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var idPair = cekPV.FirstOrDefault();
                   idPair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(idPair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(idPair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
                if (grni_raw.doc_type == "OV")
                {
                    List<Venus_grni_raw> cekOV = new List<Venus_grni_raw>();
                    cekOV = (from a in db.Venus_grni_raw
                             where (a.doc_type == "PV")
                             && a.pairID_Venus_grni_raw == grni_raw.ID_Venus_grni_raw
                             select a).ToList();
                    var pair = cekOV.FirstOrDefault();
                   pair.pairID_Venus_grni_raw = 0;
                    Venus_grni grni = db.Venus_grni.Find(pair.ID_Venus_grni_raw);
                    grni.status = "open";
                    db.Entry(pair).State = EntityState.Modified;
                    db.Entry(grni).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }

        [HttpGet]
        public IActionResult DeleteAll(string id)
        {
            TabelVendor TabelVendor = new TabelVendor();
            if (!String.IsNullOrEmpty(id))
            {
                TabelVendor.idDeleteArray = JsonConvert.DeserializeObject<string[]>(id);
                TabelVendor.idDeleteString = id;
            }
            var howmanyrow = TabelVendor.idDeleteArray != null ? TabelVendor.idDeleteArray.Count() : 0;

            TabelVendor.URLAction = "DeleteAll";
            TabelVendor.DeleteMessage = "Are you sure want to delete " + howmanyrow + " row ?";

            return PartialView("DeletePopup", TabelVendor);
        }

        [HttpPost]
        public IActionResult DeleteAll(TabelVendor TabelVendor)
        {
            try
            {
                if (!String.IsNullOrEmpty( TabelVendor.idDeleteString))
                {
                     TabelVendor.idDeleteArray = JsonConvert.DeserializeObject<string[]>( TabelVendor.idDeleteString);
                }
                var howmanyrow =  TabelVendor.idDeleteArray != null ?  TabelVendor.idDeleteArray.Count() : 0;

                foreach (var toDeleteID in  TabelVendor.idDeleteArray)
                {
                    long idDelete = Convert.ToInt64(toDeleteID);
                 
                    Venus_vendor toDelete = db.Venus_vendor.Find(idDelete);
                    List<Venus_grni> cekPairnya1 = new List<Venus_grni>();
                    cekPairnya1 = (from a in db.Venus_grni
                                   where (a.vendor.ID_Venus_vendor == idDelete)
                                   select a).ToList();
                    for (int i = 0; i < cekPairnya1.Count(); i++)
                    {
                        var grni = db.Venus_grni.Where(x => x.vendor.ID_Venus_vendor == idDelete).FirstOrDefault();
                        Venus_grni_raw grni_raw = db.Venus_grni_raw.Find(grni.ID_Venus_grni);
                    
                        cekdelete(grni_raw);

                        db.Venus_grni_raw.Remove(grni_raw);
                        db.Venus_grni.Remove(grni);

                        db.SaveChanges();

                    }

                    db.Venus_vendor.Remove(toDelete);
                    db.SaveChanges();
                }

                TempData["Status"] = "1";
                TempData["Message"] = "Delete " + howmanyrow + " row";
            }
            catch (Exception e)
            {
                TempData["Status"] = "2";
                TempData["Message"] = "Delete row";
                throw;
            }

            return RedirectToAction("Index");
        }

       
    }
}