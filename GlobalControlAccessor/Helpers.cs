﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalControlAccessor
{
    public class Helpers
    {
    }
    public class FileType : ValidationAttribute
    {
        private string _fileExtensions;

        public FileType(string fileExtensions)
        {
            // if more than one please seperate with pipe ('|')
            _fileExtensions = fileExtensions;
        }

        public override bool IsValid(object value)
        {
            // don't duplicate the required validator - return true
            if (value == null)
            {
                return true;
            }

            // is the file extention correct?
            var postedFileName = ((IFormFile)value).FileName.ToLower();

            string ext = postedFileName.Substring(postedFileName.LastIndexOf('.') + 1);
            string[] _fileExtensionArr = _fileExtensions.Split('|');

            if (!_fileExtensionArr.Contains(ext))
            {
                return false;
            }

            return true;
        }
    }

    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly int _maxFileSize;
        public MaxFileSizeAttribute(int maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        public override bool IsValid(object value)
        {
            var file = value as IFormFile;
            if (file == null)
            {
                return false;
            }
            return file.Length <= _maxFileSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(_maxFileSize.ToString());
        }
    }
}