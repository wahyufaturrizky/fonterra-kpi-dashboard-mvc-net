﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace hera.Migrations
{
    public partial class InitialCreateSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contract_buyer",
                columns: table => new
                {
                    ID_Contract_buyer = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_buyer", x => x.ID_Contract_buyer);
                });

            migrationBuilder.CreateTable(
                name: "Contract_commodity_group",
                columns: table => new
                {
                    ID_Contract_commodity_group = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_commodity_group", x => x.ID_Contract_commodity_group);
                });

            migrationBuilder.CreateTable(
                name: "Contract_period_coverage",
                columns: table => new
                {
                    ID_Contract_period_coverage = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    period = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_period_coverage", x => x.ID_Contract_period_coverage);
                });

            migrationBuilder.CreateTable(
                name: "Contract_supply_category",
                columns: table => new
                {
                    ID_Contract_supply_category = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_supply_category", x => x.ID_Contract_supply_category);
                });

            migrationBuilder.CreateTable(
                name: "Venus_business_unit",
                columns: table => new
                {
                    ID_Venus_business_unit = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_business_unit", x => x.ID_Venus_business_unit);
                });

            migrationBuilder.CreateTable(
                name: "Venus_department",
                columns: table => new
                {
                    ID_Venus_department = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_department", x => x.ID_Venus_department);
                });

            migrationBuilder.CreateTable(
                name: "Venus_grni_raw",
                columns: table => new
                {
                    ID_Venus_grni_raw = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    po_type = table.Column<string>(nullable: true),
                    po_number = table.Column<string>(nullable: false),
                    po_date = table.Column<DateTime>(nullable: false),
                    doc_type = table.Column<string>(nullable: true),
                    doc_number = table.Column<string>(nullable: false),
                    doc_date = table.Column<DateTime>(nullable: false),
                    supplier_code = table.Column<string>(nullable: true),
                    supplier_name = table.Column<string>(nullable: true),
                    remark = table.Column<string>(nullable: true),
                    po_currency = table.Column<string>(nullable: true),
                    batas_bawah = table.Column<float>(nullable: false),
                    batas_atas = table.Column<float>(nullable: false),
                    amount = table.Column<float>(nullable: false),
                    unit = table.Column<string>(nullable: false),
                    pairID_Venus_grni_raw = table.Column<long>(nullable: true),
                    archived = table.Column<bool>(nullable: false),
                    status_add_email = table.Column<int>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_grni_raw", x => x.ID_Venus_grni_raw);
                   
                });

            migrationBuilder.CreateTable(
                name: "Venus_opo_raw",
                columns: table => new
                {
                    ID_Venus_open_po = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    order_number = table.Column<int>(nullable: false),
                    or_ty = table.Column<string>(nullable: true),
                    amount_to_receive = table.Column<float>(nullable: false),
                    ordered_amount = table.Column<float>(nullable: false),
                    order_co = table.Column<int>(nullable: false),
                    supplier_number = table.Column<int>(nullable: false),
                    supplier_name = table.Column<string>(nullable: true),
                    line_description = table.Column<string>(nullable: true),
                    last_status = table.Column<int>(nullable: false),
                    next_status = table.Column<int>(nullable: false),
                    gl_date = table.Column<DateTime>(nullable: false),
                    order_date = table.Column<DateTime>(nullable: false),
                    quantity_to_receive = table.Column<float>(nullable: false),
                    unit_cost = table.Column<float>(nullable: false),
                    original_order_no = table.Column<int>(nullable: false),
                    orig_ord_type = table.Column<string>(nullable: true),
                    account_number = table.Column<string>(nullable: true),
                    cost_center = table.Column<float>(nullable: false),
                    obj_acct = table.Column<int>(nullable: false),
                    line_number = table.Column<int>(nullable: false),
                    managrl_code_1 = table.Column<string>(nullable: true),
                    managrl_code_2 = table.Column<string>(nullable: true),
                    managrl_code_3 = table.Column<string>(nullable: true),
                    managrl_code_4 = table.Column<string>(nullable: true),
                    second_item_number = table.Column<string>(nullable: true),
                    archived = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_opo_raw", x => x.ID_Venus_open_po);
                });

            migrationBuilder.CreateTable(
                name: "Venus_po_type",
                columns: table => new
                {
                    ID_Venus_po_type = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_po_type", x => x.ID_Venus_po_type);
                });

            migrationBuilder.CreateTable(
                name: "Venus_poa_raw",
                columns: table => new
                {
                    ID_Venus_poa_raw = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    company = table.Column<int>(nullable: false),
                    payment_ref = table.Column<int>(nullable: false),
                    voucher_gl_date = table.Column<DateTime>(nullable: false),
                    supplier_invoice_number = table.Column<string>(nullable: true),
                    invoice_date = table.Column<DateTime>(nullable: false),
                    payment_gl_date = table.Column<string>(nullable: true),
                    voucher_doc_type = table.Column<string>(nullable: true),
                    voucher_number = table.Column<int>(nullable: false),
                    po_number = table.Column<string>(nullable: false),
                    po_type = table.Column<string>(nullable: true),
                    po_date = table.Column<DateTime>(nullable: false),
                    po_description_line_1 = table.Column<string>(nullable: true),
                    po_description_line_2 = table.Column<string>(nullable: true),
                    vendor_code = table.Column<int>(nullable: false),
                    vendor_name = table.Column<string>(nullable: true),
                    item_code = table.Column<string>(nullable: true),
                    item_description = table.Column<string>(nullable: true),
                    stocking_type = table.Column<string>(nullable: true),
                    stocking_type_description = table.Column<string>(nullable: true),
                    account_code = table.Column<string>(nullable: true),
                    account_description = table.Column<string>(nullable: true),
                    business_unit = table.Column<int>(nullable: false),
                    amount = table.Column<float>(nullable: false),
                    domestic_currency_code = table.Column<string>(nullable: true),
                    currency_code = table.Column<string>(nullable: true),
                    search_type = table.Column<string>(nullable: true),
                    archived = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_poa_raw", x => x.ID_Venus_poa_raw);
                });

            migrationBuilder.CreateTable(
                name: "Venus_vendor",
                columns: table => new
                {
                    ID_Venus_vendor = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_vendor", x => x.ID_Venus_vendor);
                });

            migrationBuilder.CreateTable(
                name: "Contract_actual",
                columns: table => new
                {
                    ID_Contract_actual = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    buyerID_Contract_buyer = table.Column<long>(nullable: true),
                    commodity_groupID_Contract_commodity_group = table.Column<long>(nullable: true),
                    direct = table.Column<bool>(nullable: false),
                    month = table.Column<DateTime>(nullable: false),
                    value = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_actual", x => x.ID_Contract_actual);
                    table.ForeignKey(
                        name: "FK_Contract_actual_Contract_buyer_buyerID_Contract_buyer",
                        column: x => x.buyerID_Contract_buyer,
                        principalTable: "Contract_buyer",
                        principalColumn: "ID_Contract_buyer",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contract_actual_Contract_commodity_group_commodity_groupID_Contract_commodity_group",
                        column: x => x.commodity_groupID_Contract_commodity_group,
                        principalTable: "Contract_commodity_group",
                        principalColumn: "ID_Contract_commodity_group",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contract_target",
                columns: table => new
                {
                    ID_Contract_target = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    buyerID_Contract_buyer = table.Column<long>(nullable: true),
                    commodity_groupID_Contract_commodity_group = table.Column<long>(nullable: true),
                    direct = table.Column<bool>(nullable: false),
                    month = table.Column<DateTime>(nullable: false),
                    value = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_target", x => x.ID_Contract_target);
                    table.ForeignKey(
                        name: "FK_Contract_target_Contract_buyer_buyerID_Contract_buyer",
                        column: x => x.buyerID_Contract_buyer,
                        principalTable: "Contract_buyer",
                        principalColumn: "ID_Contract_buyer",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contract_target_Contract_commodity_group_commodity_groupID_Contract_commodity_group",
                        column: x => x.commodity_groupID_Contract_commodity_group,
                        principalTable: "Contract_commodity_group",
                        principalColumn: "ID_Contract_commodity_group",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contract_tracking",
                columns: table => new
                {
                    ID_Contract_tracking = table.Column<long>(nullable: false)
                           .Annotation("SqlServer:Identity", "1, 1"),
                    supplier = table.Column<string>(nullable: true),
                    supplier_ID = table.Column<string>(nullable: true),
                    direct = table.Column<bool>(nullable: false),
                    commodity_groupID_Contract_commodity_group = table.Column<long>(nullable: true),
                    supply_categoryID_Contract_supply_category = table.Column<long>(nullable: true),
                    nzd = table.Column<float>(nullable: false),
                    contract_ID = table.Column<string>(nullable: true),
                    expiry_date = table.Column<DateTime>(nullable: false),
                    buyerID_Contract_buyer = table.Column<long>(nullable: true),
                    period_coverageID_Contract_period_coverage = table.Column<long>(nullable: true),
                    completed_count = table.Column<bool>(nullable: false),
                    remarks_last_week = table.Column<string>(nullable: true),
                    remarks_this_week = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_tracking", x => x.ID_Contract_tracking);
                    table.ForeignKey(
                        name: "FK_Contract_tracking_Contract_buyer_buyerID_Contract_buyer",
                        column: x => x.buyerID_Contract_buyer,
                        principalTable: "Contract_buyer",
                        principalColumn: "ID_Contract_buyer",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contract_tracking_Contract_commodity_group_commodity_groupID_Contract_commodity_group",
                        column: x => x.commodity_groupID_Contract_commodity_group,
                        principalTable: "Contract_commodity_group",
                        principalColumn: "ID_Contract_commodity_group",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contract_tracking_Contract_period_coverage_period_coverageID_Contract_period_coverage",
                        column: x => x.period_coverageID_Contract_period_coverage,
                        principalTable: "Contract_period_coverage",
                        principalColumn: "ID_Contract_period_coverage",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Contract_tracking_Contract_supply_category_supply_categoryID_Contract_supply_category",
                        column: x => x.supply_categoryID_Contract_supply_category,
                        principalTable: "Contract_supply_category",
                        principalColumn: "ID_Contract_supply_category",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Venus_grni",
                columns: table => new
                {
                    ID_Venus_grni = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    po_typeID_Venus_po_type = table.Column<long>(nullable: true),
                    departmentID_Venus_department = table.Column<long>(nullable: true),
                    vendorID_Venus_vendor = table.Column<long>(nullable: true),

                    business_unitID_Venus_business_unit = table.Column<long>(nullable: true),
                    po_number = table.Column<string>(nullable: false),
                    po_age = table.Column<int>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    month = table.Column<DateTime>(nullable: false),
                    amount = table.Column<long>(nullable: false),
                    pic = table.Column<string>(nullable: true),
                    remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_grni", x => x.ID_Venus_grni);
                    table.ForeignKey(
                        name: "FK_Venus_grni_Venus_business_unit_business_unitID_Venus_business_unit",
                        column: x => x.business_unitID_Venus_business_unit,
                        principalTable: "Venus_business_unit",
                        principalColumn: "ID_Venus_business_unit",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_grni_Venus_department_departmentID_Venus_department",
                        column: x => x.departmentID_Venus_department,
                        principalTable: "Venus_department",
                        principalColumn: "ID_Venus_department",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_grni_Venus_po_type_po_typeID_Venus_po_type",
                        column: x => x.po_typeID_Venus_po_type,
                        principalTable: "Venus_po_type",
                        principalColumn: "ID_Venus_po_type",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_grni_Venus_vendor_vendorID_Venus_vendor",
                        column: x => x.vendorID_Venus_vendor,
                        principalTable: "Venus_vendor",
                        principalColumn: "ID_Venus_vendor",
                        onDelete: ReferentialAction.Restrict);
                
                });

            migrationBuilder.CreateTable(
                name: "Venus_open_po",
                columns: table => new
                {
                    ID_Venus_open_po = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    po_typeID_Venus_po_type = table.Column<long>(nullable: true),
                    departmentID_Venus_department = table.Column<long>(nullable: true),
                    vendorID_Venus_vendor = table.Column<long>(nullable: true),
                    business_unitID_Venus_business_unit = table.Column<long>(nullable: true),
                    po_number = table.Column<string>(nullable: false),
                    po_age = table.Column<int>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    month = table.Column<DateTime>(nullable: false),
                    amount = table.Column<long>(nullable: false),
                    pic = table.Column<string>(nullable: true),
                    remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_open_po", x => x.ID_Venus_open_po);
                    table.ForeignKey(
                        name: "FK_Venus_open_po_Venus_business_unit_business_unitID_Venus_business_unit",
                        column: x => x.business_unitID_Venus_business_unit,
                        principalTable: "Venus_business_unit",
                        principalColumn: "ID_Venus_business_unit",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_open_po_Venus_department_departmentID_Venus_department",
                        column: x => x.departmentID_Venus_department,
                        principalTable: "Venus_department",
                        principalColumn: "ID_Venus_department",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_open_po_Venus_po_type_po_typeID_Venus_po_type",
                        column: x => x.po_typeID_Venus_po_type,
                        principalTable: "Venus_po_type",
                        principalColumn: "ID_Venus_po_type",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_open_po_Venus_vendor_vendorID_Venus_vendor",
                        column: x => x.vendorID_Venus_vendor,
                        principalTable: "Venus_vendor",
                        principalColumn: "ID_Venus_vendor",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Venus_po_after",
                columns: table => new
                {
                    ID_Venus_po_after = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    po_typeID_Venus_po_type = table.Column<long>(nullable: true),
                    departmentID_Venus_department = table.Column<long>(nullable: true),
                    vendorID_Venus_vendor = table.Column<long>(nullable: true),
                    business_unitID_Venus_business_unit = table.Column<long>(nullable: true),
                    po_number = table.Column<string>(nullable: false),
                    po_age = table.Column<int>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    month = table.Column<DateTime>(nullable: false),
                    amount = table.Column<long>(nullable: false),
                    pic = table.Column<string>(nullable: true),
                    remarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Venus_po_after", x => x.ID_Venus_po_after);
                    table.ForeignKey(
                        name: "FK_Venus_po_after_Venus_business_unit_business_unitID_Venus_business_unit",
                        column: x => x.business_unitID_Venus_business_unit,
                        principalTable: "Venus_business_unit",
                        principalColumn: "ID_Venus_business_unit",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_po_after_Venus_department_departmentID_Venus_department",
                        column: x => x.departmentID_Venus_department,
                        principalTable: "Venus_department",
                        principalColumn: "ID_Venus_department",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_po_after_Venus_po_type_po_typeID_Venus_po_type",
                        column: x => x.po_typeID_Venus_po_type,
                        principalTable: "Venus_po_type",
                        principalColumn: "ID_Venus_po_type",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Venus_po_after_Venus_vendor_vendorID_Venus_vendor",
                        column: x => x.vendorID_Venus_vendor,
                        principalTable: "Venus_vendor",
                        principalColumn: "ID_Venus_vendor",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contract_quarter",
                columns: table => new
                {
                    ID_Contract_quarter = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    trackingID_Contract_tracking = table.Column<long>(nullable: true),
                    quarter = table.Column<int>(nullable: false),
                    week = table.Column<string>(nullable: false),
                   
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract_quarter", x => x.ID_Contract_quarter);
                    table.ForeignKey(
                        name: "FK_Contract_quarter_Contract_tracking_trackingID_Contract_tracking",
                        column: x => x.trackingID_Contract_tracking,
                        principalTable: "Contract_tracking",
                        principalColumn: "ID_Contract_tracking",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
          name: "EmailTable",
           columns: table => new
           {
               ID_Email = table.Column<long>(nullable: false)
                .Annotation("SqlServer:Identity", "1,1"),

               SystemId = table.Column<long>(nullable: true),

           },
           constraints: table =>
           {
               table.PrimaryKey("PK_Email", x => x.ID_Email);

               table.ForeignKey(
                   name: "FK_Emailtabel_Venus_grni_raw_ID_Venus_grni",
                   column: x => x.SystemId,
                   principalTable: "Venus_grni_raw",
                   principalColumn: "ID_Venus_grni_raw",
                   onDelete: ReferentialAction.Cascade

                   );


           });


            migrationBuilder.CreateIndex(
                name: "IX_Contract_actual_buyerID_Contract_buyer",
                table: "Contract_actual",
                column: "buyerID_Contract_buyer");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_actual_commodity_groupID_Contract_commodity_group",
                table: "Contract_actual",
                column: "commodity_groupID_Contract_commodity_group");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_quarter_trackingID_Contract_tracking",
                table: "Contract_quarter",
                column: "trackingID_Contract_tracking");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_target_buyerID_Contract_buyer",
                table: "Contract_target",
                column: "buyerID_Contract_buyer");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_target_commodity_groupID_Contract_commodity_group",
                table: "Contract_target",
                column: "commodity_groupID_Contract_commodity_group");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_tracking_buyerID_Contract_buyer",
                table: "Contract_tracking",
                column: "buyerID_Contract_buyer");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_tracking_commodity_groupID_Contract_commodity_group",
                table: "Contract_tracking",
                column: "commodity_groupID_Contract_commodity_group");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_tracking_period_coverageID_Contract_period_coverage",
                table: "Contract_tracking",
                column: "period_coverageID_Contract_period_coverage");

            migrationBuilder.CreateIndex(
                name: "IX_Contract_tracking_supply_categoryID_Contract_supply_category",
                table: "Contract_tracking",
                column: "supply_categoryID_Contract_supply_category");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_grni_business_unitID_Venus_business_unit",
                table: "Venus_grni",
                column: "business_unitID_Venus_business_unit");
     

            migrationBuilder.CreateIndex(
                name: "IX_Venus_grni_departmentID_Venus_department",
                table: "Venus_grni",
                column: "departmentID_Venus_department");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_grni_po_typeID_Venus_po_type",
                table: "Venus_grni",
                column: "po_typeID_Venus_po_type");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_grni_vendorID_Venus_vendor",
                table: "Venus_grni",
                column: "vendorID_Venus_vendor");


            migrationBuilder.CreateIndex(
                name: "IX_Venus_open_po_business_unitID_Venus_business_unit",
                table: "Venus_open_po",
                column: "business_unitID_Venus_business_unit");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_open_po_departmentID_Venus_department",
                table: "Venus_open_po",
                column: "departmentID_Venus_department");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_open_po_po_typeID_Venus_po_type",
                table: "Venus_open_po",
                column: "po_typeID_Venus_po_type");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_open_po_vendorID_Venus_vendor",
                table: "Venus_open_po",
                column: "vendorID_Venus_vendor");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_po_after_business_unitID_Venus_business_unit",
                table: "Venus_po_after",
                column: "business_unitID_Venus_business_unit");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_po_after_departmentID_Venus_department",
                table: "Venus_po_after",
                column: "departmentID_Venus_department");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_po_after_po_typeID_Venus_po_type",
                table: "Venus_po_after",
                column: "po_typeID_Venus_po_type");

            migrationBuilder.CreateIndex(
                name: "IX_Venus_po_after_vendorID_Venus_vendor",
                table: "Venus_po_after",
                column: "vendorID_Venus_vendor");
            migrationBuilder.CreateIndex(
                name: "IX_EmailTable_grnirawID_Venus_grni_raw",
                table: "EmailTable",
                column: "SystemId");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
    name: "EmailTable");
            migrationBuilder.DropTable(
                name: "Contract_actual");

            migrationBuilder.DropTable(
                name: "Contract_quarter");

            migrationBuilder.DropTable(
                name: "Contract_target");

            migrationBuilder.DropTable(
                name: "Venus_grni");

            migrationBuilder.DropTable(
                name: "Venus_grni_raw");

            migrationBuilder.DropTable(
                name: "Venus_open_po");

            migrationBuilder.DropTable(
                name: "Venus_opo_raw");

            migrationBuilder.DropTable(
                name: "Venus_po_after");

            migrationBuilder.DropTable(
                name: "Venus_poa_raw");

            migrationBuilder.DropTable(
                name: "Contract_tracking");

            migrationBuilder.DropTable(
                name: "Venus_business_unit");

            migrationBuilder.DropTable(
                name: "Venus_department");

            migrationBuilder.DropTable(
                name: "Venus_po_type");

            migrationBuilder.DropTable(
                name: "Venus_vendor");

            migrationBuilder.DropTable(
                name: "Contract_buyer");

            migrationBuilder.DropTable(
                name: "Contract_commodity_group");

            migrationBuilder.DropTable(
                name: "Contract_period_coverage");

            migrationBuilder.DropTable(
                name: "Contract_supply_category");
        }
    }
}
