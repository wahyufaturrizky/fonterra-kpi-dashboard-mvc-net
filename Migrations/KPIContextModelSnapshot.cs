﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using hera.Models;

namespace hera.Migrations
{
    [DbContext(typeof(KPIContext))]
    partial class KPIContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("hera.Models.Contract_actual", b =>
                {
                    b.Property<long>("ID_Contract_actual")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long?>("buyerID_Contract_buyer")
                        .HasColumnType("bigint");

                    b.Property<long?>("commodity_groupID_Contract_commodity_group")
                        .HasColumnType("bigint");

                    b.Property<bool>("direct")
                        .HasColumnType("bit");

                    b.Property<DateTime>("month")
                        .HasColumnType("datetime2");

                    b.Property<float>("value")
                        .HasColumnType("real");

                    b.HasKey("ID_Contract_actual");

                    b.HasIndex("buyerID_Contract_buyer");

                    b.HasIndex("commodity_groupID_Contract_commodity_group");

                    b.ToTable("Contract_actual");
                });

            modelBuilder.Entity("hera.Models.Contract_buyer", b =>
                {
                    b.Property<long>("ID_Contract_buyer")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Contract_buyer");

                    b.ToTable("Contract_buyer");
                });

            modelBuilder.Entity("hera.Models.Contract_commodity_group", b =>
                {
                    b.Property<long>("ID_Contract_commodity_group")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Contract_commodity_group");

                    b.ToTable("Contract_commodity_group");
                });

            modelBuilder.Entity("hera.Models.Contract_period_coverage", b =>
                {
                    b.Property<long>("ID_Contract_period_coverage")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("period")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Contract_period_coverage");

                    b.ToTable("Contract_period_coverage");
                });

            modelBuilder.Entity("hera.Models.Contract_quarter", b =>
                {
                    b.Property<long>("ID_Contract_quarter")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int> ("quarter")
                        .HasColumnType("int");

                    b.Property<long?>("trackingID_Contract_tracking")
                        .HasColumnType("bigint");
                    b.Property<string>("week")
                  .HasColumnType("nvarchar(max)");



                    b.HasKey("ID_Contract_quarter");

                    b.HasIndex("trackingID_Contract_tracking");

                    b.ToTable("Contract_quarter");
                });

            modelBuilder.Entity("hera.Models.Contract_supply_category", b =>
                {
                    b.Property<long>("ID_Contract_supply_category")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Contract_supply_category");

                    b.ToTable("Contract_supply_category");
                });

            modelBuilder.Entity("hera.Models.Contract_target", b =>
                {
                    b.Property<long>("ID_Contract_target")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long?>("buyerID_Contract_buyer")
                        .HasColumnType("bigint");

                    b.Property<long?>("commodity_groupID_Contract_commodity_group")
                        .HasColumnType("bigint");

                    b.Property<bool>("direct")
                        .HasColumnType("bit");

                    b.Property<DateTime>("month")
                        .HasColumnType("datetime2");

                    b.Property<float>("value")
                        .HasColumnType("real");

                    b.HasKey("ID_Contract_target");

                    b.HasIndex("buyerID_Contract_buyer");

                    b.HasIndex("commodity_groupID_Contract_commodity_group");

                    b.ToTable("Contract_target");
                });

            modelBuilder.Entity("hera.Models.Contract_tracking", b =>
                {
                    b.Property<long>("ID_Contract_tracking")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long?>("buyerID_Contract_buyer")
                        .HasColumnType("bigint");

                    b.Property<long?>("commodity_groupID_Contract_commodity_group")
                        .HasColumnType("bigint");

                    b.Property<bool>("completed_count")
                        .HasColumnType("bit");

                    b.Property<string>("contract_ID")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("direct")
                        .HasColumnType("bit");

                    b.Property<DateTime>("expiry_date")
                        .HasColumnType("datetime2");

                    b.Property<float>("nzd")
                        .HasColumnType("real");

                    b.Property<long?>("period_coverageID_Contract_period_coverage")
                        .HasColumnType("bigint");

                    b.Property<string>("remarks_last_week")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("remarks_this_week")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier_ID")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("supply_categoryID_Contract_supply_category")
                        .HasColumnType("bigint");

                    b.HasKey("ID_Contract_tracking");

                    b.HasIndex("buyerID_Contract_buyer");

                    b.HasIndex("commodity_groupID_Contract_commodity_group");

                    b.HasIndex("period_coverageID_Contract_period_coverage");

                    b.HasIndex("supply_categoryID_Contract_supply_category");

                    b.ToTable("Contract_tracking");
                });

            modelBuilder.Entity("hera.Models.Venus_business_unit", b =>
                {
                    b.Property<long>("ID_Venus_business_unit")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Venus_business_unit");

                    b.ToTable("Venus_business_unit");
                });

            modelBuilder.Entity("hera.Models.Venus_department", b =>
                {
                    b.Property<long>("ID_Venus_department")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Venus_department");

                    b.ToTable("Venus_department");
                });

            
            modelBuilder.Entity("hera.Models.Venus_grni_raw", b =>
                {
                    b.Property<long>("ID_Venus_grni_raw")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<float>("amount")
                        .HasColumnType("real");

                    b.Property<bool>("archived")
                        .HasColumnType("bit");

                    b.Property<DateTime>("doc_date")
                        .HasColumnType("datetime2");

                    b.Property<string>("doc_number")
                        .HasColumnType("nvarchar(max)");
                    b.Property<float>("batas_bawah")
              .HasColumnType("real");
                    b.Property<float>("batas_atas")
                  .HasColumnType("real");

                   
                    b.Property<int>("status_add_email")
                        .HasColumnType("int");
                    b.Property<string>("doc_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("pairID_Venus_grni_raw")
                        .HasColumnType("bigint");

                    b.Property<string>("po_currency")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("po_date")
                        .HasColumnType("datetime2");

                    b.Property<string>("po_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("po_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("remark")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier_code")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier_name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("unit")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Venus_grni_raw");

                  

                    b.ToTable("Venus_grni_raw");
                });

            modelBuilder.Entity("hera.Models.Venus_grni", b =>
            {
                b.Property<long>("ID_Venus_grni")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("bigint")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<long>("amount")
                    .HasColumnType("bigint");

                b.Property<long?>("business_unitID_Venus_business_unit")
                    .HasColumnType("bigint");

                b.Property<long?>("departmentID_Venus_department")
                    .HasColumnType("bigint");

                b.Property<DateTime>("month")
                    .HasColumnType("datetime2");

                b.Property<string>("pic")
                    .HasColumnType("nvarchar(max)");

                b.Property<int>("po_age")
                    .HasColumnType("int");

                b.Property<string>("po_number")
                    .HasColumnType("nvarchar(max)");

                b.Property<long?>("po_typeID_Venus_po_type")
                    .HasColumnType("bigint");

                b.Property<string>("remarks")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("status")
                    .HasColumnType("nvarchar(max)");

                b.Property<long?>("vendorID_Venus_vendor")
                    .HasColumnType("bigint");
              


                b.HasKey("ID_Venus_grni");

                b.HasIndex("business_unitID_Venus_business_unit");

                b.HasIndex("departmentID_Venus_department");

                b.HasIndex("po_typeID_Venus_po_type");

                b.HasIndex("vendorID_Venus_vendor");
     
                b.ToTable("Venus_grni");
            });

            modelBuilder.Entity("hera.Models.Venus_open_po", b =>
                {
                    b.Property<long>("ID_Venus_open_po")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("amount")
                        .HasColumnType("bigint");

                    b.Property<long?>("business_unitID_Venus_business_unit")
                        .HasColumnType("bigint");

                    b.Property<long?>("departmentID_Venus_department")
                        .HasColumnType("bigint");

                    b.Property<DateTime>("month")
                        .HasColumnType("datetime2");

                    b.Property<string>("pic")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("po_age")
                        .HasColumnType("int");

                    b.Property<string>("po_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("po_typeID_Venus_po_type")
                        .HasColumnType("bigint");

                    b.Property<string>("remarks")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("vendorID_Venus_vendor")
                        .HasColumnType("bigint");

                    b.HasKey("ID_Venus_open_po");

                    b.HasIndex("business_unitID_Venus_business_unit");

                    b.HasIndex("departmentID_Venus_department");

                    b.HasIndex("po_typeID_Venus_po_type");

                    b.HasIndex("vendorID_Venus_vendor");

                    b.ToTable("Venus_open_po");
                });

            modelBuilder.Entity("hera.Models.Venus_opo_raw", b =>
                {
                    b.Property<long>("ID_Venus_open_po")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("account_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<float>("amount_to_receive")
                        .HasColumnType("real");

                    b.Property<bool>("archived")
                        .HasColumnType("bit");

                    b.Property<float>("cost_center")
                        .HasColumnType("real");

                    b.Property<DateTime>("gl_date")
                        .HasColumnType("datetime2");

                    b.Property<int>("last_status")
                        .HasColumnType("int");

                    b.Property<string>("line_description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("line_number")
                        .HasColumnType("int");

                    b.Property<string>("managrl_code_1")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("managrl_code_2")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("managrl_code_3")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("managrl_code_4")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("next_status")
                        .HasColumnType("int");

                    b.Property<int>("obj_acct")
                        .HasColumnType("int");

                    b.Property<string>("or_ty")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("order_co")
                        .HasColumnType("int");

                    b.Property<DateTime>("order_date")
                        .HasColumnType("datetime2");

                    b.Property<int>("order_number")
                        .HasColumnType("int");

                    b.Property<float>("ordered_amount")
                        .HasColumnType("real");

                    b.Property<string>("orig_ord_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("original_order_no")
                        .HasColumnType("int");

                    b.Property<float>("quantity_to_receive")
                        .HasColumnType("real");

                    b.Property<string>("second_item_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier_name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("supplier_number")
                        .HasColumnType("int");

                    b.Property<float>("unit_cost")
                        .HasColumnType("real");

                    b.HasKey("ID_Venus_open_po");

                    b.ToTable("Venus_opo_raw");
                });

            modelBuilder.Entity("hera.Models.Venus_po_after", b =>
                {
                    b.Property<long>("ID_Venus_po_after")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<long>("amount")
                        .HasColumnType("bigint");

                    b.Property<long?>("business_unitID_Venus_business_unit")
                        .HasColumnType("bigint");

                    b.Property<long?>("departmentID_Venus_department")
                        .HasColumnType("bigint");

                    b.Property<DateTime>("month")
                        .HasColumnType("datetime2");

                    b.Property<string>("pic")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("po_age")
                        .HasColumnType("int");

                    b.Property<string>("po_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("po_typeID_Venus_po_type")
                        .HasColumnType("bigint");

                    b.Property<string>("remarks")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long?>("vendorID_Venus_vendor")
                        .HasColumnType("bigint");

                    b.HasKey("ID_Venus_po_after");

                    b.HasIndex("business_unitID_Venus_business_unit");

                    b.HasIndex("departmentID_Venus_department");

                    b.HasIndex("po_typeID_Venus_po_type");

                    b.HasIndex("vendorID_Venus_vendor");

                    b.ToTable("Venus_po_after");
                });

            modelBuilder.Entity("hera.Models.Venus_po_type", b =>
                {
                    b.Property<long>("ID_Venus_po_type")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Venus_po_type");

                    b.ToTable("Venus_po_type");
                });

            modelBuilder.Entity("hera.Models.Venus_poa_raw", b =>
                {
                    b.Property<long>("ID_Venus_poa_raw")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("account_code")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("account_description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<float>("amount")
                        .HasColumnType("real");

                    b.Property<bool>("archived")
                        .HasColumnType("bit");

                    b.Property<int>("business_unit")
                        .HasColumnType("int");

                    b.Property<int>("company")
                        .HasColumnType("int");

                    b.Property<string>("currency_code")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("domestic_currency_code")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("invoice_date")
                        .HasColumnType("datetime2");

                    b.Property<string>("item_code")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("item_description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("payment_gl_date")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("payment_ref")
                        .HasColumnType("int");

                    b.Property<DateTime>("po_date")
                        .HasColumnType("datetime2");

                    b.Property<string>("po_description_line_1")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("po_description_line_2")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("po_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("po_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("search_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("stocking_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("stocking_type_description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("supplier_invoice_number")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("vendor_code")
                        .HasColumnType("int");

                    b.Property<string>("vendor_name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("voucher_doc_type")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("voucher_gl_date")
                        .HasColumnType("datetime2");

                    b.Property<int>("voucher_number")
                        .HasColumnType("int");

                    b.HasKey("ID_Venus_poa_raw");

                    b.ToTable("Venus_poa_raw");
                });

            modelBuilder.Entity("hera.Models.Venus_vendor", b =>
                {
                    b.Property<long>("ID_Venus_vendor")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ID_Venus_vendor");

                    b.ToTable("Venus_vendor");
                });
            modelBuilder.Entity("hera.Models.EmailTable", b =>
            {
                b.Property<long>("ID_Email")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("bigint")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<long?>("SystemId")
                    .HasColumnType("bigint");

                b.HasIndex("SystemId");
                b.HasKey("ID_Email");

                b.ToTable("EmailTable");
            });


            modelBuilder.Entity("hera.Models.Contract_actual", b =>
                {
                    b.HasOne("hera.Models.Contract_buyer", "buyer")
                        .WithMany("Contract_actual")
                        .HasForeignKey("buyerID_Contract_buyer");

                    b.HasOne("hera.Models.Contract_commodity_group", "commodity_group")
                        .WithMany("Contract_actual")
                        .HasForeignKey("commodity_groupID_Contract_commodity_group");
                });

            modelBuilder.Entity("hera.Models.Contract_quarter", b =>
                {
                    b.HasOne("hera.Models.Contract_tracking", "tracking")
                        .WithMany()
                        .HasForeignKey("trackingID_Contract_tracking");
                });

            modelBuilder.Entity("hera.Models.Contract_target", b =>
                {
                    b.HasOne("hera.Models.Contract_buyer", "buyer")
                        .WithMany("Contract_target")
                        .HasForeignKey("buyerID_Contract_buyer");

                    b.HasOne("hera.Models.Contract_commodity_group", "commodity_group")
                        .WithMany("Contract_target")
                        .HasForeignKey("commodity_groupID_Contract_commodity_group");
                });

            modelBuilder.Entity("hera.Models.Contract_tracking", b =>
                {
                    b.HasOne("hera.Models.Contract_buyer", "buyer")
                        .WithMany("Contract_tracking")
                        .HasForeignKey("buyerID_Contract_buyer");

                    b.HasOne("hera.Models.Contract_commodity_group", "commodity_group")
                        .WithMany("Contract_tracking")
                        .HasForeignKey("commodity_groupID_Contract_commodity_group");

                    b.HasOne("hera.Models.Contract_period_coverage", "period_coverage")
                        .WithMany("Contract_tracking")
                        .HasForeignKey("period_coverageID_Contract_period_coverage");

                    b.HasOne("hera.Models.Contract_supply_category", "supply_category")
                        .WithMany("Contract_tracking")
                        .HasForeignKey("supply_categoryID_Contract_supply_category");
                });

            modelBuilder.Entity("hera.Models.Venus_grni", b =>
                {
                    b.HasOne("hera.Models.Venus_business_unit", "business_unit")
                        .WithMany("Venus_grni")
                        .HasForeignKey("business_unitID_Venus_business_unit");

                    b.HasOne("hera.Models.Venus_department", "department")
                        .WithMany("Venus_grni")
                        .HasForeignKey("departmentID_Venus_department");

                    b.HasOne("hera.Models.Venus_po_type", "po_type")
                        .WithMany("Venus_grni")
                        .HasForeignKey("po_typeID_Venus_po_type");

                    b.HasOne("hera.Models.Venus_vendor", "vendor")
                        .WithMany("Venus_grni")
                        .HasForeignKey("vendorID_Venus_vendor");
                  
                });

          
            modelBuilder.Entity("hera.Models.Venus_open_po", b =>
                {
                    b.HasOne("hera.Models.Venus_business_unit", "business_unit")
                        .WithMany("Venus_open_po")
                        .HasForeignKey("business_unitID_Venus_business_unit");

                    b.HasOne("hera.Models.Venus_department", "department")
                        .WithMany("Venus_open_po")
                        .HasForeignKey("departmentID_Venus_department");

                    b.HasOne("hera.Models.Venus_po_type", "po_type")
                        .WithMany("Venus_open_po")
                        .HasForeignKey("po_typeID_Venus_po_type");

                    b.HasOne("hera.Models.Venus_vendor", "vendor")
                        .WithMany("Venus_open_po")
                        .HasForeignKey("vendorID_Venus_vendor");
                });

            modelBuilder.Entity("hera.Models.Venus_po_after", b =>
                {
                    b.HasOne("hera.Models.Venus_business_unit", "business_unit")
                        .WithMany("Venus_po_after")
                        .HasForeignKey("business_unitID_Venus_business_unit");

                    b.HasOne("hera.Models.Venus_department", "department")
                        .WithMany("Venus_po_after")
                        .HasForeignKey("departmentID_Venus_department");

                    b.HasOne("hera.Models.Venus_po_type", "po_type")
                        .WithMany("Venus_po_after")
                        .HasForeignKey("po_typeID_Venus_po_type");

                    b.HasOne("hera.Models.Venus_vendor", "vendor")
                        .WithMany("Venus_po_after")
                        .HasForeignKey("vendorID_Venus_vendor");
                });
            modelBuilder.Entity("hera.Models.EmailTable", b =>
            {
                b.HasOne("hera.Models.Venus_grni_raw", "VenusRaw")
                    .WithMany("EmailTable")
                    .HasForeignKey("SystemId");

            });
#pragma warning restore 612, 618
        }
    }
}
