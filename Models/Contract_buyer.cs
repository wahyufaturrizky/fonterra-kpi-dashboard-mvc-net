﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Contract_buyer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Contract_buyer { get; set; }

        public ICollection<Contract_tracking> Contract_tracking { get; set; }
        public ICollection<Contract_target> Contract_target { get; set; }
        public ICollection<Contract_actual> Contract_actual { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "Maximum 50 characters")]
        public string name { get; set; }
    }
}
