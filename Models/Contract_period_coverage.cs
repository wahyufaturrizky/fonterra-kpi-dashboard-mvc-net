﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Contract_period_coverage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Contract_period_coverage { get; set; }
        public ICollection<Contract_tracking> Contract_tracking { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "Maximum 50 characters")]
        public string period { get; set; }
    }
}
