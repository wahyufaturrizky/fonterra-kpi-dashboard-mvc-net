﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Contract_quarter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Contract_quarter { get; set; }
   //     public Contract_tracking tracking { get; set; }
        public int quarter { get; set; }
        public string week { get; set; }
      
        [Column("trackingID_Contract_tracking")]
        public long trackingID_Contract_tracking { get; set; }
        [ForeignKey("trackingID_Contract_tracking")]
        public Contract_tracking tracking { get; set; }


    }
}
