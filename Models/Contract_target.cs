﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Contract_target
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Contract_target { get; set; }
        public Contract_buyer buyer { get; set; }
        public Contract_commodity_group commodity_group { get; set; }
        public bool direct { get; set; }
        public DateTime month { get; set; }
        public float value { get; set; }
    }
}
