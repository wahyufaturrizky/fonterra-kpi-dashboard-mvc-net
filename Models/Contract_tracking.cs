﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Contract_tracking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Contract_tracking { get; set; }
        public string supplier { get; set; }
        public string supplier_ID { get; set; }
        public bool direct { get; set; }
        public Contract_commodity_group commodity_group { get; set; }
        public Contract_supply_category supply_category { get; set; }
        public float nzd { get; set; }
        public string contract_ID { get; set; }
        public DateTime expiry_date { get; set; }
        public Contract_buyer buyer { get; set; }
        public Contract_period_coverage period_coverage { get; set; }
        public bool completed_count { get; set; }
        public string remarks_last_week { get; set; }
        public string remarks_this_week { get; set; }
    }
}
