﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class EmailTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Email { get; set; }
       

        [Column("SystemId")]

        public long SystemId { get; set; }

        //[NotMapped]
        [ForeignKey("SystemId")]
        public Venus_grni_raw VenusRaw { get; set; }

    }
}
