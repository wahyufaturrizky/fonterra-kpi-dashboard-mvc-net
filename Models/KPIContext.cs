﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class KPIContext : DbContext
    {
        public KPIContext(DbContextOptions<KPIContext> options)
               : base(options)
        {
        }

        #region Contract
        public DbSet<Contract_commodity_group> Contract_commodity_group { get; set; }
        public DbSet<Contract_supply_category> Contract_supply_category { get; set; }
        public DbSet<Contract_buyer> Contract_buyer { get; set; }
        public DbSet<Contract_period_coverage> Contract_period_coverage { get; set; }
        public DbSet<Contract_tracking> Contract_tracking { get; set; }
        public DbSet<Contract_quarter> Contract_quarter { get; set; }
        public DbSet<Contract_target> Contract_target { get; set; }
        public DbSet<Contract_actual> Contract_actual { get; set; }
        #endregion

        #region Venus
        public DbSet<Venus_po_type> Venus_po_type { get; set; }
        public DbSet<Venus_department> Venus_department { get; set; }
        public DbSet<Venus_vendor> Venus_vendor { get; set; }
        public DbSet<Venus_business_unit> Venus_business_unit { get; set; }
        public DbSet<Venus_open_po> Venus_open_po { get; set; }
        public DbSet<Venus_grni> Venus_grni { get; set; }
        public DbSet<Venus_po_after> Venus_po_after { get; set; }
        public DbSet<Venus_opo_raw> Venus_opo_raw { get; set; }
        public DbSet<Venus_grni_raw> Venus_grni_raw { get; set; }
        public DbSet<Venus_poa_raw> Venus_poa_raw { get; set; }
        #endregion

        public DbSet<EmailTable> EmailTable { get; set; }
    }
}
