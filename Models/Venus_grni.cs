﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_grni
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_grni { get; set; }
     
        public string po_number { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }
        public DateTime month { get; set; }
        public long amount { get; set; }
        public string pic { get; set; }
        public string remarks { get; set; }

        [Column("departmentID_Venus_department")]
        public long departmentID_Venus_department { get; set; }
        [ForeignKey("departmentID_Venus_department")]
        public Venus_department department { get; set; }
        [Column("business_unitID_Venus_business_unit")]
        public long business_unitID_Venus_business_unit { get; set; }
        [ForeignKey("business_unitID_Venus_business_unit")]
        public Venus_business_unit business_unit { get; set; }
        [Column("vendorID_Venus_vendor")]
        public long vendorID_Venus_vendor { get; set; }
        [ForeignKey("vendorID_Venus_vendor")]
        public Venus_vendor vendor { get; set; }

        [Column("po_typeID_Venus_po_type")]
        public long po_typeID_Venus_po_type { get; set; }
        [ForeignKey("po_typeID_Venus_po_type")]
        public Venus_po_type po_type { get; set; }

    }
}
