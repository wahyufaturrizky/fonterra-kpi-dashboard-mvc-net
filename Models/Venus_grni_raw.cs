﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_grni_raw
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_grni_raw { get; set; }
        public string po_type { get; set; }
        public string po_number { get; set; }
        public DateTime po_date { get; set; }
        public string doc_type { get; set; }
         public int status_add_email { get; set; }
	public string doc_number { get; set; }
        public DateTime doc_date { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string remark { get; set; }
        public string po_currency { get; set; }
        public float amount { get; set; }
        public string unit { get; set; }
   	 public float batas_bawah { get; set; }
        public float batas_atas { get; set; }
        public long pairID_Venus_grni_raw { get; set; }
        public bool archived { get; set; }
 	public ICollection<EmailTable> EmailTable { get; set; }
      
    }
}
