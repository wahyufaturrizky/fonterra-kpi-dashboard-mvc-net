﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_open_po
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_open_po { get; set; }
        public Venus_po_type po_type { get; set; }
        public Venus_department department { get; set; }
        public Venus_vendor vendor { get; set; }
        public Venus_business_unit business_unit { get; set; }
        public int po_number { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }
        public DateTime month { get; set; }
        public long amount { get; set; }
        public string pic { get; set; }
        public string remarks { get; set; }
    }
}
