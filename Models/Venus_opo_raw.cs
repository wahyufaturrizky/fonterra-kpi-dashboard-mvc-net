﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_opo_raw
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_open_po { get; set; }
        public int order_number { get; set; }
        public string or_ty { get; set; }
        public float amount_to_receive { get; set; }
        public float ordered_amount { get; set; }
        public int order_co { get; set; }
        public int supplier_number { get; set; }
        public string supplier_name { get; set; }
        public string line_description { get; set; }
        public int last_status { get; set; }
        public int next_status { get; set; }
        public DateTime gl_date { get; set; }
        public DateTime order_date { get; set; }
        public float quantity_to_receive { get; set; }
        public float unit_cost { get; set; }
        public int original_order_no { get; set; }
        public string orig_ord_type { get; set; }
        public string account_number { get; set; }
        public float cost_center { get; set; }
        public int obj_acct { get; set; }
        public int line_number { get; set; }
        public string managrl_code_1 { get; set; }
        public string managrl_code_2 { get; set; }
        public string managrl_code_3 { get; set; }
        public string managrl_code_4 { get; set; }
        public string second_item_number { get; set; }
        public bool archived { get; set; }
    }
}
