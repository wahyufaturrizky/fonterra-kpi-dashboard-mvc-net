﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_poa_raw
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_poa_raw { get; set; }
        public int company { get; set; }
        public int payment_ref { get; set; }
        public DateTime voucher_gl_date { get; set; }
        public string supplier_invoice_number { get; set; }
        public DateTime invoice_date { get; set; }
        public string payment_gl_date { get; set; }
        public string voucher_doc_type { get; set; }
        public int voucher_number { get; set; }
        public int po_number { get; set; }
        public string po_type { get; set; }
        public DateTime po_date { get; set; }
        public string po_description_line_1 { get; set; }
        public string po_description_line_2 { get; set; }
        public int vendor_code { get; set; }
        public string vendor_name { get; set; }
        public string item_code { get; set; }
        public string item_description { get; set; }
        public string stocking_type { get; set; }
        public string stocking_type_description { get; set; }
        public string account_code { get; set; }
        public string account_description { get; set; }
        public int business_unit { get; set; }
        public float amount { get; set; }
        public string domestic_currency_code { get; set; }
        public string currency_code { get; set; }
        public string search_type { get; set; }
        public bool archived { get; set; }
    }
}
