﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class Venus_vendor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID_Venus_vendor { get; set; }
        public string name { get; set; }
        public ICollection<Venus_open_po> Venus_open_po { get; set; }
        public ICollection<Venus_grni> Venus_grni { get; set; }
        public ICollection<Venus_po_after> Venus_po_after { get; set; }
    }
}
