﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class AllProductsbyQuarter
    {
        public Array Target { get; set; }
        public Array Actual { get; set; }
    }
}
