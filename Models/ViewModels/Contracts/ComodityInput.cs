﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class ComodityInput
    {
        //Display
        public string Form { get; set; }
        //Column
        public long ID_Contract_commodity_group { get; set; }
        public string name{ get; set; }
     
    }
}
