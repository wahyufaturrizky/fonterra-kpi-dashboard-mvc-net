﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class CompletenessConverage
    {
        public Array FY { get; set; }
        public Array Completed { get; set; }
        public Array Uncompleted { get; set; }
    }
}
