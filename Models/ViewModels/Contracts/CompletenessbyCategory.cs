﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class CompletenessbyCategory
    {
        public Array CommodityCategory { get; set; }
        public Array CompleteCount { get; set; }
        public Array UncompleteCount { get; set; }
    }
}
