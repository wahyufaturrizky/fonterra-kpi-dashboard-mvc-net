﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class ContractGlobalModel
    {
        //Filter and Radiobutton
        public List<Contract_buyer> BuyerRadio { get; set; }
        public List<Contract_commodity_group> CommodityCategoryRadio { get; set; }
        public int BuyerFilter { get; set; }
        public int CommodityCategoryFilter { get; set; }
        public int SpendingTypeFilter { get; set; }

        //List load Dbase
        public List<Contract_buyer> Buyer { get; set; }
        public List<Contract_commodity_group> Commodity_Category { get; set; }
        public List<Contract_period_coverage> Contract_period_coverage { get; set; }
        public List<Contract_tracking> Contract_Tracking { get; set; }
        public List<Contract_target> Contract_Target { get; set; }
        public List<Contract_actual> Contract_Actual { get; set; }

        public ContractGlobalModel()
        {
            if (TotalCompleteness == null)
            {
                TotalCompleteness = new TotalCompleteness();
            }
            if (CompletenessConverage == null)
            {
                CompletenessConverage = new CompletenessConverage();
            }
            if (ContractsbyCategory == null)
            {
                ContractsbyCategory = new ContractsbyCategory();
            }
            if (CompletenessbyCategory == null)
            {
                CompletenessbyCategory = new CompletenessbyCategory();
            }
            if (AllProducts == null)
            {
                AllProducts = new AllProducts();
            }
            if (AllProductsbyQuarter == null)
            {
                AllProductsbyQuarter = new AllProductsbyQuarter();
            }
            if (ContractsValue == null)
            {
                ContractsValue = new ContractsValue();
            }
            if (YTDTargetvsActual == null)
            {
                YTDTargetvsActual = new YTDTargetvsActual();
            }
            if (DatatableContrats == null)
            {
                DatatableContrats = new List<Contract_tracking>();
            }
        }

        public TotalCompleteness TotalCompleteness { get; set; }
        public CompletenessConverage CompletenessConverage { get; set; }
        public ContractsbyCategory ContractsbyCategory { get; set; }
        public CompletenessbyCategory CompletenessbyCategory { get; set; }
        public AllProducts AllProducts { get; set; }
        public AllProductsbyQuarter AllProductsbyQuarter { get; set; }
        public ContractsValue ContractsValue { get; set; }
        public YTDTargetvsActual YTDTargetvsActual { get; set; }
        public List<Contract_tracking> DatatableContrats { get; set; }

    }
}
