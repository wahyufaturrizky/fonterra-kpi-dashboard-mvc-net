﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class ContractsValue
    {
        public Array CommodityCategory { get; set; }
        public Array CompleteSUM { get; set; }
        public Array UncompleteSUM { get; set; }
    }
}
