﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class ContractsbyCategory
    {
        public Array CommodityCategory { get; set; }
        public Array ContractsCount { get; set; }
    }
}
