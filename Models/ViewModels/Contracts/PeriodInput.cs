﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class PeriodInput
    {
        //Display
        public string Form { get; set; }
        //Column
        public long ID_Contract_period_coverage { get; set; }
        public string period{ get; set; }
     
    }
}
