﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class QuarterInput
    {
        //Display
        public string Form { get; set; }
        //Column
        public long tracking { get; set; }
        public long ID_Contract_quarter { get; set; }
        public string contract_ID { get; set; }
        public int quarter { get; set; }
        public string week { get; set; }

    }
}
