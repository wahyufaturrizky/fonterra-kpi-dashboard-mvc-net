﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelComodity
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<ComodityGridTable> ComodityTable { get; set; }
        public Contract_commodity_group comodity { get; set; } 
        public ComodityInput InputComodity { get; set; }
    }
    public class ComodityGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_commodity_group { get; set; }
       
        public string name { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
