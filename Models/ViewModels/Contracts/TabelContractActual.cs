﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelContractActual
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<ActualGridTable> ActualTable { get; set; }
        public Contract_buyer buyer { get; set; } 
        public Contract_commodity_group commodity_group { get; set; }
        public ActualInput InputActual { get; set; }
    }
    public class ActualGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_actual { get; set; }
        public string direct { get; set; }
        public DateTime month { get; set; }
        public float value { get; set; }
        public string buyer { get; set; }
        public string commodiy_group { get; set; }

        // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }

    }
}
