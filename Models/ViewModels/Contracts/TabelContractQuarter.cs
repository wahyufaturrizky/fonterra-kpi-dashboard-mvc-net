﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelContractQuarter
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<QuarterGridTable> QuarterTable { get; set; }
        public Contract_tracking tracking { get; set; }
        public QuarterInput InputQuarter { get; set; }

    }
    public class QuarterGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_quarter { get; set; }
        public string contract_ID { get; set; }
        public long tracking { get; set; }
        public string week {get;set;}
        public int quarter { get; set; }
       

        // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }

    }
}
