﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelContractTarget
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<TargetGridTable> TargetTable { get; set; }
        public Contract_buyer buyer { get; set; } 
        public Contract_commodity_group commodity_group { get; set; }
        public TargetInput InputTarget { get; set; }
    }
    public class TargetGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_target { get; set; }
        public string direct { get; set; }
        public DateTime month { get; set; }
        public float value { get; set; }
        public string buyer { get; set; }
        public string commodiy_group { get; set; }

     
    }
}
