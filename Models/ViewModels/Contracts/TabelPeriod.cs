﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelPeriod
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<PeriodGridTable> PeriodTable { get; set; }
        public Contract_period_coverage period { get; set; } 
        public PeriodInput InputPeriod { get; set; }
    }
    public class PeriodGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_period_coverage { get; set; }
       
        public string period { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
