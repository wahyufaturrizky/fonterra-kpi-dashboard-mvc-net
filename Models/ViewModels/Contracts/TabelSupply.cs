﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TabelSupply
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<SupplyGridTable> SupplyTable { get; set; }
        public Contract_supply_category supply { get; set; } 
        public SupplyInput InputSupply { get; set; }
    }
    public class SupplyGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_supply_category { get; set; }
       
        public string name { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
