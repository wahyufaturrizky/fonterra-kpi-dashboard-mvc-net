﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TableContract
    {
        //Display
        public string Form { get; set; }
        //Column
        public long ID_Contract_tracking { get; set; }
        public string supplier { get; set; }
        public string supplier_ID { get; set; }
        public bool direct { get; set; }
        public long commodity_group { get; set; }
        public long supply_category { get; set; }
        public float nzd { get; set; }
        public string contract_ID { get; set; }
        public DateTime expiry_date { get; set; }
        public long buyer { get; set; }
        public long period_coverage { get; set; }
        public bool completed_count { get; set; }
        public string remarks_last_week { get; set; }
        public string remarks_this_week { get; set; }
    }
}
