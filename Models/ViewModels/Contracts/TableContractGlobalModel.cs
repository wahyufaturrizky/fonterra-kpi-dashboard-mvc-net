﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TableContractGlobalModel
    {
        public TableContractGlobalModel()
        {
            if (ContractInput == null)
            {
                ContractInput = new TableContract();
            }
        }

        public List<Contract_buyer> BuyerRadio { get; set; }
        public List<Contract_commodity_group> CommodityCategoryRadio { get; set; }

        public int BuyerFilter { get; set; }
        public int CommodityCategoryFilter { get; set; }
        public int SpendingTypeFilter { get; set; }
        public List<Contract_buyer> Buyer { get; set; }
        public List<Contract_commodity_group> Commodity_Category { get; set; }

        public string[] idDeleteArray { get; set; }
        public string idDeleteString { get; set; }
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }

        public List<ContractGridTable> ContractTable { get; set; }
        public TableContract ContractInput { get; set; }
    }
    public class ContractGridTable
    {
        public bool checkID { get; set; }
        public long ID_Contract_tracking { get; set; }
        public string supplier { get; set; }
        public string supplier_ID { get; set; }
        public string direct { get; set; }
        public long commodity_groupID { get; set; }
        public string commodity_group { get; set; }
        public long supply_categoryID { get; set; }
        public string supply_category { get; set; }
        public float nzd { get; set; }
        public string contract_ID { get; set; }
        public DateTime expiry_date { get; set; }
        public long buyerID { get; set; }
        public string buyer { get; set; }
        public long period_coverageID { get; set; }
        public string period_coverage { get; set; }
        public string completed_count { get; set; }
        public string remarks_last_week { get; set; }
        public string remarks_this_week { get; set; }
        public bool directBOOL { get; set; }
    }
}
