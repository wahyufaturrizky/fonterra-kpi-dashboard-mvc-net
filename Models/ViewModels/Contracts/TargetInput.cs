﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Contracts
{
    public class TargetInput
    {
        //Display
        public string Form { get; set; }
        //Column
        public long ID_Contract_target { get; set; }
        public long buyer { get; set; }
        public long commodity_group { get; set; }
        public bool direct { get; set; }
        public DateTime month { get; set; }
        public float value { get; set; }

    }
}
