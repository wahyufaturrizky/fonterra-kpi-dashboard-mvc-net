﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class TotalCompleteness
    {
        public int Uncompleted { get; set; }
        public int Completed { get; set; }
        public Contract_tracking Contract_tracking { get; set; }
    }
}
