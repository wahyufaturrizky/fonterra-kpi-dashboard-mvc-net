﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models.ViewModels.Contracts
{
    public class YTDTargetvsActual
    {
        public float Target { get; set; }
        public float Actual { get; set; }
    }
}
