﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GlobalControlAccessor;
using Microsoft.AspNetCore.Http;


using System.ComponentModel.DataAnnotations.Schema;


namespace hera.Models.ViewModels.Email
{
    public class SendEmail
    {


        public IFormFile File { get; set; }

        public List<EmailTable> ViewDatatable { get; set; }
          
        public List<Venus_grni_raw> VenusRaw { get; set; }
        public List<EmailUpload> EmailUpload1 { get; set; }
        public class EmailUpload
        {
            public long ID_Email { get; set; }
            public long SystemId { get; set; }
            public string po_number { get; set; }
            public float amount { get; set; }


            public int EmailSendButton { get; set; }

            public string ReasonOther { get; set; }
            public string otherText { get; set; }


        }

     
        public int EmailSendButton { get; set; }
   

 
        public int ID_Email { get; set; }

    

    }
}
