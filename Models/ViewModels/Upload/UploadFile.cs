﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GlobalControlAccessor;
using Microsoft.AspNetCore.Http;

namespace hera.Models.ViewModels.Upload
{
    public class UploadFile
    {
        [Required(ErrorMessage = "Please select an Excel file")]
        [FileType("xls|xlsx", ErrorMessage = "Must be an Excel File")]
        [Display(Name = "Import File")]
        public IFormFile File { get; set; }
        public List<Contract_tracking> Contract_tracking { get; set; }
        public List<UploadContracts> UploadContracts { get; set; }
        public List<UploadGRNI> UploadGRNI { get; set; }

        public List<Venus_grni_raw> Venus_grni_raw { get; set; }
        public List<Venus_grni> Venus_grni { get; set; }

        public UploadFile()
        {
            if (Contract_tracking == null)
            {
                Contract_tracking = new List<Contract_tracking>();
            }
            if (UploadContracts == null)
            {
                UploadContracts = new List<UploadContracts>();
            }
            if (Venus_grni_raw == null)
            {
                Venus_grni_raw = new List<Venus_grni_raw>();
            }
        }
    }

    public class UploadContracts
    {
        public string supplier { get; set; }
        public string supplier_ID { get; set; }
        public string direct { get; set; }
        public string commodity_group { get; set; }
        public string supply_category { get; set; }
        public string nzd { get; set; }
        public string contract_ID { get; set; }
        public string expiry_date { get; set; }
        public string buyer { get; set; }
        public string period_coverage { get; set; }
        public string completed_count { get; set; }
        //public string remarks_last_week { get; set; }
        public string remarks_this_week { get; set; }
    }
    public class UploadGRNI
    {
       
        public long ID_Venus_grni_raw { get; set; }
        public string po_type { get; set; }
        public string po_number { get; set; }
        public string po_date { get; set; }
        public string doc_type { get; set; }
        public string doc_number { get; set; }
        public string doc_date { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string remark { get; set; }
        public string po_currency { get; set; }
        public float amount { get; set; }
        public string unit { get; set; }
      
       // public bool archived { get; set; }
        public string department { get; set; }
        public string vendor { get; set; }
        public string business { get; set; }
        public string pic { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }

        public long pairID_Venus_grni_raw { get; set; }


    }
   
}
