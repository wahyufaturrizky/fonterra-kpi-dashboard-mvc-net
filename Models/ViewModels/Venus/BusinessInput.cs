﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class BusinessInput
    {
        //Display
        public string Form { get; set; }
        //Column
   
        public long ID_Venus_business_unit { get; set; }
        public string name{ get; set; }
     
    }
}
