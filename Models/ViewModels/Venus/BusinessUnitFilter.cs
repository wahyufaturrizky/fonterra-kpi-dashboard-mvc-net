﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class BusinessUnitFilter
    {
        public long ID_Venus_business_unit { get; set; }
        public string name { get; set; }
        public bool IsCheckedBusinessUnit { get; set; }
    }
}
