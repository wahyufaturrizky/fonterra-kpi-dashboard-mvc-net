﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class MonthlyGRNI
    {
        public string Values { get; set; }
        public string Remarks { get; set; }
    }
}
