﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class MonthlyGRNIFigures
    {
        public Array CasesCount { get; set; }
        public Array Open { get; set; }
        public Array Closed { get; set; }
    }
}
