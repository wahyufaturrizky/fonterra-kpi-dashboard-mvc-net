﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class OpenPOInput
    {
        //Display
        public string Form { get; set; }
        //Column

        public long ID_Venus_open_po { get; set; }
        public long po_type { get; set; }
        public long department { get; set; }
        public long vendor { get; set; }
        public long business_unit { get; set; }
        public int po_number { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }
        public DateTime month { get; set; }
        public long amount { get; set; }
        public string pic { get; set; }
        public string remarks { get; set; }

    }
}
