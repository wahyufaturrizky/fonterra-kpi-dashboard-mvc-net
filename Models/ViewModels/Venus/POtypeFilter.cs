﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace hera.Models
{
    public class POTypeFilter
    {
        public long ID_Venus_po_type { get; set; }
        public string name { get; set; }
        public bool IsCheckedPOType { get; set; }
    }
}
