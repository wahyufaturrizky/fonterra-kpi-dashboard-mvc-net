﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TabelBusiness
    {

        public string Form { get; set; }
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<BusinessGridTable> BusinessTable { get; set; }
        public Venus_business_unit business_unit { get; set; } 
        public BusinessInput InputBusiness { get; set; }
    }
    public class BusinessGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_business_unit { get; set; }
       
        public string name { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
