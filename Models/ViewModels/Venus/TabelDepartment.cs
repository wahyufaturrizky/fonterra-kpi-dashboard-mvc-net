﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TabelDepartment
    {
      
        
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }


        public List<DepartmentGridTable> DepartmentTable { get; set; }
        public Venus_department department { get; set; } 
        public DepartmentInput InputDepartment { get; set; }
    }
    public class DepartmentGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_department { get; set; }
       
        public string name { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
