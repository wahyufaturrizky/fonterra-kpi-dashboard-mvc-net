﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TabelOPORaw
    {


        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }


        public List<OPOGridTable> OpenPOTable { get; set; }
        public Venus_po_type po_type { get; set; }
        public Venus_department department { get; set; }
        public Venus_vendor vendor { get; set; }
        public Venus_business_unit business_unit { get; set; }
        public OpenPOInput InputOpenPO { get; set; }
    }
    public class OPOGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_vendor { get; set; }

        public long ID_Venus_open_po { get; set; }
        public string po_type { get; set; }
        public string department { get; set; }
        public string vendor { get; set; }
        public string business_unit { get; set; }
        public int po_number { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }
        public DateTime month { get; set; }
        public long amount { get; set; }
        public string pic { get; set; }
        public string remarks { get; set; }

        // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }

    }
}
