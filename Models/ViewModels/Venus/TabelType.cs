﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TabelType
    {
      
        
        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }

        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
     

        public List<TypeGridTable> TypeTable { get; set; }
        public Venus_po_type type { get; set; } 
        public TypeInput InputType { get; set; }
    }
    public class TypeGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_po_type { get; set; }
       
        public string name { get; set; }

     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
