﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TabelVendor
    {


        public string[] idDeleteArray { get; set; }
    
        public string idDeleteString { get; set; }
       
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }


        public List<VendorGridTable> VendorTable { get; set; }
        public Venus_vendor vendor { get; set; } 
        public VendorInput InputVendor { get; set; }
    }
    public class VendorGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_vendor { get; set; }
       
        public string name { get; set; }
     
       // public int department { get; set; }
        //public int business_unit { get; set; }
        //public  int vendor { get; set; }
     
    }
}
