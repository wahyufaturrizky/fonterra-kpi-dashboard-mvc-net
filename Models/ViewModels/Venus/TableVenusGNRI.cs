﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TableVenusGNRI
    {
        //Display
        public string Form { get; set; }
        //Column
        public long ID_Venus_grni_raw { get; set; }
        public string po_type { get; set; }
        public string po_number { get; set; }
        public string doc_type { get; set; }
        public string doc_number { get; set; }
        public DateTime doc_date { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string remark { get; set; }
        public string po_currency { get; set; }
        public float amount { get; set; }
        public string unit { get; set; }
        public int businessunit { get; set; }
        public int vendor { get; set; }

        public int department { get; set; }
 
 public string statusAksi { get; set; }
    public long pairID_Venus_grni_raw { get; set; }
        public string type_name { get; set; }
        public string pic { get; set; }
    }
}
