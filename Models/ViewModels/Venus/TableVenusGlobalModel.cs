﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class TableVenusGlobalModel
    {
        public List<POTypeFilter> POTypeFilter { get; set; }
        public string[] POTypeFilterArray { get; set; }

        public TableVenusGlobalModel()
        {
            if (GNRIInput == null)
            {
                GNRIInput = new TableVenusGNRI();
            }
        }
        public string[] idDeleteArray { get; set; }
        public string idDeleteString { get; set; }
        public string[] idAddEmailArray { get; set; }
        public string idAddEmailString { get; set; }
        public string URLAction { get; set; }
        public string DeleteMessage { get; set; }
	 public string AddEmailMessage { get; set; }

        public List<GNRIGridTable> GNRITable { get; set; }
        public TableVenusGNRI GNRIInput { get; set; }
    }
    public class GNRIGridTable
    {
        public bool checkID { get; set; }
        public long ID_Venus_grni_raw { get; set; }
        public string po_type { get; set; }
        public string po_number { get; set; }
        public DateTime po_date { get; set; }
        public string doc_type { get; set; }
        public string doc_number { get; set; }
        public DateTime doc_date { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public string remark { get; set; }
        public string po_currency { get; set; }
        public float amount { get; set; }
        public string unit { get; set; }
        public long pair { get; set; }
        public bool archived { get; set; }

public float minimum { get; set; }
        public float maksimum { get; set; }
        public string department { get; set; }
        public string vendor { get; set; }
        public string business_unit { get; set; }
        public string pic { get; set; }
        public int po_age { get; set; }
        public string status { get; set; }
        public int status_add_email { get; set; }
 public long pairID_Venus_grni_raw { get; set; }

    }
}
