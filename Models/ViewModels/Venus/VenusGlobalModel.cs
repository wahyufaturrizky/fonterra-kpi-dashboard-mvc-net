﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hera.Models.ViewModels.Contracts;

namespace hera.Models.ViewModels.Venus
{
    public class VenusGlobalModel
    {

        //List load Dbase
        public List<Venus_po_type> POType { get; set; }
        public List<Venus_department> Department { get; set; }
        public List<Venus_business_unit> BusinessUnit { get; set; }
        public List<Venus_grni> Venus_grni { get; set; }

        //Filter and Checkbox
        public List<POTypeFilter> POTypeFilter { get; set; }
        public List<DepartmentFilter> UserFilter { get; set; }
        public List<BusinessUnitFilter> BusinessUnitFilter { get; set; }

        public string[] POTypeFilterArray { get; set; }
        public string[] UserFilterArray { get; set; }
        public string[] BusinessUnitFilterArray { get; set; }

        public VenusGlobalModel()
        {
            if (MonthlyGRNI == null)
            {
                MonthlyGRNI = new List<MonthlyGRNI>();
            }
            if (MonthlyGRNIFigures == null)
            {
                MonthlyGRNIFigures = new MonthlyGRNIFigures();
            }
        }

        public List<MonthlyGRNI> MonthlyGRNI { get; set; }
        public MonthlyGRNIFigures MonthlyGRNIFigures { get; set; }
        public List<Venus_grni> MonthlyGRNIDatatable { get; set; }

    }
}
