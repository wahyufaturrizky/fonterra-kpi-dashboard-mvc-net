﻿
$(function () {
    $(".ancNewBusiness").click(function () {
        debugger;
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusBusiness/Input',
            contentType: "application/json; charset=utf-8",
            //data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancEditBusiness").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusBusiness/Input',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeleteBusiness").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusBusiness/Delete',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeleteAllBusiness").click(function () {
        debugger;
        var selectedID = document.getElementsByName("item.checkID");
        var IDchecked = [];
        for (var i = 0; i < selectedID.length; i++) {
            var checkbox = selectedID[i];
            if (checkbox.checked) IDchecked.push(checkbox.id);
        }
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusBusiness/DeleteAll',
            contentType: "application/json; charset=utf-8",
            data: { "Id": JSON.stringify(IDchecked) },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});