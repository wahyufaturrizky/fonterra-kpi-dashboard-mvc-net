﻿
$(function () {
    $(".ancNewDepartment").click(function () {
        debugger;
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusDepartment/Input',
            contentType: "application/json; charset=utf-8",
            //data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancEditDepartment").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusDepartment/Input',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeleteDepartment").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusDepartment/Delete',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeleteAllDepartment").click(function () {
        debugger;
        var selectedID = document.getElementsByName("item.checkID");
        var IDchecked = [];
        for (var i = 0; i < selectedID.length; i++) {
            var checkbox = selectedID[i];
            if (checkbox.checked) IDchecked.push(checkbox.id);
        }
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/VenusDepartment/DeleteAll',
            contentType: "application/json; charset=utf-8",
            data: { "Id": JSON.stringify(IDchecked) },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});