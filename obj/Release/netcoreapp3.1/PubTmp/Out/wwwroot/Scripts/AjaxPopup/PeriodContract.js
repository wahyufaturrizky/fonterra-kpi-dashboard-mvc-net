﻿
$(function () {
    $(".ancNewPeriod").click(function () {
        debugger;
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/ContractPeriod/Input',
            contentType: "application/json; charset=utf-8",
            //data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancEditPeriod").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/ContractPeriod/Input',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeletePeriod").click(function () {
        debugger;
        var $buttonClicked = $(this);
        var id = $buttonClicked.attr('data-id');
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/ContractPeriod/Delete',
            contentType: "application/json; charset=utf-8",
            data: { "Id": id },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});

$(function () {
    $(".ancDeleteAllPeriod").click(function () {
        debugger;
        var selectedID = document.getElementsByName("item.checkID");
        var IDchecked = [];
        for (var i = 0; i < selectedID.length; i++) {
            var checkbox = selectedID[i];
            if (checkbox.checked) IDchecked.push(checkbox.id);
        }
        var options = { "backdrop": "static", keyboard: true };
        $.ajax({
            type: "GET",
            url: '/ContractPeriod/DeleteAll',
            contentType: "application/json; charset=utf-8",
            data: { "Id": JSON.stringify(IDchecked) },
            datatype: "json",
            success: function (data) {
                debugger;
                $('#myModalDialog').html(data);
                $('#myModal').modal(options);
                $('#myModal').modal('show');

            },
            error: function () {
                alert("Dynamic content load failed.");
            }
        });
    });
    $("#closbtn").click(function () {
        $('#myModal').modal('hide');
    });
});