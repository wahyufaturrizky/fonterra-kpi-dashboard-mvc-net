
new Chart(document.getElementById("totalCompleteness"), {
    "type": "pie",
    "data": {
        "labels": ["Completed", "Uncompleted"],
        "datasets": [{
            "label": "Total Completeness",
            "data": [TCCompleted, TCUncompleted],
            "backgroundColor": ["#00d6b4", "#ff9f89"],
            "hoverBackgroundColor": ["#00d6b4", "#ff9f89"],
        }]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest"
        }
    }
});

new Chart(document.getElementById("CompletenessCoveragebyFY"), {
    "type": "horizontalBar",
    "data": {
        "labels": CCFY,
        "datasets": [
            {
                "label": "Completed",
                "data": CCCompleted,
                "fill": true,
                "backgroundColor": "#00d6b4",
                "hoverBackgroundColor": "#00d6b4",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            },
            {
                "label": "Uncompleted",
                "data": CCUncompleted,
                "fill": true,
                "backgroundColor": "#ff9f89",
                "hoverBackgroundColor": "#ff9f89",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            }
        ]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },
        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("ContractsbyCategory"), {
    "type": "pie",
    "data": {
        "labels": CBCCommodity,
        "datasets": [{
            "label": "Contracts by Category",
            "data": CBCCount,
            "backgroundColor": [
                "#ff9f89",
                "#00d6b4",
                "#fd77a4",
                "#419ef9",
                "#ffc15e",
                "#ffe0b5",
                "#c6d8ff"
            ],
            "hoverBackgroundColor": [
                "#ff9f89",
                "#00d6b4",
                "#fd77a4",
                "#419ef9",
                "#ffc15e",
                "#ffe0b5",
                "#c6d8ff"
            ]
        }]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true
    }
});

new Chart(document.getElementById("CompletenessbyCategory"), {
    "type": "horizontalBar",
    "data": {
        "labels": CoBCCommodity,
        "datasets": [
            {
                "label": "Completed",
                "data": CoBCCountComplete,
                "fill": true,
                "backgroundColor": "#00d6b4",
                "hoverBackgroundColor": "#00d6b4",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            },
            {
                "label": "Uncompleted",
                "data": CoBCCountUncomplete,
                "fill": true,
                "backgroundColor": "#ff9f89",
                "hoverBackgroundColor": "#ff9f89",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            }
        ]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "intersect": 0,
            "position": "nearest",
            "mode": "index"
        },
        "responsive": true,

        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("allProducts"), {
    "type": "bar",
    "data": {
        "labels": ["AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
        "datasets": [
            {
                "label": "Target",
                "data": APTarget,
                "fill": true,
                "backgroundColor": "#ffc15e",
                "hoverBackgroundColor": "#ffc15e",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            },
            {
                "label": "Actual",
                "data": APActual,
                "fill": true,
                "backgroundColor": "#00bf9a",
                "hoverBackgroundColor": "#00bf9a",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            }
        ]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var currentValue = dataset.data[tooltipItem.index];
                    return String((Number(currentValue) * 100).toFixed(2)) + "%";
                }
            }
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "fontColor": "#fff",
                        "callback": function (value, index, values) {
                            return String(Math.trunc(Number(value) * 100)) + "%";
                        }
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(225,78,202,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("allProductsByQuarter"), {
    "type": "bar",
    "data": {
        "labels": ["Q1", "Q2", "Q3", "Q4"],
        "datasets": [
            {
                "label": "Target",
                "data": APTargetQ,
                "fill": true,
                "backgroundColor": "#ffc15e",
                "hoverBackgroundColor": "#ffc15e",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            },
            {
                "label": "Actual",
                "data": APActualQ,
                "fill": true,
                "backgroundColor": "#00bf9a",
                "hoverBackgroundColor": "#00bf9a",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            }
        ]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var currentValue = dataset.data[tooltipItem.index];
                    return String((Number(currentValue) * 100).toFixed(2)) + "%";
                }
            }
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "fontColor": "#fff",
                        "callback": function (value, index, values) {
                            return String(Math.trunc(Number(value) * 100)) + "%";
                        }
                    }
                }
            ],

            "xAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("contractValue"), {
    "type": "bar",
    "data": {
        "labels": CVCommodity,
        "datasets": [
            {
                "label": "Completed",
                "data": CVSUMComplete,
                "fill": true,
                "backgroundColor": "#ffc15e",
                "hoverBackgroundColor": "#ffc15e",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            },
            {
                "label": "Uncompleted",
                "data": CVSUMUncomplete,
                "fill": true,
                "backgroundColor": "#00bf9a",
                "hoverBackgroundColor": "#00bf9a",
                "borderWidth": 2,
                "borderDash": [],
                "borderDashOffset": 0.0
            }
        ]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "intersect": 0,
            "position": "nearest",
            "mode": "index"
            //"callbacks": {
            //    "label": function (tooltipItem, data) {
            //        var dataset = data.datasets[tooltipItem.datasetIndex];
            //        var currentValue = dataset.data[tooltipItem.index];
            //        return formatNumberRp(String(currentValue));
            //    }
            //}
        },
        "responsive": true,

        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    "labelString": "Billions",
                    "ticks": {
                        "fontColor": "#fff",
                        "callback": function (value, index, values) {
                            //return "Rp." + String((Number(value) / 1000000000).toFixed(3));
                            return "Rp." + String((Number(value) / 10000).toFixed(3));
                        }
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("yearToDate"), {
    "type": "bar",
    "data": {
        "labels": ["YTD"],
        "datasets":
            [{
                "label": "Target",
                "data": [YTDTarget],
                "backgroundColor": "#ffc15e",
                "hoverBackgroundColor": "#ffc15e",
            },
            {
                "label": "Actual",
                "data": [YTDActual],
                "backgroundColor": "#00bf9a",
                "hoverBackgroundColor": "#00bf9a",
            }]
    },
    "options": {
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },
        "tooltips": {
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest"
        },
        "scales": {
            "yAxes": [
                {
                    "ticks": {
                        "suggestedMin": 0,
                        "padding": 20,
                        "fontColor": "#fff"
                    }
                }
            ],

            "xAxes": [
                {
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    }
});
