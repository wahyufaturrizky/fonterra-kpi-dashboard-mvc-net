//new Chart(document.getElementById("monthlyopenpo"), {
//    "type": "bar",
//    "data": {
//        "labels": ["January", "February", "March", "April"],
//        "datasets": [{
//            "label": "Bar Dataset",
//            "data": [10, 20, 30, 40],
//            "borderColor": "rgb(255, 99, 132)",
//            "backgroundColor": "rgba(255, 99, 132, 0.8)"
//        }, {
//            "label": "Line Dataset",
//            "data": [10, 20, 30, 40],
//            "type": "line",
//            "fill": false,
//            "borderColor": "rgb(54, 162, 235)"
//        }]
//    },
//    "options": {
//        "scales": {
//            "yAxes": [{
//                "ticks": {
//                    "beginAtZero": true
//                }
//            }]
//        }
//    }
//});

new Chart(document.getElementById("monthlyGRNI"), {
    "type": "bar",
    "data": {
        "labels": ["AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
        "datasets": [
            {
                "yAxisID": "b",
                "label": "Cases Count",
                "data": GNRICasesCount,
                "type": "line",
                "fill": true,
                "borderColor": "#419ef9"
            },
            {
                "yAxisID": "a",
                "label": "Closed",
                "data": GNRIClosed,
                "backgroundColor": "#e14eca",
                "hoverBackgroundColor": "#e14eca",
                "order": 2,
                "stack": 2
            },
            {
                "yAxisID": "a",
                "label": "Open",
                "data": GNRIOpen,
                "backgroundColor": "#fd77a4",
                "hoverBackgroundColor": "#fd77a4",
                "order": 2,
                "stack": 2
            }
        ]
    },
    "options": {
        //"maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom"
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    //var dataset = data.datasets[tooltipItem.datasetIndex];
                    //var currentValue = dataset.data[tooltipItem.index];
                    //if (dataset.label == "Cases Count") {
                    //    return currentValue;
                    //} else {
                    //    return formatNumberRp(String(currentValue));
                    //}
                }
            }
        },
        "responsive": true,
        "scales": {
            "yAxes":

                [
                {
                        "id": "a",
                        "stacked": true,
                    "scaleLabel": {
                        "display": true,
                        "labelString": "Amount"
                    },
                    "type": "linear",
                    "position": "left",
                  //  "stacked": true,
                  
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                       // "suggestedMin": 0,
                        "padding": 20,
                        "fontColor": "#9e9e9e",
                        
                        "callback": function (value, index, values) {
                            //return "Rp." + String((Number(value) / 1000000).toFixed(0));
                            return "Rp." + String((Number(value) / 1000000).toFixed(0));
                         //   return "Rp." + String((Number(value) / 100).toFixed(0));
                        }
                    }
                },
                {
                    "id": "b",
                    "scaleLabel": {
                        "display": true,
                        "labelString": "Count of POs"
                    },
                    "type": "linear",
                    "position": "right",
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 0,
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ],

            "xAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ]
        }
    }
});

new Chart(document.getElementById("monthlyPOAfter"), {
    "type": "bar",
    "data": {
        "labels": ["January", "February", "March", "April"],
        "datasets": [{
            "label": "Bar Dataset",
            "data": [10, 20, 30, 40],
            "borderColor": "rgb(255, 99, 132)",
            "backgroundColor": "rgba(255, 99, 132, 0.8)"
        }, {
            "label": "Line Dataset",
            "data": [10, 20, 30, 40],
            "type": "line",
            "fill": false,
            "borderColor": "rgb(54, 162, 235)"
        }]
    },
    "options": {
        "scales": {
            "yAxes": [{
                "ticks": {
                    "beginAtZero": true
                }
            }]
        }
    }
});

