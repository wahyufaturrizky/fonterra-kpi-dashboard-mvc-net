﻿var trcopyIncome;
var onActionInsertUpdateIncome = 0;
var inputs = ':checked,:selected,:text,textarea';
var classTableIncome = "tableIncome";
var IDTableIncome = "dataTablesMainIncome"
var updateButtonIncome = "ajaxUpdateIncome";
var editButtonIncome = "ajaxEditIncome";
var cancelButtonIncome = "ajaxCancelIncome";
var deleteButtonIncome = "ajaxDeleteIncome";
var deleteButtonIncomeTab = "ajaxDeleteTab";
var rejectButtonIncome = "ajaxRejectIncome";

var approveButtonIncome = "ajaxApproveIncome";
var dataSource = 0;
var saveButtonIncome = "ajaxSaveIncome";
var effect = "flash";
var IDTDEditIncome = 0;
var DecimalFormat = undefined;
var AccessList = undefined;
var onLoopIncome = 1;
var getLoopIncome = 1;
var maskNumberSetting = {
    thousands: ",",
    decimal: ".",
    integer: false,
};

var maskNumberSetting2 = {
    thousands: "",
    decimal: "",
    integer: true,
};

$(document).ready(function () {
    // get data source for particular dropdown list in current document
    //getDataSourceIncome();

    /**
     * Save button click event
     */

    $(document).on("click", "." + saveButtonIncome, function () {

        // Create new data object.
        var data = getFormDataIncome();

        if (data.valid) {
            var dataJson = JSON.stringify(data.record);
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Save" + Action);

                ajaxIncome(dataJson, "Save", url);
            }
            else {
                url = String.format("/{0}/{1}/Save", GroupMenu, NameMenu);
                ajaxIncome(dataJson, "Save", url);
            }
        }
        else {
            focusErrorTDIncome();
        }
    });

    /**
     * Update button click event
     */
    $(document).on("click", "." + updateButtonIncome, function () {

        // Create new data object.
        var data = getFormDataIncome();

        // Check if data to create or update are valid
        if (data.valid) {

            // Add key into the object
            data.record = addObjectKeyIncome(data.record, this);

            var dataJson = JSON.stringify(data.record)
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Update" + Action);
                ajaxIncome(dataJson, "UpdateAction", url);

            }
            else {
                url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
                ajaxIncome(dataJson, "UpdateCol", url);
            }
            //ajaxIncome(dataJson, "UpdateCol", url);
        }
        else {
            focusErrorTDIncome();
            $.notify({
                // options
                message: "Data is Not Valid"
            }, {
                // settings
                type: 'danger'
            });

        }
    });

    /**
     * Double click event for the table
     */

    if (!$("." + classTableIncome).hasClass('no-dblClick')) {
        $(document).on("dblclick", "." + classTableIncome + " td", function (e) {
            var id = $(this).closest('tr').attr("id");
            getDataSourceIncome("EditRow", id)
        });
    }
    /**
     * Edit button click event for the particular row in the table
     */
    $(document).on("click", "." + editButtonIncome, function () {
        //$('input:checkbox').not(this).prop('checked', false);

        var id = $(this).closest('tr').attr("id");
        getDataSourceIncome("EditRow", id)
    });

    /**
     * Cancel button click event
     */
    $(document).on("click", "." + cancelButtonIncome, function () {
        var id = $(this).closest('tr').attr("id");

        if (trcopyIncome) {
            $("." + classTableIncome + " tr[id='" + id + "']").html(trcopyIncome);
            trcopyIncome = '';
        } else {
            document.getElementById(IDTableIncome).deleteRow(1);
        }

        onActionInsertUpdateIncome = 0;
    });

    /** 
     * Delete button click event for the particular row in the table
     */

    $(document).on("click", "." + deleteButtonIncome, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKeyIncome({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Delete" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenu, NameMenu);
                    }
                    ajaxIncome(dataJson, "Delete", url);
                }
            }
        }
    });

    $(document).on("click", "." + deleteButtonIncomeTab, function () {
        var TabName = $(this).attr("id");

        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKeyIncomeTabIncome({}, this, TabName);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof ActionTab[TabName] !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "Delete" + ActionTab[TabName]);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenuTab[TabName], NameMenuTab[TabName]);
                    }
                    ajaxIncome(dataJson, "Delete", url);
                }
            }
        }
    });


    /** 
    * Reject button click event for the particular row in the table
    */
    $(document).on("click", "." + rejectButtonIncome, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Reject Data?")) {

                // Add key into the object
                var data = addObjectKeyIncome({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Reject" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Reject", GroupMenu, NameMenu);
                    }
                    ajaxIncome(dataJson, "Reject", url);
                }
            }
        }
    });


    /** 
   * Approve button click event for the particular row in the table
   */


    $(document).on("click", "." + approveButtonIncome, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Post / Approve Data?")) {

                // Add key into the object
                var data = addObjectKeyIncome({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Approve" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Approve", GroupMenu, NameMenu);
                    }
                    ajaxIncome(dataJson, "Approve", url);
                }
            }
        }
    });

    /**
     * Document click event
     */
    $(document).click(function (e) {
        if (($(e.target).closest('tr').attr("id") != IDTDEditIncome) && $(e.target).closest(".ActionBtn").attr("class") != "ActionBtn" &&
            $(e.target).closest(".unmodified").length == 0 &&
            $(e.target).closest(".select2-dropdown").length == 0 &&
            onActionInsertUpdateIncome == 1 && typeof MultiAdd == 'undefined') {
            CancelInsertOrUpdateIncome();
        }
        else {
            if ($(e.target).closest(".unmodifiedSort").length > 0) {
                onActionInsertUpdateIncome = 0;
            }
        }
    });

    /**
     * Checkbox click all event
     */
    $("#checkAll").click(function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".checkAll").each(function () {
        var that = $(this);
        var table = that.closest('table');
        that.click(function () {
            that.closest('table').find('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
});

function focusErrorTDIncome() {
    if ($("." + classTableIncome + " td").hasClass('has-error')) {
        var errorTD = $(document).find('td.has-error').find("input")[0];
        if (errorTD != undefined) {
            document.getElementById(errorTD.id).focus();
        }
    }
}

/**
 * Get form data from current row
 */
function getFormDataIncome() {
    var validation = [];
    var valid = true;
    var data = {};

    // Loop thru input elements available in the table
    $(document)
        .find('.' + classTableIncome + ' tbody tr[id="' + IDTDEditIncome + '"')
        .find(inputs)
        .filter(function () {

            var controlType = $(this).parent().prop('type');

            var key;
            var value;
            var mandatory;

            // Check control type to get value and mandatory
            if (controlType && controlType === 'select-one') {
                if ($(this).parent().prop('id') == "") {
                    key = $(this).parent().closest('td').prop('class')
                }
                else {
                    key = $(this).parent().prop('id');
                }
                value = $.trim(this.value);
                mandatory = $(this).parent().attr("Mandatory");

            } else {

                controlType = $(this).prop('type');

                key = $(this).attr('id');

                if (controlType === 'checkbox') {

                    value = $(this).prop('checked') ? true : false;
                    mandatory = $(this).attr("Mandatory");

                } else {

                    value = $.trim(this.value);
                    if (key.includes("Date")) {
                        var parts = this.value.split(/[/ :]/);

                        //value = new Date(parts[2], parts[1] - 1, parts[0]);
                        value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

                        if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
                            value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
                        }
                        //value = key.includes("Date") ? new Date(tes) : $.trim(this.value);
                    }

                    mandatory = $(this).attr("Mandatory");

                }
            }

            // Check value and mandatory
            if (value === "" && mandatory === "true") {

                // Append error class
                $(this).closest('td').addClass("has-error");

                validation.push(0);

            } else {

                // Push data
                if (value != "") {
                    data[key] = value;
                }
                // Append success class
                // This assignment help to filter selected option for DDL when switch back to table row
                $(this).addClass("success");

                validation.push(1);
            }
        });

    // Check validation
    for (var i = 0; i < validation.length; i++) {

        // break if 0 found
        if (validation[i] === 0) {
            valid = false;
            break;
        }
    }

    return {
        valid: valid,
        record: data
    }
}

/**
 * Add key property into object for update or delete
 * @param {object} data
 * @param {object} _this - Copy of current row
 */
function addObjectKeyIncome(data, _this) {

    data[TIIncome[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

function addObjectKeyIncomeTabIncome(data, _this, tabName) {
    var tempTIIncome = TIIncomeTab[tabName];

    data[tempTIIncome[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

/**
 * Make ajax request to create or update data.
 * @param {object} params
 * @param {string} action - Action name
 * @url {string}
 */
function ajaxIncome(params, action, url) {
    $.post(url, { dataJSON: params }, function (result) {

        switch (action) {
            case "Save":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });

                        removeClassAfterCreateUpdateIncome();

                    } else {
                        AddNewRowIncome(result);

                        removeClassAfterCreateUpdateIncome(true);
                        onActionInsertUpdateIncome = 0;
                        $.notify({
                            // options
                            message: 'Success insert Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateCol":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdateIncome();

                        } else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    } else {

                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + classTableIncome + " tr[id=" + IDTDEditIncome + "]").clone();

                        $("." + cancelButtonIncome).trigger("click");

                        for (var i = 1; i < TIIncome.length; i++) {
                            var dataResult = cellValueIncome(TIIncome[i], result[TIIncome[i].Name], trFormObject, i);

                            $("tr[id='" + result[TIIncome[0].Name] + "'] td[class='" + TIIncome[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdateIncome(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdateIncome = 0;
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateAction":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdateIncome();

                        } else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    } else {

                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + classTableIncome + " tr[id=" + IDTDEditIncome + "]").clone();

                        $("." + cancelButtonIncome).trigger("click");

                        for (var i = 1; i < TIIncome.length; i++) {
                            var dataResult = cellValueIncome(TIIncome[i], result[TIIncome[i].Name], trFormObject, i);

                            $("tr[id='" + result[TIIncome[0].Name] + "'] td[class='" + TIIncome[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdateIncome(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdateIncome = 0;

                        //location.reload();
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "Delete":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $("." + classTableIncome + " tr[id='" + result + "']").effect("highlight", { color: '#f4667b' }, 500, function () {
                            $("." + classTableIncome + " tr[id='" + result + "']").remove();
                        });

                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "DeleteAll":
                if (result != null) {
                    if (result.Status == false) {
                        location.reload();
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableIncome + " tr[id='" + result[i] + "']").remove();
                        }
                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "Reject":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableIncome + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Reject Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
                setTimeout(function () { window.location.href = urlredirect; }, 2000);
                break;
            case "Approve":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableIncome + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Approved Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "RegenerateAll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableIncome + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Regenerate Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
        }
    });

}

/**
 * Remova css class after insert or update event
 * @param {bool} removeValue - Flag to indicate wethear need to clear the value or not.
 */
function removeClassAfterCreateUpdateIncome(removeValue) {
    $(document).find("." + classTableIncome).find(inputs).filter(function () {

        if (removeValue) {
            this.value = "";
        }

        $(this).removeClass("success");
        $(this).closest('td').removeClass("has-error")
    });
}
/**
 * render row to insert new data
 */
function renderRowIncome() {
    $(".datepicker").datepicker("destroy");
    // flag to indicate weather need to bind data or not
    var flagBindData = 0;

    // get table object
    var tbl = document.getElementById(IDTableIncome);

    // insert row into table object
    var row = tbl.insertRow(1);

    // count total of rows
    var rowCount = $("#" + IDTableIncome + " tbody tr").length;

    // set row id equal to total row + 1
    row.id = (rowCount + 1) + 'guid';

    // set global current table row edit
    IDTDEditIncome = row.id;

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" disabled="disabled">';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TIIncome.length; i++) {
        var obj = TIIncome[i];
        var cell = row.insertCell(i)

        var control;
        switch (obj.Type) {
            case 'Input':
                control = createInputTextIncome(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null);
                break;
            case 'InputNumber':
                control = createInputNumberIncome(obj);
                break;
            case 'InputNumeric':
                control = createInputNumericIncome(obj);
                break;
            case 'DDL':
                control = createDDLIncome(i);
                break;

            case 'InputEnum':
                control = createEnumDDLIncome(i);
                break;

            case 'InputCheckbox':
                control = createInputCheckIncome(obj);
                break;

            case 'AutocompleteDDL':
                control = createSelectIncome(obj);
                break;

            case 'InputDate':
                control = createInputDateIncome(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null);
                break;

            case 'InputLookup':
                control = CreateInputlookupIncome(obj);
                break;

            case 'InputWithValidate':
                control = createInputTextWithValidateIncome(obj);
                break;

            default:
                // reset control
                control = '';
                break;
        }

        // Check if control object exist.
        // If control exist, create the object inside cell.
        if (control) {
            cell.innerHTML = control;
            cell.className = obj.Name;
        }

        // After creating control, check if it need to bind to data source.
        // This must be executed after control is exist in the cell.
        if (obj.Type === 'AutocompleteDDL') {
            bindAutocompleteIncomeEvent(obj);

        }
    }

    var lastCell = row.insertCell(i);
    lastCell.innerHTML = '<a href="javascript:;" title="Save" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxSaveIncome"></i></a> <a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat ajaxCancelIncome"></i></a>';
    lastCell.className = "text-center unmodified";

    //$(".datepicker").datepicker();
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    }).inputmask('date');

    $('[name=integer-default]').maskNumber();
    // set ActionInsertUpdate flag
    onActionInsertUpdateIncome = 1;
}
/**
 * Create new row to insert new data
 */
function AddRowIncome(DeleteBeforeAdd) {
    if (DeleteBeforeAdd != null) {
        var NewonActionInsertUpdate = 'onActionInsertUpdate' + DeleteBeforeAdd;
        if ((onActionInsertUpdateIncome == 0 && window[NewonActionInsertUpdate] == 0) || typeof MultiAdd !== 'undefined') {
            //For 2 table
            getDataSourceIncome("AddRow");
        }
    }
    else {
        if (onActionInsertUpdateIncome == 0 || typeof MultiAdd !== 'undefined') {
            //For 2 table
            getDataSourceIncome("AddRow");
        }
    }
}

/**
 * Edit row
 * @param {string} id
 */
function editRowIncome(id) {
    if (id && onActionInsertUpdateIncome == 0) {
        $(".datepicker").datepicker("destroy");
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
        // hide editing row, for the time being
        //$("." + table + " tr:last-child").fadeOut("fast");

        // Set global id for current edit table row
        IDTDEditIncome = id;

        // create html text to insert into row
        var html = '<td class="unmodified"><input type="checkbox" ></td>';

        // get current row record to update
        var row = document.getElementById(id);
        var selectedDate = new Date();
        for (var i = 1; i < TIIncome.length; i++) {
            var obj = TIIncome[i];
            var val = ""
            if (row.getElementsByClassName(obj.Name)[0] == undefined) {
                val = "Please check this row's class !!!";
                $.notify({
                    // options
                    message: "Class " + obj.Name + "didn't found !!!"
                }, {
                    // settings
                    type: 'danger'
                });
            }
            else {
                val = row.getElementsByClassName(obj.Name)[0].innerText;
            }


            var stringContent;

            switch (obj.Type) {
                case 'Input':
                    stringContent = createInputTextIncome(obj, val);
                    break;
                case 'InputNumber':
                    stringContent = createInputNumberIncome(obj, val);
                    break;
                case 'InputNumeric':
                    stringContent = createInputNumericIncome(obj, val);
                    break;

                case 'DDL':
                    if (row.getElementsByClassName(obj.Name)[0].firstElementChild != null) {
                        stringContent = createSelectIncome(obj);
                        stringContent += createInputHiddenIncome(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    } else {
                        stringContent = createDDLIncome(i, val);
                    }
                    break;

                case 'InputEnum':
                    stringContent = createEnumDDLIncome(i, val);
                    break;

                case 'InputCheckbox':
                    val = row.getElementsByClassName(obj.Name)[0].innerHTML.includes("checked") ? true : false;
                    stringContent = createInputCheckIncome(obj, val);
                    break;

                case 'AutocompleteDDL':
                    stringContent = createSelectIncome(obj);
                    stringContent += createInputHiddenIncome(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    break;

                case 'InputDate':
                    stringContent = createInputDateIncome(obj, val);
                    selectedDate = val;
                    break;

                default:
                    stringContent = val;
                    break;
            }

            html += '<td class="' + obj.Name + '">' + stringContent + '</td>';
        }

        html += '<td class="text-center unmodified">'
        if (AccessList != "" || AccessList.IsUpdate == "true") {
            html += '<a href="javascript:;" title="Update" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxUpdateIncome"></i></a>'
        }
        html += '<a href="javascript:;" title="Return" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat ajaxCancelIncome"></i></a></td>'

        // Before replacing the TR contents, make a copy so when user clicks on .
        trcopyIncome = $("." + classTableIncome + " tr[id=" + id + "]").html();

        // Make jQuery object of current table row before replaceing the TR content.
        // This object will be used to get value from select option
        var trObject = $("." + classTableIncome + " tr[id=" + id + "]").clone();

        // Create html controls.
        // Update edit row with controls created.
        $("." + classTableIncome + " tr[id=" + id + "]").html(html);

        // Bind data source for dropdown list or autocomplete.
        // This must be executed after html controls are created.
        for (var i = 1; i < TIIncome.length; i++) {
            var obj = TIIncome[i];

            if (obj.Type === 'DDL') {

                var hidden = $(trObject).find('.' + obj.Name).find(':hidden');
                var selectedValue = hidden ? $(hidden).val() : $.trim($(trObject).find('.' + obj.Name).text());

                bindSelectDataIncome(obj, selectedValue);

            } else if (obj.Type === 'AutocompleteDDL') {
                var selectedValue = {
                    id: $(trObject).find('.' + obj.Name).find(':hidden').val(),
                    text: $.trim($(trObject).find('.' + obj.Name).text())
                };

                bindAutocompleteIncomeEvent(obj, selectedValue);

            }
        }
        $('[name=integer-default]').maskNumber();
        //$(".datepicker").datepicker();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        }).inputmask('date').datepicker('setDate', ConvertStringToDatetime(selectedDate));

        // set ActionInsertUpdate flag
        onActionInsertUpdateIncome = 1;
    }
}

/**
 * Bind autocomplete event
 * @param {object} obj
 * @param {object} selectedObject - Expected { id: idValue, text: textValue }
 */
function bindAutocompleteIncomeEvent(obj, selectedObject) {
    var idSelector = '#' + obj.Name;
    var url = obj.Url;

    $(idSelector).select2({
        minimumInputLength: 1,
        language: {
            noResults: function () {
                return "Tidak ada hasil";
            },
            inputTooShort: function (args) {
                return "Minimal 1 huruf";
            },
        },
        ajax: {
            url: url,
            dataType: 'json',
            data: function (term, page) {
                return {
                    query: term.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    // Set default selected if exist.
    if (selectedObject) {
        $(idSelector).select2("trigger", "select", { data: selectedObject });
    }
}

/**
 * Bind data to select
 * @param {object} obj
 * @param {string} selectedValue
 */
function bindSelectDataIncome(obj, selectedValue) {
    var idSelector = '#' + obj.Name;

    $(idSelector).select2({
        minimumResultsForSearch: -1,
        data: obj.ListData
    });

    // Set default selected if exist.
    if (selectedValue) {
        var selected = {};

        // Get object for selected value.
        for (var i = 0; i < obj.ListData.length; i++) {
            if (String(obj.ListData[i].id) === selectedValue || obj.ListData.text === selectedValue) {
                selected.id = obj.ListData[i].id,
                selected.text = obj.ListData[i].text

                break;
            }
        }

        $(idSelector).select2("trigger", "select", { data: selected });
    }
}

/**
 * Add new row after insert
 * @param {object} result - Json object returned after inserting new data.
 */
function AddNewRowIncome(result) {

    // Create copy of table row.
    // To keep form before the first row is deleted.
    // This purpose is to get value from selected option.
    var trFormObject = $("." + classTableIncome + " tr[id=" + IDTDEditIncome + "]").clone();

    // Delete the first row
    document.getElementById(IDTableIncome).deleteRow(1);

    // Get table object
    var tbl = document.getElementById(IDTableIncome);

    // Insert new row
    var row = tbl.insertRow(1);

    // Set row id equal to total row + 1
    row.id = result[TIIncome[0].Name];

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" >';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TIIncome.length; i++) {

        var obj = TIIncome[i];
        var cell = row.insertCell(i)
        cell.innerHTML = cellValueIncome(obj, result[obj.Name], trFormObject, i);
        cell.className = obj.Name;
    }

    var lastCell = row.insertCell(i);

    if (AccessList != "" || AccessList.IsUpdate == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Edit" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-edit ajaxEditIncome"></i></a>';
    }
    if (AccessList != "" || AccessList.IsDelete == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-trash ajaxDeleteIncome"></i></a>';
    }
    lastCell.className = "text-center unmodified";

    // set ActionInsertUpdate flag
    onActionInsertUpdateIncome = 0;
}

/**
 * Add cell value
 * @param {object} obj - Json object for the model.
 * @param {object} value - The value for each property defined in obj.
 * @param {object} trFormObject - Object copy contain table row form before inserting to database
 */
function cellValueIncome(obj, value, trFormObject, loop) {
    switch (obj.Type) {
        case 'AutocompleteDDL':
        case 'DDL':
            // Create selector '#IDControl option:selected'
            var selector = '#' + obj.Name + ' .success';

            // Get object from table row object
            var selectObject = $(trFormObject).find(selector);

            // Set label
            var label = selectObject.length ? $(selectObject).text() : value;

            if (label == value) {
                var selectData = TIIncome[loop].ListData.find(function (v) {
                    return v.id == value;
                });
                label = selectData.text;
            }

            // Create hidden control
            var control = createInputHiddenIncome(obj, value);

            return label + control;

        case 'Date':
            return value ? JSONDateWithTime(value) : '';

        case 'Bool':
            if (value != undefined && obj.ListData != undefined) {
                var text = $.grep(
                    obj.ListData,
                    function (n, i) {
                        return value === true ? n.toLowerCase() == 'active' : n.toLowerCase() === 'inactive';
                    });

                return text.length ? text[0] : value;
            }
            else {
                return null;
            }

        case 'InputEnum':
            if (obj.ListData.length) {
                var selectEnumData = TIIncome[loop].ListData.find(function (text, v) {
                    return v == value;
                });
                return selectEnumData;
            }

            return '';

        case 'InputCheckbox':
            return createInputCheckIncome(obj, value, true);

        case 'InputDate':
            return value ? JSONDateWithTime(value) : '';

        case 'InputNumber':
            value = addDecimalSeparatorIncome(value, maskNumberSetting);
            value = addThousandSeparatorIncome(value, maskNumberSetting);
            return value;

        case 'InputNumeric':
            value = addThousandSeparator2Income(value, maskNumberSetting2);
            return value;


        default:
            return value ? value : '';
    }
}

/**
 * Cancel insert or update.
 */
function CancelInsertOrUpdateIncome() {

    if (confirm("Do you really want to quit insert or update data ?")) {
        if (trcopyIncome) {

            $("." + classTableIncome + " tr[id='" + IDTDEditIncome + "']").html(trcopyIncome);
            //$("." + table + " tr:last-child").fadeIn("fast");

            // clear global variable
            trcopyIncome = '';
            IDTDEditIncome = '';

        } else {
            document.getElementById(IDTableIncome).deleteRow(1);
        }
        onActionInsertUpdateIncome = 0;
    }

}

function ApproveAllIncome() {
    if (confirm("Do you really want to Post / Approve record ?")) {
        var $inputs =
               $(document).find("." + classTableIncome).find(":checked").filter(function () {
                   if (this.id == "") {
                       return $.trim(this.value);
                   }
               });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TIIncome[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TIIncome[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TIIncome[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "ApproveAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/ApproveAll", GroupMenu, NameMenu);
            }
            ajaxIncome(dataJson, "Approve", url);
        } else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
        //var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
        //window.location.href = urlredirect;
    }
}


function RejectAllIncome() {
    var $inputs =
    $(document).find("." + classTableIncome).find(":checked").filter(function () {
        if (this.id == "") {
            return $.trim(this.value);
        }
    });
    var array = $inputs.map(function () {
        return this.value;
    }).get();
    var idName = TIIncome[0].Name;
    var data = [];
    $.each($inputs, function () {
        var obj = new Object();
        obj[TIIncome[0].Name] = $(this).closest('tr').attr("id");
        if (obj[TIIncome[0].Name] != undefined) {
            data.push(obj);
        }
    });
    var dataJson = JSON.stringify(data)
    if (typeof Action !== 'undefined') {
        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "RejectAll" + Action);
    } else {
        var url = String.format("/{0}/{1}/RejectAll", GroupMenu, NameMenu);
    }
    ajaxIncome(dataJson, "Reject", url);
    var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
    window.location.href = urlredirect;
}


function DeleteAllIncome() {
    if (onActionInsertUpdateIncome == 1) {
        //can't delete while on action Insert / Update
        return false;
    }
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("." + classTableIncome).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TIIncome[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TIIncome[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TIIncome[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "DeleteAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenu, NameMenu);
            }
            ajaxIncome(dataJson, "DeleteAll", url);
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }

    }
}

function TabDeleteAllIncome(TabName) {
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("#tab" + TabName + "." + classTableIncome).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        //var idName = TIIncome[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            var tempTIIncome = TIIncomeTab[TabName];

            //data[tempTIIncome[0].Name] = $(_this).closest('tr').attr("id");

            obj[tempTIIncome[0].Name] = $(this).closest('tr').attr("id");
            if (obj[tempTIIncome[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof ActionTab !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "DeleteAll" + ActionTab[TabName]);
            } else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenuTab[TabName], NameMenuTab[TabName]);
            }
            ajaxIncome(dataJson, "DeleteAll", url);
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
    }
}

/**
 * Create DDL.
 * @param {object} DDLN
 * @param {string} ddlVal
 */

function createDDLIncome(DDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    //AutoSelect For IsActive
    if (ddlVal == null && TIIncome[DDLN].Name == "IsActive") {
        ddlVal = "Active";
    }

    onChangeFunction = typeof TIIncome[DDLN].onchange !== 'undefined' ? TIIncome[DDLN].onchange : "";

    input = '<select class="form-control" Mandatory="' + TIIncome[DDLN].Mandatory + '" name=' + TIIncome[DDLN].Name + ' id=' + TIIncome[DDLN].Name + ' onchange=' + onChangeFunction + '>';
    //input += '<option value="">Select</option>'
    if (TIIncome[DDLN].ListData.length != 0) {
        input += '<option value="">Choose Any</option>';
        for (ddlLoop = 0; ddlLoop < TIIncome[DDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TIIncome[DDLN].ListData[ddlLoop].text)
                selected = "selected";
            input += '<option value="' + TIIncome[DDLN].ListData[ddlLoop].id + '" ' + selected + '>' + TIIncome[DDLN].ListData[ddlLoop].text + '</option>';
        }
    } else {
        input += '<option value="">No Data Available</option>';
    }
    return input;

}

/**
 * Create Input hidden.
 * @param {object} obj
 * @param {string} value
 */
function createInputHiddenIncome(obj, value) {
    value = value ? $.trim(value) : '';

    return '<input type="hidden" id="hidden-' + obj.Name + '" value="' + value + '">';
}

/**
 * Create Input text.
 * @param {object} obj
 */
function createInputTextIncome(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" >' + value + '</textarea>';
    }
    return inputType;
}

function createInputTextWithValidateIncome(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control validateAmount ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" >' + value + '</textarea>';
    }
    return inputType;
}

function createInputNumberIncome(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control maskNumber ' + obj.Name + '" maxlength="' + obj.Length + '" name="currency-default" id="' + obj.Name + '" value="' + value + '">';
    return inputType;
}

function createInputNumericIncome(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="number" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    //var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '" onkeypress="return ' + (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57 + '">';
    return inputType;
}

function isNumberKeyIncome(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        alert('Salah');
    }
    else {
        alert('Benar');
    }

}

function CreateInputlookupIncome(obj, value) {
    return '<div class="form-group " style="margin: 0px !important;"><div class="input-group"><div class="bc-wrapper">' +
           '<input class="form-control autocomplete" data-val="true" data-val-number="The field IDApplicant must be a number." data-val-required="The IDApplicant field is required." id="' + obj.Name + '" name="' + obj.Name + '" readonly="readonly" type="hidden" value="0" autocomplete="off">' +
           '<div class="bc-menu list-group"></div></div><div class="bc-wrapper"><input class="form-control autocomplete" data-val="true" data-val-required="The NIK field is required" id="' + obj.ID + '" name="' + obj.ID + '" readonly="readonly" type="text" value="" autocomplete="off">' +
           '<div class="bc-menu list-group" style="display: none;"></div></div><span class="input-group-btn"><a href="' + obj.Url + '" id="idApplicationLU" onclick="showModal(this, 2);return false;" title="OrganizationLookup" class="btn btn-default">' +
           '<i class="glyphicon glyphicon-search"></i></a></span><span class="field-validation-valid text-danger" data-valmsg-for="ApplicantNIK" data-valmsg-replace="true"></span></div></div>';
}
function createInputDateIncome(obj, value) {
    value = value ? value.trim() : '';

    return '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control datepicker ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';;
}

/**
 * Create Input type checkbox.
 */
function createInputCheckIncome(obj, value, disabled) {
    var checked = value ? 'checked ="on"' : '';
    var options = disabled ? checked + ' disabled="disabled"' : checked;

    return '<input type="checkbox" Mandatory="' + obj.Mandatory + '" class=" ' + obj.Name + '" ' + options + ' id="' + obj.Name + '">';
}

/**
 * Create Select.
 * @param {object} obj
 * @param {object} options
 */
function createSelectIncome(obj, options) {
    var selectOptions = '<option value="">Select</option>';

    return '<select class="form-control ' + obj.Name + '" mandatory="' + obj.Mandatory + '" id="' + obj.Name + '">' + selectOptions + '</select>';
}

/**
 * TODO Check code below
 */
function createEnumDDLIncome(EDDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    input = '<select Mandatory="' + TIIncome[EDDLN].Mandatory + '" name=' + TIIncome[EDDLN].Name + '>';
    if (TIIncome[EDDLN].ListData.length != 0) {
        for (ddlLoop = 0; ddlLoop < TIIncome[EDDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TIIncome[EDDLN].ListData[ddlLoop])
                selected = "selected";
            input += '<option value="' + ddlLoop + '" ' + selected + '>' + TIIncome[EDDLN].ListData[ddlLoop] + '</option>';
        }
    }
    return input;
}

/**
 * Function to get data source for drop down list.
 * The data source will be used for drop down list in current document.
 */
function getDataSourceIncome(ActionRow, RowID) {
    onLoopIncome = 1;
    getLoopIncome = 1;
    getDecimalDataSourceIncome(ActionRow, RowID);
    getAccessDataSourceIncome(ActionRow, RowID);
    for (var i = 1 ; i < TIIncome.length; i++) {
        switch (TIIncome[i].Type) {
            case 'DDL':
                getDropdownDataSourceIncome(TIIncome[i], TIIncome[i].Url, ActionRow, RowID);
                break;
            case 'DDLWithOnChanged':
                getDropdownDataSourceIncome(TIIncome[i], TIIncome[i].Url, ActionRow, RowID);
                break;
            case 'InputEnum':
                getDropdownDataSourceIncome(TIIncome[i], TIIncome[i].Url, ActionRow, RowID);
                break;
        }
    }
    if (getLoopIncome > 1) {
        waitingDialog.show('Loading...');
    }
    else {
        if (ActionRow == "AddRow") {
            renderRowIncome();
        }
        else {
            editRowIncome(RowID)
        }
    }
}

function getDecimalDataSourceIncome(ActionRow, RowID) {
    if (DecimalFormat == undefined) {
        getLoopIncome++;
        $.getJSON("/SetupEnumSetup/Enum/getDecimalFormatString", function (result) {
            DecimalFormat = result;
            onLoopIncome++;
            closeLoadingBarIncome(onLoopIncome, ActionRow, RowID)
        });
    }
}

function getAccessDataSourceIncome(ActionRow, RowID) {
    if (AccessList == undefined) {
        getLoopIncome++;
        $.getJSON("/GlobalControlAccessor/GlobalAccess/GetAccessList", function (result) {
            AccessList = result;
            onLoopIncome++;
            closeLoadingBarIncome(onLoopIncome, ActionRow, RowID)
        });
    }
}

function closeLoadingBarIncome(onLoopIncome, ActionRow, RowID) {
    if (onLoopIncome == getLoopIncome) {
        waitingDialog.hide();
        if (ActionRow == "AddRow") {
            renderRowIncome();
        }
        else {
            editRowIncome(RowID)
        }
    }
}

/**
 * Get data source with ajax.
 * Parameter given are object to set and url.
 */
function getDropdownDataSourceIncome(obj, url, ActionRow, RowID) {

    // only make request if the object is not available
    if (!obj.ListData) {
        getLoopIncome++;
        url = !url && obj.Url ? obj.Url : url;
        $.getJSON(url, function (data) {
            obj.ListData = data;
            onLoopIncome++;
            closeLoadingBarIncome(onLoopIncome, ActionRow, RowID)
        }).fail(function () {
            $.post("/GlobalControlAccessor/GlobalAccess/jsonErrorLog", { dataJSON: url }, function (result) {
                onLoopIncome++;
                closeLoadingBarIncome(onLoopIncome, ActionRow, RowID)
                var splitUrl = url.split("/");
                $.notify({
                    // options
                    message: "Fail to get " + splitUrl[3]
                }, {
                    // settings
                    type: 'danger'
                });
            });
        });
    }
}

function addDecimalSeparatorIncome(value, settings) {
    if (DecimalFormat > 0) {
        var DecimalStringFormat = String.format("(\\d\{{0}\})$", DecimalFormat);
        var DecimalRegExp = new RegExp(DecimalStringFormat, '');
        value = value.toString().replace(DecimalRegExp, settings.decimal.concat("$1"));

        //value = value.replace(/(\d{2})$/, settings.decimal.concat("$1"));
        value = value.toString().replace(/(\d+)(\d{3}, \d{2})$/g, "$1".concat(settings.thousands).concat("$2"));
    }
    return value;
}

function addThousandSeparatorIncome(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}

function addThousandSeparator2Income(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}

function ConvertStringToDatetime(value) {
    var parts = value.split(/[/ :]/);

    //value = new Date(parts[2], parts[1] - 1, parts[0]);
    value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

    if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
        value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
    }
    return value;
}
