﻿var trcopy;
var onActionInsertUpdate = 0;
var editingtrid = 0;
var editingtdcol = 0;
var inputs = ':checked,:selected,:text,textarea';
var table = "tableDemo";
var updatebutton = "ajaxUpdate";
var editbutton = "ajaxEdit";
var cancelbutton = "cancel";
var deletebutton = "ajaxDelete";
var savebutton = "ajaxSave";
var effect = "flash";
var ActiveEnum = [];
var IDTDEdit = 0;
$(document).ready(function () {
    $(document).on("click", "." + cancelbutton, function () {
        var id = $(this).closest('tr').attr("id");
        if (trcopy != null) {
            $("." + table + " tr[id='" + id + "']").html(trcopy);
            //$("." + table + " tr:last-child").fadeIn("fast");
            trcopy = null;
        } else {
            document.getElementById("dataTablesMain").deleteRow(1);
        }
        onActionInsertUpdate = 0;
    });

    $(document).on("dblclick", "." + table + " td", function (e) {
        //check if any other TD is in editing mode ? If so then dont show editing box

        //var isEditingform = $(this).closest("tr").attr("class");
        //var isEditingformTD = $(this).attr("class").includes("unmodified");
        //if (tdediting == 0 && editing == 0 && isEditingform != "inputform" && isEditingformTD == false) {
        //    editingtrid = $(this).closest('tr').attr("id");
        //    editingtdcol = $(this).attr("class");
        //    var text = $(this).html();
        //    var tr = $(this).parent();
        //    var tbody = tr.parent();
        //    for (var i = 0; i < tr.children().length; i++) {
        //        if (tr.children().get(i) == this) {
        //            var column = i;
        //            break;
        //        }
        //    }

        //    // decrement column value by one to avoid sr no column
        //    column--;
        //    //alert(column+"==="+placeholder[column]);
        //    if (column <= TI.length) {
        //        var text = $(this).html();
        //        input = createInput(column, text);
        //        $(this).html(input);
        //        $(this).find(inputs).focus();
        //        tdediting = 1;
        //    }
        //}
        var id = $(this).closest('tr').attr("id");
        TableEditColumn(id);
    });

    $(document).on("click", "." + deletebutton, function () {
        $('input:checkbox').not(this).prop('checked', false);
        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {
                var data = new Object();
                data[TI[0].Name] = $(this).closest('tr').attr("id");
                var dataJson = JSON.stringify(data)
                var url = String.format("/{0}/{1}/Delete", GroupMenu, NameMenu);
                ajax(dataJson, "Delete", url);
                //ajax("rid=" + id, "del");
            }
        }
    });

    $(document).on("click", "." + editbutton, function () {
        //$('input:checkbox').not(this).prop('checked', false);
        //var id = $(this).closest('tr').attr("id");
        //if (id && editing == 0 && tdediting == 0) {
        //    // hide editing row, for the time being
        //    //$("." + table + " tr:last-child").fadeOut("fast");

        //    var html;
        //    html += '<td class="unmodified"><input type="checkbox" ></td>';
        //    for (i = 1; i < TI.length; i++) {
        //        // fetch value inside the TD and place as VALUE in input field
        //        var val = $(this).closest("tr").find('.' + TI[i].Name + '').text();
        //        if (TI[i].Type == "Input") {

        //            input = createInput(i, val);
        //            html += '<td class="' + TI[i].Name + '">' + input + '</td>';
        //        }
        //        else if (TI[i].Type == "DDL") {
        //            ddl = createDDL(i, val);
        //            html += '<td class="' + TI[i].Name + '">' + ddl + '</td>';
        //        }
        //        else {
        //            html += '<td class="' + TI[i].Name + '">' + val + '</td>';
        //        }
        //    }
        //    html += '<td class="text-center unmodified"><a href="javascript:;" title="Update" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-save ajaxUpdate"></i></a> <a href="javascript:;" title="Return" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat cancel"></i></a></td>';

        //    // Before replacing the TR contents, make a copy so when user clicks on 
        //    trcopy = $("." + table + " tr[id=" + id + "]").html();
        //    $("." + table + " tr[id=" + id + "]").html(html);

        //    // set editing flag
        //    editing = 1;
        //}
        $('input:checkbox').not(this).prop('checked', false);
        var id = $(this).closest('tr').attr("id");
        TableEditColumn(id);
    });

    $(document).on("blur", "." + table, function (e) {
        //if(tdediting == 1)
        //{
        //    var newval = $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").find(inputs).val();
        //    $("." + table + " tr[id='" + editingtrid + "'] td[class='" + editingtdcol + "']").html(newval);
        //    tdediting = 0;
        //}

        //if (editing == 1) {
        //    var id = $(this).closest('tr').attr("id");
        //    if (trcopy != null) {
        //        $("." + table + " tr[id='" + id + "']").html(trcopy);
        //        //$("." + table + " tr:last-child").fadeIn("fast");
        //        trcopy = null;
        //    } else {
        //        document.getElementById("dataTablesMain").deleteRow(1);
        //    }
        //    editing = 0;
        //    creating = 0;
        //}
    });

    $(document).click(function (e) {
        if (($(e.target).closest('tr').attr("id") != IDTDEdit) &&
            $(e.target).closest(".ActionBtn").attr("class") != "ActionBtn" &&
            $(e.target).closest(".unmodified").attr("class") != "text-center unmodified" && onActionInsertUpdate == 1) {
            CancelInsertOrUpdate();
        }
    });

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(document).on("click", "." + savebutton, function () {
        var validation = 1;

        var $inputs =
		$(document).find("." + table).find(inputs).filter(function () {
		    // check if input element is blank ??
		    if ($.trim(this.value) == "" && $(this).attr("Mandatory") == "true") {
		        $(this).closest('td').addClass("has-error");
		        validation = 0;
		    } else {
		        $(this).addClass("success");
		    }
		    return $.trim(this.value);
		});

        var array = $inputs.map(function () {
            return this.value;
        }).get();

        //var serialized = $inputs.serialize();
        if (validation == 1) {

            var data = new Object();
            for (i = 0; i < $inputs.length; i++) {
                //createObject(data, TI[i + 1].Name, $inputs[i].value);
                if (TI[i + 1].Type.includes("InputCheckbox")) {
                    var convertBool = ($inputs[i].value == "on") ? "true" : "false";
                    data[TI[i + 1].Name] = convertBool;
                }
                else {
                    data[TI[i + 1].Name] = $inputs[i].value;
                }
            }
            var dataJson = JSON.stringify(data)
            var url = String.format("/{0}/{1}/Save", GroupMenu, NameMenu);
            ajax(dataJson, "Save", url);
        }
    });

    $(document).on("click", "." + updatebutton, function () {
        var validation = 1;
        id = $(this).closest('tr').attr("id");
        var $inputs =
		$(document).find("." + table).find(inputs).filter(function () {
		    if ($.trim(this.value) == "" && $(this).attr("Mandatory") == "true") {
		        $(this).closest('td').addClass("has-error");
		        validation = 0;
		    } else {
		        $(this).addClass("success");
		    }
		    return $.trim(this.value);
		});

        var array = $inputs.map(function () {
            return this.value;
        }).get();
        if (validation == 1) {

            var data = new Object();
            if (id != null) {
                data[TI[0].Name] = id;
            }
            for (i = 0; i < $inputs.length; i++) {
                //createObject(data, TI[i + 1].Name, $inputs[i].value);
                if (TI[i + 1].Type.includes("InputCheckbox")) {
                    var convertBool = ($inputs[i].value == "on") ? "true" : "false";
                    data[TI[i + 1].Name] = convertBool;
                }
                else {
                    data[TI[i + 1].Name] = $inputs[i].value;
                }
            }
            var dataJson = JSON.stringify(data)
            var url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
            ajax(dataJson, "UpdateCol", url);
            // clear editing flag
        }
    });
    getData();
});

function TableEditColumn(id) {
    if (id && onActionInsertUpdate == 0) {
        $('input:checkbox').not(this).prop('checked', false);
        // hide editing row, for the time being
        //$("." + table + " tr:last-child").fadeOut("fast");
        IDTDEdit = id;
        var html;
        html += '<td class="unmodified"><input type="checkbox" ></td>';
        for (i = 1; i < TI.length; i++) {
            // fetch value inside the TD and place as VALUE in input field
            var tbl = document.getElementById(id);
            var val = tbl.getElementsByClassName(TI[i].Name)[0].innerText;
            //var val = $(this).closest("tr").find('.' + TI[i].Name + '').text();
            if (TI[i].Type == "Input") {

                input = createInput(i, val);
                html += '<td class="' + TI[i].Name + '">' + input + '</td>';
            }
            else if (TI[i].Type == "DDL") {
                ddl = createDDL(i, val);
                html += '<td class="' + TI[i].Name + '">' + ddl + '</td>';
            }
            else if (TI[i].Type == "InputBool") {
                ddl = createEnumDDL(i, val);
                html += '<td class="' + TI[i].Name + '">' + ddl + '</td>';
            }
            else if (TI[i].Type == "InputCheckbox") {
                if (tbl.getElementsByClassName(TI[i].Name)[0].innerHTML.includes("checked")) {
                    val = true;
                }
                ddl = createCheckbox(i, val);
                html += '<td class="' + TI[i].Name + '">' + ddl + '</td>';
            }
            else {
                html += '<td class="' + TI[i].Name + '">' + val + '</td>';
            }
        }
        html += '<td class="text-center unmodified"><a href="javascript:;" title="Update" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-save ajaxUpdate"></i></a> <a href="javascript:;" title="Return" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat cancel"></i></a></td>';

        // Before replacing the TR contents, make a copy so when user clicks on 
        trcopy = $("." + table + " tr[id=" + id + "]").html();
        $("." + table + " tr[id=" + id + "]").html(html);

        // set ActionInsertUpdate flag\
        onActionInsertUpdate = 1;
    }
}

function CancelInsertOrUpdate() {
    var id = IDTDEdit;
    if (confirm("Do you really want to quit insert or update data ?")) {
        if (trcopy != null) {
            $("." + table + " tr[id='" + id + "']").html(trcopy);
            //$("." + table + " tr:last-child").fadeIn("fast");
            trcopy = null;
        } else {
            document.getElementById("dataTablesMain").deleteRow(1);
        }
        onActionInsertUpdate = 0;
    }
    // set ActionInsertUpdate flag\

}

function AddColumn() {
    if (onActionInsertUpdate == 0) {
        var tbl = document.getElementById('dataTablesMain');
        var row = tbl.insertRow(1);
        var id = $('#dataTablesMain tr:last').attr('id');
        row.id = parseInt(id) + 1;
        IDTDEdit = row.id;
        i = 1;
        var firstCell = row.insertCell(0);
        firstCell.innerHTML = '<input type="checkbox" disabled="disabled">';
        firstCell.className = 'unmodified';
        for (i ; i < TI.length; i++) {
            var cell = row.insertCell(i)
            if (TI[i].Type == "Input") {
                input = createInput(i, '');
                cell.innerHTML = input;
                cell.className = TI[i].Name;
            }
            if (TI[i].Type == "DDL" && TI[i].ListData != undefined) {
                ddl = createDDL(i);
                cell.innerHTML = ddl;
                cell.className = TI[i].Name;
            }
            if (TI[i].Type == "InputBool") {
                inputBool = createEnumDDL(i);
                cell.innerHTML = inputBool;
                cell.className = TI[i].Name;
            }
            if (TI[i].Type == "InputCheckbox") {
                inputCheckbox = createCheckbox(i);
                cell.innerHTML = inputCheckbox;
                cell.className = TI[i].Name;
            }
        }
        var lastCell = row.insertCell(i);
        lastCell.innerHTML = '<a href="javascript:;" title="Save" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-save ajaxSave"></i></a> <a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat cancel"></i></a>';
        lastCell.className = "text-center unmodified";

        // set ActionInsertUpdate flag\
        onActionInsertUpdate = 1;
    }
}

function AddColumnInsert(result) {
    document.getElementById("dataTablesMain").deleteRow(1);
    var tbl = document.getElementById('dataTablesMain');
    var row = tbl.insertRow(1);
    row.id = result[TI[0].Name];
    i = 1;
    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" >';
    firstCell.className = 'unmodified';
    for (i ; i < TI.length; i++) {

        var cell = row.insertCell(i)
        cell.innerHTML = UpdateTD(i, result);
        cell.className = TI[i].Name;
    }
    var lastCell = row.insertCell(i);
    lastCell.innerHTML = '<a href="javascript:;" title="Edit" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-edit ajaxEdit"></i></a> <a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-trash ajaxDelete"></i></a>';
    lastCell.className = "text-center unmodified";

    // set ActionInsertUpdate flag\
    onActionInsertUpdate = 0;
}

function createInput(CIN, CIV) {
    CIV = typeof CIV !== 'undefined' ? CIV : null;

    if (CIV != null || CIV != undefined) {
        var CIVTrim = CIV.trim();
    }

    input = '<input type="Text" Mandatory="' + TI[CIN].Mandatory + '" class="form-control ' + TI[CIN].Name + '" maxlength="' + TI[CIN].Length + '" name="' + TI[CIN].Name + '" value="' + CIVTrim + '" >';

    return input;
}

function createDDL(DDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    input = '<select name=' + TI[i].Name + '>';
    if (TI[DDLN].ListData.length != 0) {
        for (ddlLoop = 0; ddlLoop < TI[DDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TI[DDLN].ListData[ddlLoop].Value)
                selected = "selected";
            input += '<option value="' + TI[DDLN].ListData[ddlLoop].intID + '" ' + selected + '>' + TI[DDLN].ListData[ddlLoop].Value + '</option>';
        }
    }
    return input;

}

function createEnumDDL(DDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    input = '<select name=' + TI[i].Name + '>';
    if (TI[DDLN].ListData.length != 0) {
        for (ddlLoop = 0; ddlLoop < TI[DDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TI[DDLN].ListData[ddlLoop].Value)
                selected = "selected";
            input += '<option value="' + ddlLoop + '" ' + selected + '>' + TI[DDLN].ListData[ddlLoop] + '</option>';
        }
    }
    return input;

}

function createCheckbox(CCNo, CCVal) {
    CCVal = typeof CCVal !== 'undefined' ? CCVal : null;

    checked = "";
    if (CCVal == true) {
        checked = 'checked ="on"';
    }

    input = '<input type="checkbox" class="form-control ' + TI[CCNo].Name + '" ' + checked + ' >';

    return input;
}

function DeleteAll() {
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("." + table).find(inputs).filter(function () {
            return $.trim(this.value);
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TI[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TI[0].Name] = $(this).closest('tr').attr("id");
            data.push(obj);
        });
        var dataJson = JSON.stringify(data)
        if (Action != undefined) {
            var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "DeleteAll" + Action);
        } else {
            var url = String.format("/{0}/{1}/DeleteAll", GroupMenu, NameMenu);
        }
        ajax(dataJson, "DeleteAll", url);
    }
}

function UpdateTD(iUpdate, uResult) {
    if (TI[iUpdate].Type == "Date") {

        return JSONDateWithTime(uResult[TI[iUpdate].Name]);
    }
    else if (TI[iUpdate].Type == "Bool" && TI[i].ListData != undefined) {
        return (uResult[TI[iUpdate].Name] == true) ? TI[i].ListData[1] : TI[i].ListData[0];
    }
    else if (TI[iUpdate].Type == "DDL" && TI[i].ListData != undefined) {
        var selectData = TI[iUpdate].ListData.find(function (obj) {
            return obj.intID == uResult[TI[iUpdate].Name];
        });

        return selectData.Value;
    }
    else if (TI[iUpdate].Type == "InputBool" && TI[i].ListData != undefined) {
        return (uResult[TI[iUpdate].Name] == true) ? TI[i].ListData[1] : TI[i].ListData[0];
    }
    else if (TI[iUpdate].Type == "InputCheckbox") {
        checked = "";
        if (uResult[TI[iUpdate].Name] == true) {
            checked = "checked";
        }
        var checkboxHTML = '<input type="checkbox" checked="' + checked + '" disabled="disabled" >';
        return checkboxHTML;
    }
    else {
        return uResult[TI[iUpdate].Name];
    }
}

function ajax(params, action, url) {
    $.getJSON(url, { dataJSON: params }, function (result) {
        switch (action) {
            case "Save":
                if (result != null) {
                    if (result.Status == false) {
                        alert(result.Message);
                    }
                    else {
                        AddColumnInsert(result);
                        $(document).find("." + table).find(inputs).filter(function () {
                            this.value = "";
                            $(this).removeClass("success");
                            $(this).closest('td').removeClass("has-error")
                        });
                        $.notify({
                            // options
                            message: 'Success insert Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                        onActionInsertUpdate = 0;
                    }
                }
                break;
            case "UpdateCol":
                if (result != null) {
                    if (result.Status != undefined) {
                        if (result.Status == true) {
                            CancelInsertOrUpdate()
                        }
                        else {
                            alert(result.Message);
                        }
                    }
                    else {
                        $("." + cancelbutton).trigger("click");
                        for (i = 1; i < TI.length; i++) {
                            var dataResult = UpdateTD(i, result);
                            $("tr[id='" + result[TI[0].Name] + "'] td[class='" + TI[i].Name + "']").html(dataResult);
                        }
                        $(document).find("." + table).find(inputs).filter(function () {
                            this.value = "";
                            $(this).removeClass("success");
                            $(this).closest('td').removeClass("has-error")
                        });
                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                        onActionInsertUpdate = 0;
                    }
                }
                break;
            case "Delete":
                if (result != null) {
                    if (result.Status == false) {
                        alert(result.Message);
                    }
                    else {
                        $("." + table + " tr[id='" + result + "']").effect("highlight", { color: '#f4667b' }, 500, function () {
                            $("." + table + " tr[id='" + result + "']").remove();
                        });
                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                break;
            case "DeleteAll":
                if (result != null) {
                    if (result.Status == false) {
                        alert(result.Message);
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }
                    }
                    $.notify({
                        // options
                        message: 'Success delete Data'
                    }, {
                        // settings
                        type: 'success'
                    });
                }
                break;
        }
    });
}

function getEnum(iEnum) {
    $.getJSON("/SetupEnumSetup/Enum/getActiveEnum", {}, function (ResultEnum) {
        TI[iEnum]["ListData"] = ResultEnum;
    });
}

function getData() {
    for (i = 1 ; i < TI.length; i++) {
        switch (TI[i].Type) {
            case "DDL":
                if (TI[i].ListData == undefined) {
                    getDDL(i);
                }
                break;
            case "Bool":
                getEnum(i);
                break;
            case "InputBool":
                getEnum(i);
                break;
        }

    }

}

function getDDL(iDDL) {
    $.getJSON(TI[iDDL].Url, {}, function (ResultData) {
        TI[iDDL]["ListData"] = ResultData;
    });
}
