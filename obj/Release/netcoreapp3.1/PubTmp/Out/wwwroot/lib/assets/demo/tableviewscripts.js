﻿tableviewscripts = {
    API_URL: "hello",
    refreshTable: function () { },

    initVenusTableScript: function () {
        // Set API URL
        // tableviewscripts.API_URL = ;

        // set filter to refresh table
        // $(document).ready(function() {
        //   $(".radiobtnfilter2").click(function() {
        //     demovenus.refreshChartVenus();
        //   });
        // });

        // initTable
        initGRNI();
        initOPO();
        initPOA();

        // refresh table
    }
};

function initGRNI() {
    $(document).ready(function () {
        var table = $("#grniTable").DataTable({
            serverSide: true,
            ajax: {
                url: "api/venus_grni_table",
                data: function (d) {
                    d.filterParam = serializeFilterParam("filterForm");
                }
            },
            columns: [
                { name: "id" },
                { name: "po_type" },
                { name: "po_number" },
                { name: "doc_type" },
                { name: "doc_number" },
                { name: "doc_date" },
                { name: "supplier_code" },
                { name: "supplier_name" },
                { name: "remark" },
                { name: "po_currency" },
                { name: "amount" },
                { name: "unit" }
            ],
            aoColumnDefs: [
                {
                    "aTargets": [12],
                    "mData": null,
                    "bSearchable": false,
                    "mRender": function (data, type, full) {
                        var edit = '<a href="/edit-grni/' + full[0] + '" class="btn btn-primary m-1"><i class="far fa-edit"></i> Edit</a>';
                        var del = '<a href="/delete-grni/' + full[0] + '" class="btn btn-danger m-1"><i class="far fa-trash-alt"></i> Delete</a>';
                        return `${edit}${del}`;
                    }
                }
            ]
        });
        $(".radiobtnfilter2").click(function () {
            table.ajax.reload();
        });
    });
}
function initOPO() {
    $(document).ready(function () {
        var table = $("#opoTable").DataTable({
            serverSide: true,
            ajax: {
                url: "api/venus_opo_table",
                data: function (d) {
                    d.filterParam = serializeFilterParam("filterForm");
                }
            },
            columns: [
                { name: "id" },
                { name: "order_number" },
                { name: "or_ty" },
                { name: "amount_to_receive" },
                { name: "ordered_amount" },
                { name: "order_co" },
                { name: "supplier_number" },
                { name: "supplier_name" },
                { name: "line_description" },
                { name: "last_status" },
                { name: "next_status" },
                { name: "gl_date" },
                { name: "order_date" },
                { name: "quantity_to_receive" },
                { name: "unit_cost" },
                { name: "original_order_no" },
                { name: "orig_ord_type" },
                { name: "account_number" },
                { name: "cost_center" },
                { name: "obj_acct" },
                { name: "line_number" },
                { name: "managrl_code_1" },
                { name: "managrl_code_2" },
                { name: "managrl_code_3" },
                { name: "managrl_code_4" },
                { name: "second_item_number" }
            ],
            aoColumnDefs: [
                {
                    "aTargets": [26],
                    "mData": null,
                    "bSearchable": false,
                    "mRender": function (data, type, full) {
                        var edit = '<a href="/edit-opo/' + full[0] + '" class="btn btn-primary m-1"><i class="far fa-edit"></i> Edit</a>';
                        var del = '<a href="/delete-opo/' + full[0] + '" class="btn btn-danger m-1"><i class="far fa-trash-alt"></i> Delete</a>';
                        return `${edit}${del}`;
                    }
                }
            ]
        });
        $(".radiobtnfilter2").click(function () {
            table.ajax.reload();
        });
    });
}
function initPOA() {
    $(document).ready(function () {
        var table = $("#poaTable").DataTable({
            serverSide: true,
            ajax: {
                url: "api/venus_poa_table",
                data: function (d) {
                    d.filterParam = serializeFilterParam("filterForm");
                }
            },
            columns: [
                { name: "id" },
                { name: "company" },
                { name: "payment_ref" },
                { name: "voucher_gl_date" },
                { name: "supplier_invoice_number" },
                { name: "invoice_date" },
                { name: "payment_gl_date" },
                { name: "voucher_doc_type" },
                { name: "voucher_number" },
                { name: "po_number" },
                { name: "po_type" },
                { name: "po_date" },
                { name: "po_description_line_1" },
                { name: "po_description_line_2" },
                { name: "vendor_code" },
                { name: "vendor_name" },
                { name: "item_code" },
                { name: "item_description" },
                { name: "stocking_type" },
                { name: "stocking_type_description" },
                { name: "account_code" },
                { name: "account_description" },
                { name: "business_unit" },
                { name: "amount" },
                { name: "domestic_currency_code" },
                { name: "currency_code" },
                { name: "search_type" }
            ],
            aoColumnDefs: [
                {
                    "aTargets": [27],
                    "mData": null,
                    "bSearchable": false,
                    "mRender": function (data, type, full) {
                        var edit = '<a href="/edit-poa/' + full[0] + '" class="btn btn-primary m-1"><i class="far fa-edit"></i> Edit</a>';
                        var del = '<a href="/delete-poa/' + full[0] + '" class="btn btn-danger m-1"><i class="far fa-trash-alt"></i> Delete</a>';
                        return `${edit}${del}`;
                    }
                }
            ]
        });
        $(".radiobtnfilter2").click(function () {
            table.ajax.reload();
        });
    });
}
