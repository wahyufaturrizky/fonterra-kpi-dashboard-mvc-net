﻿var trcopyDeduction;
var onActionInsertUpdateDeduction = 0;
var inputs = ':checked,:selected,:text,textarea';
var classTableDeduction = "tableDeduction";
var IDTableDeduction = "dataTablesMainDeduction"
var updateButtonDeduction = "ajaxUpdateDeduction";
var editButtonDeduction = "ajaxEditDeduction";
var cancelButtonDeduction = "ajaxCancelDeduction";
var deleteButtonDeduction = "ajaxDeleteDeduction";
var deleteButtonDeductionTab = "ajaxDeleteTab";
var rejectButtonDeduction = "ajaxRejectDeduction";

var approveButtonDeduction = "ajaxApproveDeduction";
var dataSource = 0;
var saveButtonDeduction = "ajaxSaveDeduction";
var effect = "flash";
var IDTDEditDeduction = 0;
var DecimalFormat = undefined;
var AccessList = undefined;
var onLoopDeduction = 1;
var getLoopDeduction = 1;
var maskNumberSetting = {
    thousands: ",",
    decimal: ".",
    integer: false,
};

var maskNumberSetting2 = {
    thousands: "",
    decimal: "",
    integer: true,
};

$(document).ready(function () {
    // get data source for particular dropdown list in current document
    //getDataSourceDeduction();

    /**
     * Save button click event
     */

    $(document).on("click", "." + saveButtonDeduction, function () {

        // Create new data object.
        var data = getFormDataDeduction();

        if (data.valid) {
            var dataJson = JSON.stringify(data.record);
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Save" + Action);

                ajaxDeduction(dataJson, "Save", url);
            }
            else {
                url = String.format("/{0}/{1}/Save", GroupMenu, NameMenu);
                ajaxDeduction(dataJson, "Save", url);
            }
        }
        else {
            focusErrorTDDeduction();
        }
    });

    /**
     * Update button click event
     */
    $(document).on("click", "." + updateButtonDeduction, function () {

        // Create new data object.
        var data = getFormDataDeduction();

        // Check if data to create or update are valid
        if (data.valid) {

            // Add key into the object
            data.record = addObjectKeyDeduction(data.record, this);

            var dataJson = JSON.stringify(data.record)
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Update" + Action);
                ajaxDeduction(dataJson, "UpdateAction", url);

            }
            else {
                url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
                ajaxDeduction(dataJson, "UpdateCol", url);
            }
            //ajaxDeduction(dataJson, "UpdateCol", url);
        }
        else {
            focusErrorTDDeduction();
            $.notify({
                // options
                message: "Data is Not Valid"
            }, {
                // settings
                type: 'danger'
            });

        }
    });

    /**
     * Double click event for the table
     */

    if (!$("." + classTableDeduction).hasClass('no-dblClick')) {
        $(document).on("dblclick", "." + classTableDeduction + " td", function (e) {
            var id = $(this).closest('tr').attr("id");
            getDataSourceDeduction("EditRow", id)
        });
    }
    /**
     * Edit button click event for the particular row in the table
     */
    $(document).on("click", "." + editButtonDeduction, function () {
        //$('input:checkbox').not(this).prop('checked', false);

        var id = $(this).closest('tr').attr("id");
        getDataSourceDeduction("EditRow", id)
    });

    /**
     * Cancel button click event
     */
    $(document).on("click", "." + cancelButtonDeduction, function () {
        var id = $(this).closest('tr').attr("id");

        if (trcopyDeduction) {
            $("." + classTableDeduction + " tr[id='" + id + "']").html(trcopyDeduction);
            trcopyDeduction = '';
        } else {
            document.getElementById(IDTableDeduction).deleteRow(1);
        }

        onActionInsertUpdateDeduction = 0;
    });

    /** 
     * Delete button click event for the particular row in the table
     */

    $(document).on("click", "." + deleteButtonDeduction, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKeyDeduction({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Delete" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenu, NameMenu);
                    }
                    ajaxDeduction(dataJson, "Delete", url);
                }
            }
        }
    });

    $(document).on("click", "." + deleteButtonDeductionTab, function () {
        var TabName = $(this).attr("id");

        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKeyDeductionTabDeduction({}, this, TabName);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof ActionTab[TabName] !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "Delete" + ActionTab[TabName]);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenuTab[TabName], NameMenuTab[TabName]);
                    }
                    ajaxDeduction(dataJson, "Delete", url);
                }
            }
        }
    });


    /** 
    * Reject button click event for the particular row in the table
    */
    $(document).on("click", "." + rejectButtonDeduction, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Reject Data?")) {

                // Add key into the object
                var data = addObjectKeyDeduction({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Reject" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Reject", GroupMenu, NameMenu);
                    }
                    ajaxDeduction(dataJson, "Reject", url);
                }
            }
        }
    });


    /** 
   * Approve button click event for the particular row in the table
   */


    $(document).on("click", "." + approveButtonDeduction, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Post / Approve Data?")) {

                // Add key into the object
                var data = addObjectKeyDeduction({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Approve" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Approve", GroupMenu, NameMenu);
                    }
                    ajaxDeduction(dataJson, "Approve", url);
                }
            }
        }
    });

    /**
     * Document click event
     */
    $(document).click(function (e) {
        if (($(e.target).closest('tr').attr("id") != IDTDEditDeduction) && $(e.target).closest(".ActionBtn").attr("class") != "ActionBtn" &&
            $(e.target).closest(".unmodified").length == 0 &&
            $(e.target).closest(".select2-dropdown").length == 0 &&
            onActionInsertUpdateDeduction == 1 && typeof MultiAdd == 'undefined') {
            CancelInsertOrUpdateDeduction();
        }
        else {
            if ($(e.target).closest(".unmodifiedSort").length > 0) {
                onActionInsertUpdateDeduction = 0;
            }
        }
    });

    /**
     * Checkbox click all event
     */
    $("#checkAll").click(function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".checkAll").each(function () {
        var that = $(this);
        var table = that.closest('table');
        that.click(function () {
            that.closest('table').find('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
});

function focusErrorTDDeduction() {
    if ($("." + classTableDeduction + " td").hasClass('has-error')) {
        var errorTD = $(document).find('td.has-error').find("input")[0];
        if (errorTD != undefined) {
            document.getElementById(errorTD.id).focus();
        }
    }
}

/**
 * Get form data from current row
 */
function getFormDataDeduction() {
    var validation = [];
    var valid = true;
    var data = {};

    // Loop thru input elements available in the table
    $(document)
        .find('.' + classTableDeduction + ' tbody tr[id="' + IDTDEditDeduction + '"')
        .find(inputs)
        .filter(function () {

            var controlType = $(this).parent().prop('type');

            var key;
            var value;
            var mandatory;

            // Check control type to get value and mandatory
            if (controlType && controlType === 'select-one') {
                if ($(this).parent().prop('id') == "") {
                    key = $(this).parent().closest('td').prop('class')
                }
                else {
                    key = $(this).parent().prop('id');
                }
                value = $.trim(this.value);
                mandatory = $(this).parent().attr("Mandatory");

            } else {

                controlType = $(this).prop('type');

                key = $(this).attr('id');

                if (controlType === 'checkbox') {

                    value = $(this).prop('checked') ? true : false;
                    mandatory = $(this).attr("Mandatory");

                } else {

                    value = $.trim(this.value);
                    if (key.includes("Date")) {
                        var parts = this.value.split(/[/ :]/);

                        //value = new Date(parts[2], parts[1] - 1, parts[0]);
                        value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

                        if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
                            value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
                        }
                        //value = key.includes("Date") ? new Date(tes) : $.trim(this.value);
                    }

                    mandatory = $(this).attr("Mandatory");

                }
            }

            // Check value and mandatory
            if (value === "" && mandatory === "true") {

                // Append error class
                $(this).closest('td').addClass("has-error");

                validation.push(0);

            } else {

                // Push data
                if (value != "") {
                    data[key] = value;
                }
                // Append success class
                // This assignment help to filter selected option for DDL when switch back to table row
                $(this).addClass("success");

                validation.push(1);
            }
        });

    // Check validation
    for (var i = 0; i < validation.length; i++) {

        // break if 0 found
        if (validation[i] === 0) {
            valid = false;
            break;
        }
    }

    return {
        valid: valid,
        record: data
    }
}

/**
 * Add key property into object for update or delete
 * @param {object} data
 * @param {object} _this - Copy of current row
 */
function addObjectKeyDeduction(data, _this) {

    data[TIDeduction[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

function addObjectKeyDeductionTabDeduction(data, _this, tabName) {
    var tempTIDeduction = TIDeductionTab[tabName];

    data[tempTIDeduction[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

/**
 * Make ajax request to create or update data.
 * @param {object} params
 * @param {string} action - Action name
 * @url {string}
 */
function ajaxDeduction(params, action, url) {
    $.post(url, { dataJSON: params }, function (result) {

        switch (action) {
            case "Save":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });

                        removeClassAfterCreateUpdateDeduction();

                    } else {
                        AddNewRowDeduction(result);

                        removeClassAfterCreateUpdateDeduction(true);
                        onActionInsertUpdateDeduction = 0;
                        $.notify({
                            // options
                            message: 'Success insert Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateCol":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdateDeduction();

                        } else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    } else {

                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + classTableDeduction + " tr[id=" + IDTDEditDeduction + "]").clone();

                        $("." + cancelButtonDeduction).trigger("click");

                        for (var i = 1; i < TIDeduction.length; i++) {
                            var dataResult = cellValueDeduction(TIDeduction[i], result[TIDeduction[i].Name], trFormObject, i);

                            $("tr[id='" + result[TIDeduction[0].Name] + "'] td[class='" + TIDeduction[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdateDeduction(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdateDeduction = 0;
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateAction":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdateDeduction();

                        } else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    } else {

                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + classTableDeduction + " tr[id=" + IDTDEditDeduction + "]").clone();

                        $("." + cancelButtonDeduction).trigger("click");

                        for (var i = 1; i < TIDeduction.length; i++) {
                            var dataResult = cellValueDeduction(TIDeduction[i], result[TIDeduction[i].Name], trFormObject, i);

                            $("tr[id='" + result[TIDeduction[0].Name] + "'] td[class='" + TIDeduction[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdateDeduction(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdateDeduction = 0;

                        //location.reload();
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "Delete":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $("." + classTableDeduction + " tr[id='" + result + "']").effect("highlight", { color: '#f4667b' }, 500, function () {
                            $("." + classTableDeduction + " tr[id='" + result + "']").remove();
                        });

                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "DeleteAll":
                if (result != null) {
                    if (result.Status == false) {
                        location.reload();
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableDeduction + " tr[id='" + result[i] + "']").remove();
                        }
                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "Reject":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableDeduction + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Reject Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
                setTimeout(function () { window.location.href = urlredirect; }, 2000);
                break;
            case "Approve":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableDeduction + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Approved Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "RegenerateAll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + classTableDeduction + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Regenerate Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
        }
    });

}

/**
 * Remova css class after insert or update event
 * @param {bool} removeValue - Flag to indicate wethear need to clear the value or not.
 */
function removeClassAfterCreateUpdateDeduction(removeValue) {
    $(document).find("." + classTableDeduction).find(inputs).filter(function () {

        if (removeValue) {
            this.value = "";
        }

        $(this).removeClass("success");
        $(this).closest('td').removeClass("has-error")
    });
}
/**
 * render row to insert new data
 */
function renderRowDeduction() {
    $(".datepicker").datepicker("destroy");
    // flag to indicate weather need to bind data or not
    var flagBindData = 0;

    // get table object
    var tbl = document.getElementById(IDTableDeduction);

    // insert row into table object
    var row = tbl.insertRow(1);

    // count total of rows
    var rowCount = $("#" + IDTableDeduction + " tbody tr").length;

    // set row id equal to total row + 1
    row.id = (rowCount + 1) + 'guid';

    // set global current table row edit
    IDTDEditDeduction = row.id;

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" disabled="disabled">';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TIDeduction.length; i++) {
        var obj = TIDeduction[i];
        var cell = row.insertCell(i)

        var control;
        switch (obj.Type) {
            case 'Input':
                control = createInputTextDeduction(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null);
                break;
            case 'InputNumber':
                control = createInputNumberDeduction(obj);
                break;
            case 'InputNumeric':
                control = createInputNumericDeduction(obj);
                break;
            case 'DDL':
                control = createDDLDeduction(i);
                break;

            case 'InputEnum':
                control = createEnumDDLDeduction(i);
                break;

            case 'InputCheckbox':
                control = createInputCheckDeduction(obj);
                break;

            case 'AutocompleteDDL':
                control = createSelectDeduction(obj);
                break;

            case 'InputDate':
                control = createInputDateDeduction(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null);
                break;

            case 'InputLookup':
                control = CreateInputlookupDeduction(obj);
                break;

            case 'InputWithValidate':
                control = createInputTextWithValidateDeduction(obj);
                break;

            default:
                // reset control
                control = '';
                break;
        }

        // Check if control object exist.
        // If control exist, create the object inside cell.
        if (control) {
            cell.innerHTML = control;
            cell.className = obj.Name;
        }

        // After creating control, check if it need to bind to data source.
        // This must be executed after control is exist in the cell.
        if (obj.Type === 'AutocompleteDDL') {
            bindAutocompleteDeductionEvent(obj);

        }
    }

    var lastCell = row.insertCell(i);
    lastCell.innerHTML = '<a href="javascript:;" title="Save" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxSaveDeduction"></i></a> <a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat ajaxCancelDeduction"></i></a>';
    lastCell.className = "text-center unmodified";

    //$(".datepicker").datepicker();
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    }).inputmask('date');

    $('[name=integer-default]').maskNumber();
    // set ActionInsertUpdate flag
    onActionInsertUpdateDeduction = 1;
}
/**
 * Create new row to insert new data
 */
function AddRowDeduction(DeleteBeforeAdd) {
    if (DeleteBeforeAdd != null) {
        var NewonActionInsertUpdate = 'onActionInsertUpdate' + DeleteBeforeAdd;
        if ((onActionInsertUpdateDeduction == 0 && window[NewonActionInsertUpdate] == 0) || typeof MultiAdd !== 'undefined') {
            //For 2 table
            getDataSourceDeduction("AddRow");
        }
    }
    else {
        if (onActionInsertUpdateIncome == 0 || typeof MultiAdd !== 'undefined') {
            //For 2 table
            getDataSourceDeduction("AddRow");
        }
    }
}

/**
 * Edit row
 * @param {string} id
 */
function editRowDeduction(id) {
    if (id && onActionInsertUpdateDeduction == 0) {
        $(".datepicker").datepicker("destroy");
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
        // hide editing row, for the time being
        //$("." + table + " tr:last-child").fadeOut("fast");

        // Set global id for current edit table row
        IDTDEditDeduction = id;

        // create html text to insert into row
        var html = '<td class="unmodified"><input type="checkbox" ></td>';

        // get current row record to update
        var row = document.getElementById(id);
        var selectedDate = new Date();
        for (var i = 1; i < TIDeduction.length; i++) {
            var obj = TIDeduction[i];
            var val = ""
            if (row.getElementsByClassName(obj.Name)[0] == undefined) {
                val = "Please check this row's class !!!";
                $.notify({
                    // options
                    message: "Class " + obj.Name + "didn't found !!!"
                }, {
                    // settings
                    type: 'danger'
                });
            }
            else {
                val = row.getElementsByClassName(obj.Name)[0].innerText;
            }


            var stringContent;

            switch (obj.Type) {
                case 'Input':
                    stringContent = createInputTextDeduction(obj, val);
                    break;
                case 'InputNumber':
                    stringContent = createInputNumberDeduction(obj, val);
                    break;
                case 'InputNumeric':
                    stringContent = createInputNumericDeduction(obj, val);
                    break;

                case 'DDL':
                    if (row.getElementsByClassName(obj.Name)[0].firstElementChild != null) {
                        stringContent = createSelectDeduction(obj);
                        stringContent += createInputHiddenDeduction(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    } else {
                        stringContent = createDDLDeduction(i, val);
                    }
                    break;

                case 'InputEnum':
                    stringContent = createEnumDDLDeduction(i, val);
                    break;

                case 'InputCheckbox':
                    val = row.getElementsByClassName(obj.Name)[0].innerHTML.includes("checked") ? true : false;
                    stringContent = createInputCheckDeduction(obj, val);
                    break;

                case 'AutocompleteDDL':
                    stringContent = createSelectDeduction(obj);
                    stringContent += createInputHiddenDeduction(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    break;

                case 'InputDate':
                    stringContent = createInputDateDeduction(obj, val);
                    selectedDate = val;
                    break;

                default:
                    stringContent = val;
                    break;
            }

            html += '<td class="' + obj.Name + '">' + stringContent + '</td>';
        }

        html += '<td class="text-center unmodified">'
        if (AccessList != "" || AccessList.IsUpdate == "true") {
            html += '<a href="javascript:;" title="Update" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxUpdateDeduction"></i></a>'
        }
        html += '<a href="javascript:;" title="Return" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat ajaxCancelDeduction"></i></a></td>'

        // Before replacing the TR contents, make a copy so when user clicks on .
        trcopyDeduction = $("." + classTableDeduction + " tr[id=" + id + "]").html();

        // Make jQuery object of current table row before replaceing the TR content.
        // This object will be used to get value from select option
        var trObject = $("." + classTableDeduction + " tr[id=" + id + "]").clone();

        // Create html controls.
        // Update edit row with controls created.
        $("." + classTableDeduction + " tr[id=" + id + "]").html(html);

        // Bind data source for dropdown list or autocomplete.
        // This must be executed after html controls are created.
        for (var i = 1; i < TIDeduction.length; i++) {
            var obj = TIDeduction[i];

            if (obj.Type === 'DDL') {

                var hidden = $(trObject).find('.' + obj.Name).find(':hidden');
                var selectedValue = hidden ? $(hidden).val() : $.trim($(trObject).find('.' + obj.Name).text());

                bindSelectDataDeduction(obj, selectedValue);

            } else if (obj.Type === 'AutocompleteDDL') {
                var selectedValue = {
                    id: $(trObject).find('.' + obj.Name).find(':hidden').val(),
                    text: $.trim($(trObject).find('.' + obj.Name).text())
                };

                bindAutocompleteDeductionEvent(obj, selectedValue);

            }
        }
        $('[name=integer-default]').maskNumber();
        //$(".datepicker").datepicker();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        }).inputmask('date').datepicker('setDate', ConvertStringToDatetime(selectedDate));

        // set ActionInsertUpdate flag
        onActionInsertUpdateDeduction = 1;
    }
}

/**
 * Bind autocomplete event
 * @param {object} obj
 * @param {object} selectedObject - Expected { id: idValue, text: textValue }
 */
function bindAutocompleteDeductionEvent(obj, selectedObject) {
    var idSelector = '#' + obj.Name;
    var url = obj.Url;

    $(idSelector).select2({
        minimumInputLength: 1,
        language: {
            noResults: function () {
                return "Tidak ada hasil";
            },
            inputTooShort: function (args) {
                return "Minimal 1 huruf";
            },
        },
        ajax: {
            url: url,
            dataType: 'json',
            data: function (term, page) {
                return {
                    query: term.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    // Set default selected if exist.
    if (selectedObject) {
        $(idSelector).select2("trigger", "select", { data: selectedObject });
    }
}

/**
 * Bind data to select
 * @param {object} obj
 * @param {string} selectedValue
 */
function bindSelectDataDeduction(obj, selectedValue) {
    var idSelector = '#' + obj.Name;

    $(idSelector).select2({
        minimumResultsForSearch: -1,
        data: obj.ListData
    });

    // Set default selected if exist.
    if (selectedValue) {
        var selected = {};

        // Get object for selected value.
        for (var i = 0; i < obj.ListData.length; i++) {
            if (String(obj.ListData[i].id) === selectedValue || obj.ListData.text === selectedValue) {
                selected.id = obj.ListData[i].id,
                selected.text = obj.ListData[i].text

                break;
            }
        }

        $(idSelector).select2("trigger", "select", { data: selected });
    }
}

/**
 * Add new row after insert
 * @param {object} result - Json object returned after inserting new data.
 */
function AddNewRowDeduction(result) {

    // Create copy of table row.
    // To keep form before the first row is deleted.
    // This purpose is to get value from selected option.
    var trFormObject = $("." + classTableDeduction + " tr[id=" + IDTDEditDeduction + "]").clone();

    // Delete the first row
    document.getElementById(IDTableDeduction).deleteRow(1);

    // Get table object
    var tbl = document.getElementById(IDTableDeduction);

    // Insert new row
    var row = tbl.insertRow(1);

    // Set row id equal to total row + 1
    row.id = result[TIDeduction[0].Name];

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" >';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TIDeduction.length; i++) {

        var obj = TIDeduction[i];
        var cell = row.insertCell(i)
        cell.innerHTML = cellValueDeduction(obj, result[obj.Name], trFormObject, i);
        cell.className = obj.Name;
    }

    var lastCell = row.insertCell(i);

    if (AccessList != "" || AccessList.IsUpdate == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Edit" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-edit ajaxEditDeduction"></i></a>';
    }
    if (AccessList != "" || AccessList.IsDelete == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-trash ajaxDeleteDeduction"></i></a>';
    }
    lastCell.className = "text-center unmodified";

    // set ActionInsertUpdate flag
    onActionInsertUpdateDeduction = 0;
}

/**
 * Add cell value
 * @param {object} obj - Json object for the model.
 * @param {object} value - The value for each property defined in obj.
 * @param {object} trFormObject - Object copy contain table row form before inserting to database
 */
function cellValueDeduction(obj, value, trFormObject, loop) {
    switch (obj.Type) {
        case 'AutocompleteDDL':
        case 'DDL':
            // Create selector '#IDControl option:selected'
            var selector = '#' + obj.Name + ' .success';

            // Get object from table row object
            var selectObject = $(trFormObject).find(selector);

            // Set label
            var label = selectObject.length ? $(selectObject).text() : value;

            if (label == value) {
                var selectData = TIDeduction[loop].ListData.find(function (v) {
                    return v.id == value;
                });
                label = selectData.text;
            }

            // Create hidden control
            var control = createInputHiddenDeduction(obj, value);

            return label + control;

        case 'Date':
            return value ? JSONDateWithTime(value) : '';

        case 'Bool':
            if (value != undefined && obj.ListData != undefined) {
                var text = $.grep(
                    obj.ListData,
                    function (n, i) {
                        return value === true ? n.toLowerCase() == 'active' : n.toLowerCase() === 'inactive';
                    });

                return text.length ? text[0] : value;
            }
            else {
                return null;
            }

        case 'InputEnum':
            if (obj.ListData.length) {
                var selectEnumData = TIDeduction[loop].ListData.find(function (text, v) {
                    return v == value;
                });
                return selectEnumData;
            }

            return '';

        case 'InputCheckbox':
            return createInputCheckDeduction(obj, value, true);

        case 'InputDate':
            return value ? JSONDateWithTime(value) : '';

        case 'InputNumber':
            value = addDecimalSeparatorDeduction(value, maskNumberSetting);
            value = addThousandSeparatorDeduction(value, maskNumberSetting);
            return value;

        case 'InputNumeric':
            value = addThousandSeparator2Deduction(value, maskNumberSetting2);
            return value;


        default:
            return value ? value : '';
    }
}

/**
 * Cancel insert or update.
 */
function CancelInsertOrUpdateDeduction() {

    if (confirm("Do you really want to quit insert or update data ?")) {
        if (trcopyDeduction) {

            $("." + classTableDeduction + " tr[id='" + IDTDEditDeduction + "']").html(trcopyDeduction);
            //$("." + table + " tr:last-child").fadeIn("fast");

            // clear global variable
            trcopyDeduction = '';
            IDTDEditDeduction = '';

        } else {
            document.getElementById(IDTableDeduction).deleteRow(1);
        }
        onActionInsertUpdateDeduction = 0;
    }

}

function ApproveAllDeduction() {
    if (confirm("Do you really want to Post / Approve record ?")) {
        var $inputs =
               $(document).find("." + classTableDeduction).find(":checked").filter(function () {
                   if (this.id == "") {
                       return $.trim(this.value);
                   }
               });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TIDeduction[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TIDeduction[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TIDeduction[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "ApproveAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/ApproveAll", GroupMenu, NameMenu);
            }
            ajaxDeduction(dataJson, "Approve", url);
        } else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
        //var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
        //window.location.href = urlredirect;
    }
}


function RejectAllDeduction() {
    var $inputs =
    $(document).find("." + classTableDeduction).find(":checked").filter(function () {
        if (this.id == "") {
            return $.trim(this.value);
        }
    });
    var array = $inputs.map(function () {
        return this.value;
    }).get();
    var idName = TIDeduction[0].Name;
    var data = [];
    $.each($inputs, function () {
        var obj = new Object();
        obj[TIDeduction[0].Name] = $(this).closest('tr').attr("id");
        if (obj[TIDeduction[0].Name] != undefined) {
            data.push(obj);
        }
    });
    var dataJson = JSON.stringify(data)
    if (typeof Action !== 'undefined') {
        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "RejectAll" + Action);
    } else {
        var url = String.format("/{0}/{1}/RejectAll", GroupMenu, NameMenu);
    }
    ajaxDeduction(dataJson, "Reject", url);
    var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
    window.location.href = urlredirect;
}


function DeleteAllDeduction() {
    if (onActionInsertUpdateDeduction == 1) {
        //can't delete while on action Insert / Update
        return false;
    }
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("." + classTableDeduction).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TIDeduction[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TIDeduction[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TIDeduction[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "DeleteAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenu, NameMenu);
            }
            ajaxDeduction(dataJson, "DeleteAll", url);
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }

    }
}

function TabDeleteAllDeduction(TabName) {
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("#tab" + TabName + "." + classTableDeduction).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        //var idName = TIDeduction[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            var tempTIDeduction = TIDeductionTab[TabName];

            //data[tempTIDeduction[0].Name] = $(_this).closest('tr').attr("id");

            obj[tempTIDeduction[0].Name] = $(this).closest('tr').attr("id");
            if (obj[tempTIDeduction[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof ActionTab !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "DeleteAll" + ActionTab[TabName]);
            } else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenuTab[TabName], NameMenuTab[TabName]);
            }
            ajaxDeduction(dataJson, "DeleteAll", url);
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
    }
}

/**
 * Create DDL.
 * @param {object} DDLN
 * @param {string} ddlVal
 */

function createDDLDeduction(DDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    //AutoSelect For IsActive
    if (ddlVal == null && TIDeduction[DDLN].Name == "IsActive") {
        ddlVal = "Active";
    }

    onChangeFunction = typeof TIDeduction[DDLN].onchange !== 'undefined' ? TIDeduction[DDLN].onchange : "";

    input = '<select class="form-control" Mandatory="' + TIDeduction[DDLN].Mandatory + '" name=' + TIDeduction[DDLN].Name + ' id=' + TIDeduction[DDLN].Name + ' onchange=' + onChangeFunction + '>';
    //input += '<option value="">Select</option>'
    if (TIDeduction[DDLN].ListData.length != 0) {
        input += '<option value="">Choose Any</option>';
        for (ddlLoop = 0; ddlLoop < TIDeduction[DDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TIDeduction[DDLN].ListData[ddlLoop].text)
                selected = "selected";
            input += '<option value="' + TIDeduction[DDLN].ListData[ddlLoop].id + '" ' + selected + '>' + TIDeduction[DDLN].ListData[ddlLoop].text + '</option>';
        }
    } else {
        input += '<option value="">No Data Available</option>';
    }
    return input;

}

/**
 * Create Input hidden.
 * @param {object} obj
 * @param {string} value
 */
function createInputHiddenDeduction(obj, value) {
    value = value ? $.trim(value) : '';

    return '<input type="hidden" id="hidden-' + obj.Name + '" value="' + value + '">';
}

/**
 * Create Input text.
 * @param {object} obj
 */
function createInputTextDeduction(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" >' + value + '</textarea>';
    }
    return inputType;
}

function createInputTextWithValidateDeduction(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control validateAmount ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" >' + value + '</textarea>';
    }
    return inputType;
}

function createInputNumberDeduction(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control maskNumber ' + obj.Name + '" maxlength="' + obj.Length + '" name="currency-default" id="' + obj.Name + '" value="' + value + '">';
    return inputType;
}

function createInputNumericDeduction(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="number" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    //var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '" onkeypress="return ' + (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57 + '">';
    return inputType;
}

function isNumberKeyDeduction(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        alert('Salah');
    }
    else {
        alert('Benar');
    }

}

function CreateInputlookupDeduction(obj, value) {
    return '<div class="form-group " style="margin: 0px !important;"><div class="input-group"><div class="bc-wrapper">' +
           '<input class="form-control autocomplete" data-val="true" data-val-number="The field IDApplicant must be a number." data-val-required="The IDApplicant field is required." id="' + obj.Name + '" name="' + obj.Name + '" readonly="readonly" type="hidden" value="0" autocomplete="off">' +
           '<div class="bc-menu list-group"></div></div><div class="bc-wrapper"><input class="form-control autocomplete" data-val="true" data-val-required="The NIK field is required" id="' + obj.ID + '" name="' + obj.ID + '" readonly="readonly" type="text" value="" autocomplete="off">' +
           '<div class="bc-menu list-group" style="display: none;"></div></div><span class="input-group-btn"><a href="' + obj.Url + '" id="idApplicationLU" onclick="showModal(this, 2);return false;" title="OrganizationLookup" class="btn btn-default">' +
           '<i class="glyphicon glyphicon-search"></i></a></span><span class="field-validation-valid text-danger" data-valmsg-for="ApplicantNIK" data-valmsg-replace="true"></span></div></div>';
}
function createInputDateDeduction(obj, value) {
    value = value ? value.trim() : '';

    return '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control datepicker ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';;
}

/**
 * Create Input type checkbox.
 */
function createInputCheckDeduction(obj, value, disabled) {
    var checked = value ? 'checked ="on"' : '';
    var options = disabled ? checked + ' disabled="disabled"' : checked;

    return '<input type="checkbox" Mandatory="' + obj.Mandatory + '" class=" ' + obj.Name + '" ' + options + ' id="' + obj.Name + '">';
}

/**
 * Create Select.
 * @param {object} obj
 * @param {object} options
 */
function createSelectDeduction(obj, options) {
    var selectOptions = '<option value="">Select</option>';

    return '<select class="form-control ' + obj.Name + '" mandatory="' + obj.Mandatory + '" id="' + obj.Name + '">' + selectOptions + '</select>';
}

/**
 * TODO Check code below
 */
function createEnumDDLDeduction(EDDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    input = '<select Mandatory="' + TIDeduction[EDDLN].Mandatory + '" name=' + TIDeduction[EDDLN].Name + '>';
    if (TIDeduction[EDDLN].ListData.length != 0) {
        for (ddlLoop = 0; ddlLoop < TIDeduction[EDDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TIDeduction[EDDLN].ListData[ddlLoop])
                selected = "selected";
            input += '<option value="' + ddlLoop + '" ' + selected + '>' + TIDeduction[EDDLN].ListData[ddlLoop] + '</option>';
        }
    }
    return input;
}

/**
 * Function to get data source for drop down list.
 * The data source will be used for drop down list in current document.
 */
function getDataSourceDeduction(ActionRow, RowID) {
    onLoopDeduction = 1;
    getLoopDeduction = 1;
    getDecimalDataSourceDeduction(ActionRow, RowID);
    getAccessDataSourceDeduction(ActionRow, RowID);
    for (var i = 1 ; i < TIDeduction.length; i++) {
        switch (TIDeduction[i].Type) {
            case 'DDL':
                getDropdownDataSourceDeduction(TIDeduction[i], TIDeduction[i].Url, ActionRow, RowID);
                break;
            case 'DDLWithOnChanged':
                getDropdownDataSourceDeduction(TIDeduction[i], TIDeduction[i].Url, ActionRow, RowID);
                break;
            case 'InputEnum':
                getDropdownDataSourceDeduction(TIDeduction[i], TIDeduction[i].Url, ActionRow, RowID);
                break;
        }
    }
    if (getLoopDeduction > 1) {
        waitingDialog.show('Loading...');
    }
    else {
        if (ActionRow == "AddRow") {
            renderRowDeduction();
        }
        else {
            editRowDeduction(RowID)
        }
    }
}

function getDecimalDataSourceDeduction(ActionRow, RowID) {
    if (DecimalFormat == undefined) {
        getLoopDeduction++;
        $.getJSON("/SetupEnumSetup/Enum/getDecimalFormatString", function (result) {
            DecimalFormat = result;
            onLoopDeduction++;
            closeLoadingBarDeduction(onLoopDeduction, ActionRow, RowID)
        });
    }
}

function getAccessDataSourceDeduction(ActionRow, RowID) {
    if (AccessList == undefined) {
        getLoopDeduction++;
        $.getJSON("/GlobalControlAccessor/GlobalAccess/GetAccessList", function (result) {
            AccessList = result;
            onLoopDeduction++;
            closeLoadingBarDeduction(onLoopDeduction, ActionRow, RowID)
        });
    }
}

function closeLoadingBarDeduction(onLoopDeduction, ActionRow, RowID) {
    if (onLoopDeduction == getLoopDeduction) {
        waitingDialog.hide();
        if (ActionRow == "AddRow") {
            renderRowDeduction();
        }
        else {
            editRowDeduction(RowID)
        }
    }
}

/**
 * Get data source with ajax.
 * Parameter given are object to set and url.
 */
function getDropdownDataSourceDeduction(obj, url, ActionRow, RowID) {

    // only make request if the object is not available
    if (!obj.ListData) {
        getLoopDeduction++;
        url = !url && obj.Url ? obj.Url : url;
        $.getJSON(url, function (data) {
            obj.ListData = data;
            onLoopDeduction++;
            closeLoadingBarDeduction(onLoopDeduction, ActionRow, RowID)
        }).fail(function () {
            $.post("/GlobalControlAccessor/GlobalAccess/jsonErrorLog", { dataJSON: url }, function (result) {
                onLoopDeduction++;
                closeLoadingBarDeduction(onLoopDeduction, ActionRow, RowID)
                var splitUrl = url.split("/");
                $.notify({
                    // options
                    message: "Fail to get " + splitUrl[3]
                }, {
                    // settings
                    type: 'danger'
                });
            });
        });
    }
}

function addDecimalSeparatorDeduction(value, settings) {
    if (DecimalFormat > 0) {
        var DecimalStringFormat = String.format("(\\d\{{0}\})$", DecimalFormat);
        var DecimalRegExp = new RegExp(DecimalStringFormat, '');
        value = value.toString().replace(DecimalRegExp, settings.decimal.concat("$1"));

        //value = value.replace(/(\d{2})$/, settings.decimal.concat("$1"));
        value = value.toString().replace(/(\d+)(\d{3}, \d{2})$/g, "$1".concat(settings.thousands).concat("$2"));
    }
    return value;
}

function addThousandSeparatorDeduction(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}

function addThousandSeparator2Deduction(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}
function ConvertStringToDatetime(value) {
    var parts = value.split(/[/ :]/);

    //value = new Date(parts[2], parts[1] - 1, parts[0]);
    value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

    if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
        value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
    }
    return value;
}

