﻿var trcopy;
var onActionInsertUpdate = 0;
var inputs = ':checked,:selected,:text,textarea';
var table = "tableDemo";
var updatebutton = "ajaxUpdate";
var editbutton = "ajaxEdit";
var cancelbutton = "cancel";
var deletebutton = "ajaxDelete";
var deletebuttonTab = "ajaxDeleteTab";
var rejectbutton = "ajaxReject";

var approvebutton = "ajaxApprove";
var dataSource = 0;
var savebutton = "ajaxSave";
var effect = "flash";
var IDTDEdit = 0;
var DecimalFormat = undefined;
var AccessList = undefined;
var onLoop = 1;
var getLoop = 1;
var maskNumberSetting = {
    thousands: ",",
    decimal: ".",
    integer: false,
};

var maskNumberSetting2 = {
    thousands: "",
    decimal: "",
    integer: true,
};

$(document).ready(function () {
    // get data source for particular dropdown list in current document
    //getDataSource();

    /**
     * Save button click event
     */

    if (typeof PostBackData == 'undefined') {
        waitingDialog.show('Loading...');
        waitingDialog.hide();
    }

    $(document).on("click", "." + savebutton, function () {
        //Prevent double Click
        waitingDialog.show('Loading...');

        // Create new data object.
        var data = getFormData();

        if (data.valid) {
            var dataJson = JSON.stringify(data.record);
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Save" + Action);

                ajax(dataJson, "Save", url);
            }
            else {
                url = String.format("/{0}/{1}/Save", GroupMenu, NameMenu);
                ajax(dataJson, "Save", url);
            }
        }
        else {
            focusErrorTD();
        }
    });

    /**
     * Update button click event
     */
    $(document).on("click", "." + updatebutton, function () {
        //Prevent double Click
        waitingDialog.show('Loading...');

        // Create new data object.
        var data = getFormData();

        // Check if data to create or update are valid
        if (data.valid) {

            // Add key into the object
            data.record = addObjectKey(data.record, this);

            var dataJson = JSON.stringify(data.record)
            var url = '';
            if (typeof Action !== 'undefined') {
                url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Update" + Action);
                ajax(dataJson, "UpdateAction", url);

            }
            else {
                var resultToString = dataJson;
                var formUrl = window.location.href;

                if (resultToString.includes("\"IsActive\":\"true\"")) {
                    if (formUrl.includes("TaxRate")) {
                        var URL = "/SetupPayrollSetup/TaxRate/IsActiveBOOL";
                        var resultpass = dataJson;
                        IsActiveValidation(URL, resultpass);
                    }
                    else if (formUrl.includes("PositionExpense")) {
                        var URL = "/SetupPayrollSetup/PositionExpense/IsActiveBOOL";
                        var resultpass = dataJson;
                        IsActiveValidation(URL, resultpass);
                    }
                    else {
                        url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
                        ajax(dataJson, "UpdateCol", url);
                    }
                }
                else {
                    url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
                    ajax(dataJson, "UpdateCol", url);
                }
            }
            //ajax(dataJson, "UpdateCol", url);
        }
        else {
            focusErrorTD();
            $.notify({
                // options
                message: "Data is Not Valid"
            }, {
                // settings
                type: 'danger'
            });
        }
    });

    /**
     * Double click event for the table
     */

    if (!$("." + table).hasClass('no-dblClick')) {
        $(document).on("dblclick", "." + table + " td", function (e) {
            var id = $(this).closest('tr').attr("id");
            getDataSource("EditRow", id)
        });
    }
    /**
     * Edit button click event for the particular row in the table
     */
    $(document).on("click", "." + editbutton, function () {
        //$('input:checkbox').not(this).prop('checked', false);

        var id = $(this).closest('tr').attr("id");
        getDataSource("EditRow", id)
    });

    /**
     * Cancel button click event
     */
    $(document).on("click", "." + cancelbutton, function () {
        var id = $(this).closest('tr').attr("id");

        if (trcopy) {
            $("." + table + " tr[id='" + id + "']").html(trcopy);
            trcopy = '';
        } else {
            document.getElementById("dataTablesMain").deleteRow(1);
        }

        onActionInsertUpdate = 0;
    });

    /** 
     * Delete button click event for the particular row in the table
     */

    $(document).on("click", "." + deletebutton, function () {
        //Prevent double Click
        waitingDialog.show('Loading...');

        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKey({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Delete" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenu, NameMenu);
                    }
                    ajax(dataJson, "Delete", url);
                }
            } else {
                waitingDialog.hide();
            }
        }
    });

    $(document).on("click", "." + deletebuttonTab, function () {
        var TabName = $(this).attr("id");

        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to delete record ?")) {

                // Add key into the object
                var data = addObjectKeyTab({}, this, TabName);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof ActionTab[TabName] !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "Delete" + ActionTab[TabName]);
                    } else {
                        var url = String.format("/{0}/{1}/Delete", GroupMenuTab[TabName], NameMenuTab[TabName]);
                    }
                    ajax(dataJson, "Delete", url);
                }
            } else {
                //waitingDialog.hide();
            }
        }
    });


    /** 
    * Reject button click event for the particular row in the table
    */
    $(document).on("click", "." + rejectbutton, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Reject Data?")) {

                // Add key into the object
                var data = addObjectKey({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Reject" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Reject", GroupMenu, NameMenu);
                    }
                    ajax(dataJson, "Reject", url);
                }
            }
        }
    });


    /** 
    * Approve button click event for the particular row in the table
    */


    $(document).on("click", "." + approvebutton, function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false)

        var id = $(this).closest('tr').attr("id");
        if (id) {
            if (confirm("Do you really want to Post / Approve Data?")) {

                // Add key into the object
                var data = addObjectKey({}, this);

                // Check if data are available.
                if (data) {
                    var dataJson = JSON.stringify(data)
                    if (typeof Action !== 'undefined') {
                        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Approve" + Action);
                    } else {
                        var url = String.format("/{0}/{1}/Approve", GroupMenu, NameMenu);
                    }
                    ajax(dataJson, "Approve", url);
                }
            }
        }
    });

    /**
     * Document click event
     */
    $(document).click(function (e) {
        if (($(e.target).closest('tr').attr("id") != IDTDEdit) && $(e.target).closest(".ActionBtn").attr("class") != "ActionBtn" &&
            $(e.target).closest(".unmodified").length == 0 &&
            $(e.target).closest(".select2-dropdown").length == 0 &&
            onActionInsertUpdate == 1 && typeof MultiAdd == 'undefined') {
            CancelInsertOrUpdate();
        }
        else {
            if ($(e.target).closest(".unmodifiedSort").length > 0) {
                onActionInsertUpdate = 0;
            }
        }
    });

    /**
     * Checkbox click all event
     */
    $("#checkAll").click(function () {
        $('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".checkAll").each(function () {
        var that = $(this);
        var table = that.closest('table');
        that.click(function () {
            that.closest('table').find('.unmodified').children('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
});

function focusErrorTD() {
    if ($("." + table + " td").hasClass('has-error')) {
        var errorTD = $(document).find('td.has-error').find("input")[0];
        if (errorTD != undefined) {
            document.getElementById(errorTD.id).focus();
        }
        waitingDialog.hide();
    }
}

/**
 * Get form data from current row
 */
function getFormData() {
    var validation = [];
    var valid = true;
    var data = {};

    // Loop thru input elements available in the table
    $(document)
        .find('.' + table + ' tbody tr[id="' + IDTDEdit + '"')
        .find(inputs)
        .filter(function () {

            var controlType = $(this).parent().prop('type');

            var key;
            var value;
            var mandatory;

            // Check control type to get value and mandatory
            if (controlType && controlType === 'select-one') {
                if ($(this).parent().prop('id') == "") {
                    key = $(this).parent().closest('td').prop('class')
                }
                else {
                    key = $(this).parent().prop('id');
                }
                value = $.trim(this.value);
                mandatory = $(this).parent().attr("Mandatory");

            } else {

                controlType = $(this).prop('type');

                key = $(this).attr('id');

                if (controlType === 'checkbox' || controlType == 'radio') {

                    value = $(this).prop('checked') ? true : false;
                    mandatory = $(this).attr("Mandatory");

                } else {

                    value = $.trim(this.value);
                    if (key.includes("Date")) {
                        var parts = this.value.split(/[/ :]/);

                        //value = new Date(parts[2], parts[1] - 1, parts[0]);
                        value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

                        if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
                            value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
                        }
                        //value = key.includes("Date") ? new Date(tes) : $.trim(this.value);
                    }

                    mandatory = $(this).attr("Mandatory");

                }
            }

            // Check value and mandatory
            if (value === "" && mandatory === "true") {

                // Append error class
                $(this).closest('td').addClass("has-error");

                validation.push(0);

            } else {

                // Push data
                if (value != "") {
                    data[key] = value;
                }
                // Append success class
                // This assignment help to filter selected option for DDL when switch back to table row
                $(this).addClass("success");

                validation.push(1);
            }
        });

    // Check validation
    for (var i = 0; i < validation.length; i++) {

        // break if 0 found
        if (validation[i] === 0) {
            valid = false;
            break;
        }
    }

    return {
        valid: valid,
        record: data
    }
}

/**
 * Add key property into object for update or delete
 * @param {object} data
 * @param {object} _this - Copy of current row
 */
function addObjectKey(data, _this) {

    data[TI[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

function addObjectKeyTab(data, _this, tabName) {
    var tempTI = TITab[tabName];

    data[tempTI[0].Name] = $(_this).closest('tr').attr("id");

    return data;
}

/**
 * Make ajax request to create or update data.
 * @param {object} params
 * @param {string} action - Action name
 * @url {string}
 */
function ajax2(params, action, url, urlredirect) {
    $.post(url, { dataJSON: params }, function (result) {

        switch (action) {
            case "RegenerateAll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Regenerate Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { window.location.href = urlredirect; }, 2000);
                break;
        }
    });

}

function ajax(params, action, url) {
    $.post(url, { dataJSON: params }, function (result) {

        switch (action) {
            case "Save":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });

                        removeClassAfterCreateUpdate();

                    } else {
                        AddNewRow(result);

                        removeClassAfterCreateUpdate(true);
                        onActionInsertUpdate = 0;
                        $.notify({
                            // options
                            message: 'Success insert Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateCol":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdate();
                        }
                        else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    }
                    else {
                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + table + " tr[id=" + IDTDEdit + "]").clone();

                        $("." + cancelbutton).trigger("click");

                        for (var i = 1; i < TI.length; i++) {
                            var dataResult = cellValue(TI[i], result[TI[i].Name], trFormObject, i);

                            $("tr[id='" + result[TI[0].Name] + "'] td[class='" + TI[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdate(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdate = 0;

                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "UpdateAction":
                if (result != null) {
                    if (result.Status != undefined) {

                        if (result.Status == true) {
                            CancelInsertOrUpdate();

                        } else {
                            $.notify({
                                // options
                                message: result.Message
                            }, {
                                // settings
                                type: 'danger'
                            });

                        }

                    } else {

                        // Create copy of table row.
                        // To keep form before the first row is deleted.
                        // This purpose is to get value from selected option.
                        var trFormObject = $("." + table + " tr[id=" + IDTDEdit + "]").clone();

                        $("." + cancelbutton).trigger("click");

                        for (var i = 1; i < TI.length; i++) {
                            var dataResult = cellValue(TI[i], result[TI[i].Name], trFormObject, i);

                            $("tr[id='" + result[TI[0].Name] + "'] td[class='" + TI[i].Name + "']").html(dataResult);
                        }

                        removeClassAfterCreateUpdate(true);

                        $.notify({
                            // options
                            message: 'Success update Data'
                        }, {
                            // settings
                            type: 'success'
                        });

                        onActionInsertUpdate = 0;

                        //location.reload();
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "Delete":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $("." + table + " tr[id='" + result + "']").effect("highlight", { color: '#f4667b' }, 500, function () {
                            $("." + table + " tr[id='" + result + "']").remove();
                        });

                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;

            case "DeleteAll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }
                        $.notify({
                            // options
                            message: 'Success delete Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 4000);
                break;
            case "Reject":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Reject Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
                setTimeout(function () { window.location.href = urlredirect; }, 2000);
                break;
            case "Approve":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Approved Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "RegenerateAll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    }
                    else {
                        for (i = 0; i < result.length; i++) {
                            $("." + table + " tr[id='" + result[i] + "']").remove();
                        }

                        $.notify({
                            // options
                            message: 'Success Regenerate Data'
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                    var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
                    window.location.href = urlredirect;
                }
                setTimeout(function () { location.reload(); }, 2000);
                break;
            case "GeneratePayroll":
                if (result != null) {
                    if (result.Status == false) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'danger'
                        });
                    } else if (result.Status == true) {
                        $.notify({
                            // options
                            message: result.Message
                        }, {
                            // settings
                            type: 'success'
                        });
                    }
                    //else {
                    //    for (i = 0; i < result.length; i++) {
                    //        $("." + table + " tr[id='" + result[i] + "']").remove();
                    //    }

                    //    $.notify({
                    //        // options
                    //        message: 'Success Regenerate Data'
                    //    }, {
                    //        // settings
                    //        type: 'success'
                    //    });
                    //}
                    //var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
                    //window.location.href = urlredirect;
                }
                waitingDialog.hide();
                //setTimeout(function () { location.reload(); }, 2000);
                break;
        }
    });

}

/**
 * Remova css class after insert or update event
 * @param {bool} removeValue - Flag to indicate wethear need to clear the value or not.
 */
function removeClassAfterCreateUpdate(removeValue) {
    $(document).find("." + table).find(inputs).filter(function () {

        if (removeValue) {
            this.value = "";
        }

        $(this).removeClass("success");
        $(this).closest('td').removeClass("has-error")
    });
}
/**
 * render row to insert new data
 */
function renderRow() {
    $(".datepicker").datepicker("destroy");
    // flag to indicate weather need to bind data or not
    var flagBindData = 0;

    // get table object
    var tbl = document.getElementById('dataTablesMain');

    // insert row into table object
    var row = tbl.insertRow(1);

    // count total of rows
    var rowCount = $("#dataTablesMain tbody tr").length;

    // set row id equal to total row + 1
    row.id = (rowCount + 1) + 'guid';

    // set global current table row edit
    IDTDEdit = row.id;

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" disabled="disabled">';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TI.length; i++) {
        var obj = TI[i];
        var cell = row.insertCell(i)

        var control;
        switch (obj.Type) {
            case 'Input':
                control = createInputText(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null, "");
                break;
            case 'InputNumber':
                control = createInputNumber(obj);
                break;
            case 'InputNumeric':
                control = createInputNumeric(obj);
                break;
            case 'DDL':
                control = createDDL(i);
                break;

            case 'InputEnum':
                control = createEnumDDL(i);
                break;

            case 'InputCheckbox':
                control = createInputCheck(obj);
                break;
            case 'InputCheckboxGroup':
                control = createInputCheckGroup(obj);
                break;

            case 'AutocompleteDDL':
                control = createSelect(obj);
                break;

            case 'InputDate':
                control = createInputDate(obj, typeof obj.AutoText !== 'undefined' ? obj.AutoText : null);
                break;

            case 'InputLookup':
                control = CreateInputlookup(obj);
                break;

            case 'InputWithValidate':
                control = createInputTextWithValidate(obj);
                break;

            default:
                // reset control
                control = '';
                break;
        }

        // Check if control object exist.
        // If control exist, create the object inside cell.
        if (control) {
            cell.innerHTML = control;
            cell.className = obj.Name;
        }

        // After creating control, check if it need to bind to data source.
        // This must be executed after control is exist in the cell.
        if (obj.Type === 'AutocompleteDDL') {
            bindAutocompleteEvent(obj);

        }
    }

    var lastCell = row.insertCell(i);
    lastCell.innerHTML = '<a href="javascript:;" title="Save" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxSave"></i></a> <a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat cancel"></i></a>';
    lastCell.className = "text-center unmodified";

    //$(".datepicker").datepicker();
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    }).inputmask('date');

    $('[name=integer-default]').maskNumber();
    // set ActionInsertUpdate flag
    onActionInsertUpdate = 1;
}
/**
 * Create new row to insert new data
 */
function AddRow() {
    if (onActionInsertUpdate == 0 || typeof MultiAdd !== 'undefined') {
        getDataSource("AddRow");
    }
}

/**
 * Edit row
 * @param {string} id
 */
function editRow(id) {
    if (id && onActionInsertUpdate == 0) {
        $(".datepicker").datepicker("destroy");
        $('.unmodified').children('input:checkbox').not(this).prop('checked', false);
        // hide editing row, for the time being
        //$("." + table + " tr:last-child").fadeOut("fast");

        // Set global id for current edit table row
        IDTDEdit = id;

        // create html text to insert into row
        var html = '<td class="unmodified"><input type="checkbox" ></td>';

        // get current row record to update
        var row = document.getElementById(id);
        var selectedDate = 0;
        for (var i = 1; i < TI.length; i++) {
            var obj = TI[i];
            var val = ""
            if (row.getElementsByClassName(obj.Name)[0] == undefined) {
                val = "Please check this row's class !!!";
                $.notify({
                    // options
                    message: "Class " + obj.Name + "didn't found !!!"
                }, {
                    // settings
                    type: 'danger'
                });
            }
            else {
                val = row.getElementsByClassName(obj.Name)[0].innerText;
            }


            var stringContent;

            switch (obj.Type) {
                case 'Input':
                    stringContent = createInputText(obj, val, typeof obj.IsEdit !== 'undefined' ? "disabled" : "");
                    break;
                case 'InputNumber':
                    stringContent = createInputNumber(obj, val);
                    break;
                case 'InputNumeric':
                    stringContent = createInputNumeric(obj, val);
                    break;

                case 'DDL':
                    if (row.getElementsByClassName(obj.Name)[0].firstElementChild != null) {
                        stringContent = createDDL(i, val, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                        //stringContent = createSelect(obj);
                        //stringContent += createInputHidden(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    } else {
                        stringContent = createDDL(i, val);
                    }
                    break;

                case 'InputEnum':
                    stringContent = createEnumDDL(i, val);
                    break;

                case 'InputCheckbox':
                    val = row.getElementsByClassName(obj.Name)[0].innerHTML.includes("checked") ? true : false;
                    stringContent = createInputCheck(obj, val);
                    break;
                case 'InputCheckboxGroup':
                    val = row.getElementsByClassName(obj.Name)[0].innerHTML.includes("checked") ? true : false;
                    stringContent = createInputCheckGroup(obj, val);
                    break;

                case 'AutocompleteDDL':
                    stringContent = createSelect(obj);
                    stringContent += createInputHidden(obj, row.getElementsByClassName(obj.Name)[0].firstElementChild.value);
                    break;

                case 'InputDate':
                    if (val != null) {
                        stringContent = createInputDate(obj, val);
                    }
                    selectedDate = val;
                    break;

                default:
                    stringContent = val;
                    break;
            }

            html += '<td class="' + obj.Name + '">' + stringContent + '</td>';
        }

        html += '<td class="text-center unmodified">'
        if (AccessList != "" || AccessList.IsUpdate == "true") {
            html += '<a href="javascript:;" title="Update" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-floppy-disk ajaxUpdate"></i></a>'
        }
        html += '<a href="javascript:;" title="Return" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-repeat cancel"></i></a></td>'

        // Before replacing the TR contents, make a copy so when user clicks on .
        trcopy = $("." + table + " tr[id=" + id + "]").html();

        // Make jQuery object of current table row before replaceing the TR content.
        // This object will be used to get value from select option
        var trObject = $("." + table + " tr[id=" + id + "]").clone();

        // Create html controls.
        // Update edit row with controls created.
        $("." + table + " tr[id=" + id + "]").html(html);

        // Bind data source for dropdown list or autocomplete.
        // This must be executed after html controls are created.
        for (var i = 1; i < TI.length; i++) {
            var obj = TI[i];

            if (obj.Type === 'DDL') {

                var hidden = $(trObject).find('.' + obj.Name).find(':hidden');
                var selectedValue = hidden ? $(hidden).val() : $.trim($(trObject).find('.' + obj.Name).text());

                bindSelectData(obj, selectedValue);

            } else if (obj.Type === 'AutocompleteDDL') {
                var selectedValue = {
                    id: $(trObject).find('.' + obj.Name).find(':hidden').val(),
                    text: $.trim($(trObject).find('.' + obj.Name).text())
                };

                bindAutocompleteEvent(obj, selectedValue);

            }
        }
        $('[name=integer-default]').maskNumber();
        //$(".datepicker").datepicker();
        //$('.datepicker').datepicker('setValue', ConvertStringToDatetime(selectedDate));
        if (selectedDate != 0) {
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            }).inputmask('date').datepicker('setDate', ConvertStringToDatetime(selectedDate));
        } else {
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            }).inputmask('date').datepicker();
        }

        // set ActionInsertUpdate flag
        onActionInsertUpdate = 1;
    }
}

/**
 * Bind autocomplete event
 * @param {object} obj
 * @param {object} selectedObject - Expected { id: idValue, text: textValue }
 */
function bindAutocompleteEvent(obj, selectedObject) {
    var idSelector = '#' + obj.Name;
    var url = obj.Url;

    $(idSelector).select2({
        minimumInputLength: 1,
        language: {
            noResults: function () {
                return "Tidak ada hasil";
            },
            inputTooShort: function (args) {
                return "Minimal 1 huruf";
            },
        },
        ajax: {
            url: url,
            dataType: 'json',
            data: function (term, page) {
                return {
                    query: term.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: false
        }
    });

    // Set default selected if exist.
    if (selectedObject) {
        $(idSelector).select2("trigger", "select", { data: selectedObject });
    }
}

/**
 * Bind data to select
 * @param {object} obj
 * @param {string} selectedValue
 */
function bindSelectData(obj, selectedValue) {
    var idSelector = '#' + obj.Name;

    $(idSelector).select2({
        minimumResultsForSearch: -1,
        data: obj.ListData
    });

    // Set default selected if exist.
    if (selectedValue) {
        var selected = {};

        // Get object for selected value.
        for (var i = 0; i < obj.ListData.length; i++) {
            if (String(obj.ListData[i].id) === selectedValue || obj.ListData.text === selectedValue) {
                selected.id = obj.ListData[i].id,
                selected.text = obj.ListData[i].text

                break;
            }
        }

        $(idSelector).select2("trigger", "select", { data: selected });
    }
}

/**
 * Add new row after insert
 * @param {object} result - Json object returned after inserting new data.
 */
function AddNewRow(result) {

    // Create copy of table row.
    // To keep form before the first row is deleted.
    // This purpose is to get value from selected option.
    var trFormObject = $("." + table + " tr[id=" + IDTDEdit + "]").clone();

    // Delete the first row
    document.getElementById("dataTablesMain").deleteRow(1);

    // Get table object
    var tbl = document.getElementById('dataTablesMain');

    // Insert new row
    var row = tbl.insertRow(1);

    // Set row id equal to total row + 1
    row.id = result[TI[0].Name];

    var firstCell = row.insertCell(0);
    firstCell.innerHTML = '<input type="checkbox" >';
    firstCell.className = 'unmodified';

    for (var i = 1 ; i < TI.length; i++) {

        var obj = TI[i];
        var cell = row.insertCell(i)
        cell.innerHTML = cellValue(obj, result[obj.Name], trFormObject, i);
        cell.className = obj.Name;
    }

    var lastCell = row.insertCell(i);

    if (AccessList != "" || AccessList.IsUpdate == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Edit" class="btn btn-outline btn-warning btn-xs"><i class="glyphicon glyphicon-edit ajaxEdit"></i></a>';
    }
    if (AccessList != "" || AccessList.IsDelete == "true") {
        lastCell.innerHTML += '<a href="javascript:;" title="Cancel" class="btn btn-outline btn-danger btn-xs"><i class="glyphicon glyphicon-trash ajaxDelete"></i></a>';
    }
    lastCell.className = "text-center unmodified";

    // set ActionInsertUpdate flag
    onActionInsertUpdate = 0;
}

/**
 * Add cell value
 * @param {object} obj - Json object for the model.
 * @param {object} value - The value for each property defined in obj.
 * @param {object} trFormObject - Object copy contain table row form before inserting to database
 */
function cellValue(obj, value, trFormObject, loop) {
    switch (obj.Type) {
        case 'AutocompleteDDL':
        case 'DDL':
            // Create selector '#IDControl option:selected'
            var selector = '#' + obj.Name + ' .success';

            // Get object from table row object
            var selectObject = $(trFormObject).find(selector);

            // Set label
            var label = selectObject.length ? $(selectObject).text() : value;

            if (label == value) {
                var selectData = TI[loop].ListData.find(function (v) {
                    return v.id == value;
                });
                label = selectData.text;
            }

            // Create hidden control
            var control = createInputHidden(obj, value);

            return label + control;

        case 'Date':
            return value ? JSONDateWithTime(value) : '';

        case 'Bool':
            if (value != undefined && obj.ListData != undefined) {
                var text = $.grep(
                    obj.ListData,
                    function (n, i) {
                        return value === true ? n.toLowerCase() == 'active' : n.toLowerCase() === 'inactive';
                    });

                return text.length ? text[0] : value;
            }
            else {
                return null;
            }

        case 'InputEnum':
            if (obj.ListData.length) {
                var selectEnumData = TI[loop].ListData.find(function (text, v) {
                    return v == value;
                });
                return selectEnumData;
            }

            return '';

        case 'InputCheckbox':
            return createInputCheck(obj, value, true);
        case 'InputCheckboxGroup':
            return createInputCheck(obj, value, true);

        case 'InputDate':
            return value ? JSONDateWithTime(value) : '';

        case 'InputNumber':
            value = addDecimalSeparator(value, maskNumberSetting);
            value = addThousandSeparator(value, maskNumberSetting);
            return value;

        case 'InputNumeric':
            value = addThousandSeparator2(value, maskNumberSetting2);
            return value;


        default:
            return value ? value : '';
    }
}

/**
 * Cancel insert or update.
 */
function CancelInsertOrUpdate() {

    if (confirm("Do you really want to quit insert or update data ?")) {
        if (trcopy) {

            $("." + table + " tr[id='" + IDTDEdit + "']").html(trcopy);
            //$("." + table + " tr:last-child").fadeIn("fast");

            // clear global variable
            trcopy = '';
            IDTDEdit = '';

        } else {
            document.getElementById("dataTablesMain").deleteRow(1);
        }
        onActionInsertUpdate = 0;
    }

}

function ApproveAll() {
    if (confirm("Do you really want to Post / Approve record ?")) {
        var $inputs =
               $(document).find("." + table).find(":checked").filter(function () {
                   if (this.id == "") {
                       return $.trim(this.value);
                   }
               });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TI[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TI[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TI[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "ApproveAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/ApproveAll", GroupMenu, NameMenu);
            }
            ajax(dataJson, "Approve", url);
        } else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
        //var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
        //window.location.href = urlredirect;
    }
}


function RejectAll() {
    var $inputs =
    $(document).find("." + table).find(":checked").filter(function () {
        if (this.id == "") {
            return $.trim(this.value);
        }
    });
    var array = $inputs.map(function () {
        return this.value;
    }).get();
    var idName = TI[0].Name;
    var data = [];
    $.each($inputs, function () {
        var obj = new Object();
        obj[TI[0].Name] = $(this).closest('tr').attr("id");
        if (obj[TI[0].Name] != undefined) {
            data.push(obj);
        }
    });
    var dataJson = JSON.stringify(data)
    if (typeof Action !== 'undefined') {
        var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "RejectAll" + Action);
    } else {
        var url = String.format("/{0}/{1}/RejectAll", GroupMenu, NameMenu);
    }
    ajax(dataJson, "Reject", url);
    var urlredirect = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "Index");
    window.location.href = urlredirect;
}


function DeleteAll() {
    if (onActionInsertUpdate == 1) {
        //can't delete while on action Insert / Update
        return false;
    }
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("." + table).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        var idName = TI[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            obj[TI[0].Name] = $(this).closest('tr').attr("id");
            if (obj[TI[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof Action !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenu, NameMenu, "DeleteAll" + Action);
            } else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenu, NameMenu);
            }
            var formUrl = window.location.href;
            if (formUrl.includes("PositionExpense")) {
                var URL = "/SetupPayrollSetup/PositionExpense/IsActiveBOOLwithID";
                var IDPos = dataJson;
                IsActiveValidationwithDeleteAll(URL, IDPos);
            }
            else {
                ajax(dataJson, "DeleteAll", url);
                waitingDialog.show('Loading...');
                setTimeout(function () { waitingDialog.hide(); location.reload(); }, 2000);
                //location.reload();
            }
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }

    }
}

function TabDeleteAll(TabName) {
    if (confirm("Do you really want to delete record ?")) {
        var $inputs =
        $(document).find("#tab" + TabName + "." + table).find(":checked").filter(function () {
            if (this.id == "") {
                return $.trim(this.value);
            }
        });
        var array = $inputs.map(function () {
            return this.value;
        }).get();
        //var idName = TI[0].Name;
        var data = [];
        $.each($inputs, function () {
            var obj = new Object();
            var tempTI = TITab[TabName];

            //data[tempTI[0].Name] = $(_this).closest('tr').attr("id");

            obj[tempTI[0].Name] = $(this).closest('tr').attr("id");
            if (obj[tempTI[0].Name] != undefined) {
                data.push(obj);
            }
        });
        if (data.length > 0) {
            var dataJson = JSON.stringify(data)
            if (typeof ActionTab !== 'undefined') {
                var url = String.format("/{0}/{1}/{2}", GroupMenuTab[TabName], NameMenuTab[TabName], "DeleteAll" + ActionTab[TabName]);
            }
            else {
                var url = String.format("/{0}/{1}/DeleteAll", GroupMenuTab[TabName], NameMenuTab[TabName]);
            }
            ajax(dataJson, "DeleteAll", url);
            location.reload();
        }
        else {
            $.notify({
                // options
                message: "Please choose data!"
            }, {
                // settings
                type: 'danger'
            });
        }
    }
}

/**
 * Create DDL.
 * @param {object} DDLN
 * @param {string} ddlVal
 */

function createDDL(DDLN, ddlVal, ddlID) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    //AutoSelect For IsActive
    if (ddlVal == null && TI[DDLN].Name == "IsActive") {
        ddlVal = "Active";
    }

    onChangeFunction = typeof TI[DDLN].onchange !== 'undefined' ? TI[DDLN].onchange : "";

    input = '<select class="form-control" Mandatory="' + TI[DDLN].Mandatory + '" name=' + TI[DDLN].Name + ' id=' + TI[DDLN].Name + ' onchange=' + onChangeFunction + '>';
    if (TI[DDLN].ListData.length != 0) {
        input += '<option value="">Choose Any</option>';
        for (ddlLoop = 0; ddlLoop < TI[DDLN].ListData.length; ddlLoop++) {
            selected = "";
            if ($.trim(ddlVal) == TI[DDLN].ListData[ddlLoop].text)
                selected = "selected";
            input += '<option value="' + TI[DDLN].ListData[ddlLoop].id + '" ' + selected + '>' + TI[DDLN].ListData[ddlLoop].text + '</option>';
        }
    }

    //for ddl with a condition (a data that already used will be hidden)
    var findSelecectedData = TI[DDLN].ListData.find(function (item) {
        return item.text == $.trim(ddlVal)
    });
    if (findSelecectedData == undefined && ddlID != undefined) {
        input += '<option value="' + ddlID + '" selected>' + $.trim(ddlVal) + '</option>';
    }
    return input;

}

/**
 * Create Input hidden.
 * @param {object} obj
 * @param {string} value
 */
function createInputHidden(obj, value) {
    value = value ? $.trim(value) : '';

    return '<input type="hidden" id="hidden-' + obj.Name + '" value="' + value + '">';
}

/**
 * Create Input text.
 * @param {object} obj
 */
function createInputText(obj, value, IsEdit) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '"' + IsEdit + '>';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" ' + IsEdit + '>' + value + '</textarea>';
    }
    return inputType;
}

function createInputTextWithValidate(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control validateAmount ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    if (parseInt(obj.Length) >= 200) {
        inputType = '<textarea rows="4" cols="50" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" >' + value + '</textarea>';
    }
    return inputType;
}

function createInputNumber(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control maskNumber ' + obj.Name + '" maxlength="' + obj.Length + '" name="currency-default" id="' + obj.Name + '" value="' + value + '">';
    return inputType;
}

function createInputNumeric(obj, value) {
    value = value ? value.trim() : '';
    var inputType = '<input type="number" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';
    //var inputType = '<input type="text" Mandatory="' + obj.Mandatory + '" class="form-control ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '" onkeypress="return ' + (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57 + '">';
    return inputType;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        alert('Salah');
    }
    else {
        alert('Benar');
    }

}

function CreateInputlookup(obj, value) {
    return '<div class="form-group " style="margin: 0px !important;"><div class="input-group"><div class="bc-wrapper">' +
           '<input class="form-control autocomplete" data-val="true" data-val-number="The field IDApplicant must be a number." data-val-required="The IDApplicant field is required." id="' + obj.Name + '" name="' + obj.Name + '" readonly="readonly" type="hidden" value="0" autocomplete="off">' +
           '<div class="bc-menu list-group"></div></div><div class="bc-wrapper"><input class="form-control autocomplete" data-val="true" data-val-required="The NIK field is required" id="' + obj.ID + '" name="' + obj.ID + '" readonly="readonly" type="text" value="" autocomplete="off">' +
           '<div class="bc-menu list-group" style="display: none;"></div></div><span class="input-group-btn"><a href="' + obj.Url + '" id="idApplicationLU" onclick="showModal(this, 2);return false;" title="OrganizationLookup" class="btn btn-default">' +
           '<i class="glyphicon glyphicon-search"></i></a></span><span class="field-validation-valid text-danger" data-valmsg-for="ApplicantNIK" data-valmsg-replace="true"></span></div></div>';
}
function createInputDate(obj, value) {
    value = value ? value.trim() : '';

    return '<input type="Text" Mandatory="' + obj.Mandatory + '" class="form-control datepicker ' + obj.Name + '" maxlength="' + obj.Length + '" name="' + obj.Name + '" id="' + obj.Name + '" value="' + value + '">';;
}

/**
 * Create Input type checkbox.
 */
function createInputCheck(obj, value, disabled) {
    var checked = value ? 'checked ="on"' : '';
    var options = disabled ? checked + ' disabled="disabled"' : checked;

    return '<input type="checkbox" Mandatory="' + obj.Mandatory + '" class=" ' + obj.Name + '" ' + options + ' id="' + obj.Name + '">';
}

/**
 * Create Input type checkbox group.
 */
function createInputCheckGroup(obj, value, disabled) {
    var checked = value ? 'checked ="on"' : '';
    var options = disabled ? checked + ' disabled="disabled"' : checked;

    return '<input type="radio" Mandatory="' + obj.Mandatory + '" class=" ' + obj.Name + '" ' + options + ' id="' + obj.Name + '" name="' + obj.GroupName + '">';
}

/**
 * Create Select.
 * @param {object} obj
 * @param {object} options
 */
function createSelect(obj, options) {
    var selectOptions = '<option value="">Select</option>';

    return '<select class="form-control ' + obj.Name + '" mandatory="' + obj.Mandatory + '" id="' + obj.Name + '">' + selectOptions + '</select>';
}

/**
 * TODO Check code below
 */
function createEnumDDL(EDDLN, ddlVal) {
    ddlVal = typeof ddlVal !== 'undefined' ? ddlVal : null;

    input = '<select Mandatory="' + TI[EDDLN].Mandatory + '" name=' + TI[EDDLN].Name + '>';
    if (TI[EDDLN].ListData.length != 0) {
        for (ddlLoop = 0; ddlLoop < TI[EDDLN].ListData.length; ddlLoop++) {
            //console.log(selectOpt[i]);
            selected = "";
            if ($.trim(ddlVal) == TI[EDDLN].ListData[ddlLoop])
                selected = "selected";
            input += '<option value="' + ddlLoop + '" ' + selected + '>' + TI[EDDLN].ListData[ddlLoop] + '</option>';
        }
    }
    return input;
}

/**
 * Function to get data source for drop down list.
 * The data source will be used for drop down list in current document.
 */
function getDataSource(ActionRow, RowID) {
    onLoop = 1;
    getLoop = 1;
    getDecimalDataSource(ActionRow, RowID);
    getAccessDataSource(ActionRow, RowID);
    for (var i = 1 ; i < TI.length; i++) {
        switch (TI[i].Type) {
            case 'DDL':
                getDropdownDataSource(TI[i], TI[i].Url, ActionRow, RowID);
                break;
            case 'DDLWithOnChanged':
                getDropdownDataSource(TI[i], TI[i].Url, ActionRow, RowID);
                break;
            case 'InputEnum':
                getDropdownDataSource(TI[i], TI[i].Url, ActionRow, RowID);
                break;
        }
    }
    if (getLoop > 1) {
        waitingDialog.show('Loading...');
    }
    else {
        if (ActionRow == "AddRow") {
            renderRow();
        }
        else {
            editRow(RowID)
        }
    }
}

function getDecimalDataSource(ActionRow, RowID) {
    if (DecimalFormat == undefined) {
        getLoop++;
        $.getJSON("/SetupEnumSetup/Enum/getDecimalFormatString", function (result) {
            DecimalFormat = result;
            onLoop++;
            closeLoadingBar(onLoop, ActionRow, RowID)
        });
    }
}

function getAccessDataSource(ActionRow, RowID) {
    if (AccessList == undefined) {
        getLoop++;
        $.getJSON("/GlobalControlAccessor/GlobalAccess/GetAccessList", function (result) {
            AccessList = result;
            onLoop++;
            closeLoadingBar(onLoop, ActionRow, RowID)
        });
    }
}

function closeLoadingBar(onLoop, ActionRow, RowID) {
    if (onLoop == getLoop) {
        waitingDialog.hide();
        if (ActionRow == "AddRow") {
            renderRow();
        }
        else {
            editRow(RowID)
        }
    }
}

/**
 * Get data source with ajax.
 * Parameter given are object to set and url.
 */
function getDropdownDataSource(obj, url, ActionRow, RowID) {

    // only make request if the object is not available
    if (!obj.ListData) {
        getLoop++;
        url = !url && obj.Url ? obj.Url : url;
        $.getJSON(url, function (data) {
            obj.ListData = data;
            onLoop++;
            closeLoadingBar(onLoop, ActionRow, RowID)
        }).fail(function () {
            $.post("/GlobalControlAccessor/GlobalAccess/jsonErrorLog", { dataJSON: url }, function (result) {
                onLoop++;
                closeLoadingBar(onLoop, ActionRow, RowID)
                var splitUrl = url.split("/");
                $.notify({
                    // options
                    message: "Fail to get " + splitUrl[3]
                }, {
                    // settings
                    type: 'danger'
                });
            });
        });
    }
}

function addDecimalSeparator(value, settings) {
    if (DecimalFormat > 0) {
        var DecimalStringFormat = String.format("(\\d\{{0}\})$", DecimalFormat);
        var DecimalRegExp = new RegExp(DecimalStringFormat, '');
        value = value.toString().replace(DecimalRegExp, settings.decimal.concat("$1"));

        //value = value.replace(/(\d{2})$/, settings.decimal.concat("$1"));
        value = value.toString().replace(/(\d+)(\d{3}, \d{2})$/g, "$1".concat(settings.thousands).concat("$2"));
    }
    return value;
}

function addThousandSeparator(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}

function addThousandSeparator2(value, settings) {
    var totalThousandsPoints = (value.length - 3) / 3;
    var thousandsPointsAdded = 0;
    while (totalThousandsPoints > thousandsPointsAdded) {
        thousandsPointsAdded++;
        value = value.replace(/(\d+)(\d{3}.*)/, "$1".concat(settings.thousands).concat("$2"));
    }

    return value;
}

function LoadingBar() {
    waitingDialog.show('Loading...');
}

function ConvertStringToDatetime(value) {
    var parts = value.split(/[/ :]/);

    //value = new Date(parts[2], parts[1] - 1, parts[0]);
    value = new Date(Date.UTC(parts[2], parts[1] - 1, parts[0], '00', '00')) //di Update  Karena value yang di atas setelah di stringify hasil day -1

    if (parts[3] != undefined && parts[4] != undefined && parts[5] != undefined) {
        value = new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
    }
    return value;
}

function IsActiveValidationwithDeleteAll(URL, dataJson) {
    $.getJSON(URL, { dataJSON: dataJson }, function (result) {
        MessageboxValidationDeleteAll(result, dataJson)
    });
}

function MessageboxValidationDeleteAll(IsValidation, dataJson) {
    var url = '';
    if (IsValidation == "1") {
        alert('You cannot delete this column')
    }
    else {
        url = String.format("/{0}/{1}/DeleteAll", GroupMenu, NameMenu);
        //ajax(dataJson, "DeleteAll", url);
        location.reload();
    }
}

function IsActiveValidation(URL, resultpass) {
    $.getJSON(URL, function (result) {
        MessageboxValidation(result, resultpass)
    });
}

function MessageboxValidation(IsValidation, resultpass) {
    var dataJson = resultpass;
    var url = '';
    if (IsValidation == "1") {
        if (confirm('There is another year is active \nOnly 1 year can be active and read by system \nPress OK if you want continue')) {
            url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
            ajax(dataJson, "UpdateCol", url);
        }
        else {
            //location.reload();
            $.notify({
                // options
                message: 'Update unsuccessful'
            }, {
                // settings
                type: 'danger'
            });
            setTimeout(function () { location.reload(); }, 2000);
        }
    }
    else {
        url = String.format("/{0}/{1}/UpdateCol", GroupMenu, NameMenu);
        ajax(dataJson, "UpdateCol", url);
    }
}