﻿var exportTypeDocument;

$(document).ready(function () {
    // add export button to datatable
    var exportOptions = '<div class="dropdown" style="float: right !important;">'
                      + '    <button class="form-control input-sm" type="button" data-toggle="dropdown">Export As'
                      + '    <span class="caret"></span></button>'
                      + '    <ul class="dropdown-menu" style="min-width: 100px !important;">'
                      + '      <li class="export-document"><a href="#" >Excel</a></li>'
                      + '      <li class="export-document"><a href="#">PDF</a></li>'
                      + '    </ul>'
                      + '</div>';


    $('#dataTables-example_wrapper .col-sm-6:eq(1)').html(exportOptions);

    // show export confirmation dialog
    $('.export-document').click(function () {
        exportTypeDocument = $(this).text();
        $('#myModal').modal('show');
    });
});

function downloadDocument(exportTypeDocument, documentName, dataParams) {
    debugger;
    $.ajax({
        cache: false,
        url: '/SetupCommonSetup/ExportDocument/PostReportPartial',
        data: { exportTypeDocument: exportTypeDocument, documentName: documentName, dataParams: dataParams },
        success: function (data) {
            $('#myModal').modal('hide');
            window.location = '/SetupCommonSetup/ExportDocument/Download?fileGuid=' + data.FileGuid
                              + '&filename=' + data.FileName + '&AppName=' + data.AppName;
        }
    });
}

function RemoveNullModel(ListModel) {
    var returnNewModel = new Object();
    if (ListModel != null) {
        for (var key in ListModel) {
            var value = ListModel[key];
            if (value != null) {
                if (value != false || key == "isActiveEnum") {
                    returnNewModel[key] = value;
                }
            }
        }
    }
    return returnNewModel;
}
