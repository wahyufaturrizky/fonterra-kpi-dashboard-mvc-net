﻿$(document).ready(function () {
    $("#IDCompanyGroup").attr("onchange", "CompanyListGen();");

    //$("#IDCompany").attr("disabled", "disabled");

    $("#IDLocationGroup").attr("onchange", "LocationListGen();");

    //$("#IDLocation").attr("disabled", "disabled");
    $("#IDLocation").attr("onchange", "JobLevelListGen();");
    $("#IDCompany").attr("onchange", "LocationListGen();");

    //$("#IDJoblevel").attr("disabled", "disabled");
});

function CompanyListGen() {
    waitingDialog.show('Loading...');
    var targetID = "IDCompany";
    var targetLabel = "Company";

    var sourceID = $("#IDCompanyGroup");
    var id = {};

    var locationGroupID = $("#IDLocationGroup");

    id["SourceID"] = sourceID.val();

    //if (id["SourceID"] != '') {

    var urlGet = "/GlobalControlAccessor/GlobalAccess/GetAllCompanyByCompanyGroup?IDCompanyGroup=" + sourceID.val() + "&IncludeHttpContext=1";


    //$("#IDLocation").empty();
    //$(document.createElement('option')).attr('value', "").text("-- Select Location --").appendTo($("#IDLocation"));

    $("#IDCompany").empty();
    $(document.createElement('option')).attr('value', "").text("-- Select Company --").appendTo($("#IDCompany"));

    generateDropDown2(urlGet, null, targetID, targetLabel);

    $("#IDCompany").removeAttr("disabled");
    //}

}

function LocationListGen() {
    waitingDialog.show('Loading...');

    var targetID = "IDLocation";
    var targetLabel = "Location";

    var sourceID = $("#IDLocationGroup");
    var id = {};
    id["SourceID"] = sourceID.val();

    //var urlGet = "/PayrollAdministrationGlobalLookup/DropDownGenerator/LocationByLocationGroup";

    //var urlGet = "/GlobalControlAccessor/GlobalAccess/GetAllLocationByLocationGroup?IDLocationGroup=" + $("#IDLocationGroup").val() + "&IncludeHttpContext=1";
    var urlGet = "/GlobalControlAccessor/GlobalAccess/GetAllLocationByLocationGroupAndCompany?IDLocationGroup=" + $("#IDLocationGroup").val() + "&IncludeHttpContext=1&IDCompany=" + $("#IDCompany").val();

    generateDropDown2(urlGet, null, targetID, targetLabel);

    $("#IDLocation").removeAttr("disabled");
}

function JobLevelListGen() {
    waitingDialog.show('Loading...');

    var targetID = "IDJoblevel";
    var targetLabel = "Job Level";

    //var urlGet = "/PayrollAdministrationGlobalLookup/DropDownGenerator/LocationByLocationGroup";

    var urlGet = "/GlobalControlAccessor/GlobalAccess/GetAllJobLevel?IncludeHttpContext=1";
    //var urlGet = "/GlobalControlAccessor/GlobalAccess/GetAllJobLevelByIDLocationAndIDCompany?IDLocation=" + $("#IDLocation").val() + "&IncludeHttpContext=1&IDCompany=" + $("#IDCompany").val();

    generateDropDown2(urlGet, null, targetID, targetLabel);

    $("#IDJoblevel").removeAttr("disabled");
}

function GlobalSearchBaseReset() {
    $("#IDLocation").empty();
    $(document.createElement('option')).attr('value', "").text("-- Select Location --").appendTo($("#IDLocation"));

    $("#IDCompany").empty();
    $(document.createElement('option')).attr('value', "").text("-- Select Company --").appendTo($("#IDCompany"));

    $("#IDCompanyGroup").val('');
    //$(document.createElement('option')).attr('value', "").text("-- Select Company Group--").appendTo($("#IDCompanyGroup"));

    $("#IDLocationGroup").val('');
    //$(document.createElement('option')).attr('value', "").text("-- Select Location Group --").appendTo($("#IDLocationGroup"));

    $("#IDJoblevel").val('');

    $('#IDCompanyGroup').removeAttr("disabled");
    $('#IDLocationGroup').removeAttr("disabled");
}

function IsAllOnChanged(value) {
    if (value.checked) {
        $('#IDJoblevel').attr("disabled", "disabled");
        $('#IDCompanyGroup').attr("disabled", "disabled");
        $('#IDCompany').attr("disabled", "disabled");
        $('#IDLocationGroup').attr("disabled", "disabled");
        $('#IDLocation').attr("disabled", "disabled");

        $('#IDJoblevel').val("");
        $('#IDCompanyGroup').val("");
        $('#IDCompany').val("");
        $('#IDLocationGroup').val("");
        $('#IDLocation').val("");
    }
    else {
        $('#IDLocationGroup').removeAttr("disabled");
        $('#IDCompanyGroup').removeAttr("disabled");
        $('#IDCompany').removeAttr("disabled");
        $('#IDLocation').removeAttr("disabled");
        $('#IDJoblevel').removeAttr("disabled");

        //$("#IDCompany").attr("disabled", "disabled");
        //$("#IDLocation").attr("disabled", "disabled");
        //$("#IDJoblevel").attr("disabled", "disabled");
    }

}

function glyphiconOnChange() {
    var glyClass = document.getElementById("glyphiconOnChange").className;
    if (glyClass == "glyphicon glyphicon-collapse-down") {
        document.getElementById("glyphiconOnChange").className = "glyphicon glyphicon-collapse-up"
    }
    else {
        document.getElementById("glyphiconOnChange").className = "glyphicon glyphicon-collapse-down"
    }
}