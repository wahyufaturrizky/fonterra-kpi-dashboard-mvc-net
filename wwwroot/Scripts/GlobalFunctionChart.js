﻿
initDashboardPageCharts: function() {
    gradientChartOptionsConfigurationWithTooltipBlue = {
        "maintainAspectRatio": false,
        "legend": {
            "display": false
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 60,
                        "suggestedMax": 125,
                        "padding": 20,
                        "fontColor": "#2380f7"
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#2380f7"
                    }
                }
            ]
        }
    };

    gradientChartOptionsConfigurationWithTooltipPurple = {
        "maintainAspectRatio": false,
        "legend": {
            "display": false
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 60,
                        "suggestedMax": 125,
                        "padding": 20,
                        "fontColor": "#9a9a9a"
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(225,78,202,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9a9a9a"
                    }
                }
            ]
        }
    };

    barChartOptionsYpercent = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom"
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var currentValue = dataset.data[tooltipItem.index];
                    return String((Number(currentValue) * 100).toFixed(2)) + "%";
                }
            }
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "callback": function (value, index, values) {
                            return String(Math.trunc(Number(value) * 100)) + "%";
                        }
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(225,78,202,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9a9a9a"
                    }
                }
            ]
        }
    };

    barChartOptions = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom"
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 0,
                        // "suggestedMax": 125,
                        "padding": 20,
                        "fontColor": "#9a9a9a"
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(225,78,202,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9a9a9a"
                    }
                }
            ]
        }
    };

    gradientChartOptionsConfigurationWithTooltipOrange = {
        "maintainAspectRatio": false,
        "legend": {
            "display": false
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 50,
                        "suggestedMax": 110,
                        "padding": 20,
                        "fontColor": "#ff8a76"
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(220,53,69,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#ff8a76"
                    }
                }
            ]
        }
    };

    gradientChartOptionsConfigurationWithTooltipGreen = {
        "maintainAspectRatio": false,
        "legend": {
            "display": false
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.0)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 50,
                        "suggestedMax": 125,
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ],

            "xAxes": [
                {
                    "barPercentage": 1.6,
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(0,242,195,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ]
        }
    };

    pieChartConfiguration = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "point",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true
    };

    barStackedChartConfigurationRPBillion = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "intersect": 0,
            "position": "nearest",
            "mode": "index",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var currentValue = dataset.data[tooltipItem.index];
                    return formatNumberRp(String(currentValue));
                }
            }
        },
        "responsive": true,

        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    labelString: "Billions",
                    "ticks": {
                        "fontColor": "#fff",
                        "callback": function (value, index, values) {
                            return "Rp" + String((Number(value) / 1000000000).toFixed(3));
                        }
                    }
                }
            ]
        }
    };

    barStackedChartConfiguration = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "intersect": 0,
            "position": "nearest",
            "mode": "index"
        },
        "responsive": true,

        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    };

    horizontalBarStackedChartConfiguration = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom",
            "labels": {
                "boxWidth": 20,
                "fontColor": "#fff"
            }
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "intersect": 0,
            "position": "nearest",
            "mode": "index"
        },
        "responsive": true,

        "scales": {
            "xAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ],
            "yAxes": [
                {
                    "stacked": true,
                    "ticks": {
                        "fontColor": "#fff"
                    }
                }
            ]
        }
    };

    gradientBarChartConfigurationYpercent = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom"
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest",
            "callbacks": {
                "label": function (tooltipItem, data) {
                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var currentValue = dataset.data[tooltipItem.index];
                    return String((Number(currentValue) * 100).toFixed(2)) + "%";
                }
            }
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "callback": function (value, index, values) {
                            return String(Math.trunc(Number(value) * 100)) + "%";
                        }
                    }
                }
            ],

            "xAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ]
        }
    };

    gradientBarChartConfiguration = {
        "maintainAspectRatio": false,
        "legend": {
            "display": true,
            "position": "bottom"
        },

        "tooltips": {
            "backgroundColor": "#f5f5f5",
            "titleFontColor": "#333",
            "bodyFontColor": "#666",
            "bodySpacing": 4,
            "xPadding": 12,
            "mode": "nearest",
            "intersect": 0,
            "position": "nearest"
        },
        "responsive": true,
        "scales": {
            "yAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "suggestedMin": 0,
                        // "suggestedMax": 120,
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ],

            "xAxes": [
                {
                    "gridLines": {
                        "drawBorder": false,
                        "color": "rgba(29,140,248,0.1)",
                        "zeroLineColor": "transparent"
                    },
                    "ticks": {
                        "padding": 20,
                        "fontColor": "#9e9e9e"
                    }
                }
            ]
        }
    };
    $(document).ready(function () {
        $(".radiobtnfilter").click(function () {
            demo.refreshChart();
        });
    });

    this.refreshChart();
}