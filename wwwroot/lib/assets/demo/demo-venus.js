new Chart(document.getElementById("monthlyopenpo"), {
  "type": "bar",
  "data": {
    "labels": ["January", "February", "March", "April"],
    "datasets": [{
      "label": "Bar Dataset",
      "data": [10, 20, 30, 40],
      "borderColor": "rgb(255, 99, 132)",
      "backgroundColor": "rgba(255, 99, 132, 0.8)"
    }, {
      "label": "Line Dataset",
      "data": [10, 20, 30, 40],
      "type": "line",
      "fill": false,
      "borderColor": "rgb(54, 162, 235)"
    }]
  },
  "options": {
    "scales": {
      "yAxes": [{
        "ticks": {
          "beginAtZero": true
        }
      }]
    }
  }
});

new Chart(document.getElementById("monthlyGRNI"), {
  "type": "bar",
  "data": {
    "labels": ["January", "February", "March", "April"],
    "datasets": [{
      "label": "Bar Dataset",
      "data": [10, 20, 30, 40],
      "borderColor": "rgb(255, 99, 132)",
      "backgroundColor": "rgba(255, 99, 132, 0.8)"
    }, {
      "label": "Line Dataset",
      "data": [10, 20, 30, 40],
      "type": "line",
      "fill": false,
      "borderColor": "rgb(54, 162, 235)"
    }]
  },
  "options": {
    "scales": {
      "yAxes": [{
        "ticks": {
          "beginAtZero": true
        }
      }]
    }
  }
});

new Chart(document.getElementById("monthlyPOAfter"), {
  "type": "bar",
  "data": {
    "labels": ["January", "February", "March", "April"],
    "datasets": [{
      "label": "Bar Dataset",
      "data": [10, 20, 30, 40],
      "borderColor": "rgb(255, 99, 132)",
      "backgroundColor": "rgba(255, 99, 132, 0.8)"
    }, {
      "label": "Line Dataset",
      "data": [10, 20, 30, 40],
      "type": "line",
      "fill": false,
      "borderColor": "rgb(54, 162, 235)"
    }]
  },
  "options": {
    "scales": {
      "yAxes": [{
        "ticks": {
          "beginAtZero": true
        }
      }]
    }
  }
});

